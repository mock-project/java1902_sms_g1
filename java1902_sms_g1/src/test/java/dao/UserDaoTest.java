package dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fa.sms.config.SpringApplicationContextConfig;
import fa.sms.config.SpringSecurityConfig;
import fa.sms.entities.User;
import fa.sms.repository.UserRepository;
import fa.sms.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringApplicationContextConfig.class,SpringSecurityConfig.class})
@WebAppConfiguration
public class UserDaoTest {
  
  @Autowired
  private UserRepository userRepostiory;
  @Autowired
  private PasswordEncoder ecoder;
  @Autowired
  private UserService userService;
  
  
  @Test
  public void testSaveUser() {
    
    
    User user = new User();
    user.setAccount("marocvi");
    user.setEmail("marocvi89@gmail.com");
    user.setPassword(ecoder.encode("12345678"));
    user.setRole("USER");
    assertNotNull(userRepostiory.save(user));
    
    
    
  }
  
  @Test
  public void testLoadUser() {
	  userService.getUser(1);
	  userService.getUser(1);
  }
  
  

}
