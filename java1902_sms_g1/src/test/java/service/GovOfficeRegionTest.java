/*
 * @author Le Duy Hien
 * version 1.0
 */
package service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fa.sms.config.SpringApplicationContextConfig;
import fa.sms.entities.County;
import fa.sms.entities.GovOfficeRegion;
import fa.sms.entities.Organisation;
import fa.sms.entities.OrganisationDetail;
import fa.sms.entities.SupportingMaterial;
import fa.sms.repository.CountyRepository;
import fa.sms.repository.GovOfficeRegionRepository;
import fa.sms.repository.OrganisationDetailRepository;
import fa.sms.repository.OrganisationRepository;
import fa.sms.repository.SupportingMaterialRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringApplicationContextConfig.class)
@WebAppConfiguration
public class GovOfficeRegionTest {
	@Autowired
	private GovOfficeRegionRepository gr;
	@Autowired
	private CountyRepository coun;
	@Autowired
	private SupportingMaterialRepository sm;
	@Autowired
	private OrganisationRepository or;
	@Autowired
	private OrganisationDetailRepository ora;
	@Test
	public void testfindbyId() {
		Organisation asd = or.getOne(1);
		County c = coun.findByCountyName("GreenWich");
		System.out.println(c.getCountyName());
		List<GovOfficeRegion> asd1 = gr.findByCounty(c);
		assertEquals(5, asd1);
	}
	
	  @Test
	  public void testfindS() {
		  Organisation asd = or.findByOrganisationName("123");
		  OrganisationDetail d = ora.findByOrganisationByOrganisationId(asd);
	   List<SupportingMaterial> qwe = sm.findByOrganisationDetailByOrganisationDetailId(d);
	    System.out.println(qwe.size());
	   assertEquals(1, qwe.size());
	  }
}
