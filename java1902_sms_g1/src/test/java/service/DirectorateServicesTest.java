package service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fa.sms.config.SpringApplicationContextConfig;
import fa.sms.entities.BussinessType;
import fa.sms.entities.Directorate;
import fa.sms.repository.DirectorateRepository;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringApplicationContextConfig.class)
@WebAppConfiguration
public class DirectorateServicesTest {
@Autowired
private DirectorateRepository directorateRepository;
	@Test
	public void testSaveDirectorate() {
		Directorate directorate= new Directorate();
		BussinessType bussinessType= new BussinessType();
		bussinessType.setBussinessTypeId(1);
		directorate.setDirectorateName("SSC");
		directorate.setAddressLine1("Ha Noi");
		directorate.setPostcode("1");
		directorate.setBussinessTypeByBussinessTypeId(bussinessType);
		assertNotNull(directorateRepository.save(directorate));
	}
	@Test
	public void testgetAllDirectorate() {

		assertNotNull(directorateRepository.findAll());
	}
	@Test
	public void testfindbyId() {

		assertNotNull(directorateRepository.findById(1));
	}
}
