package ultil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fa.sms.common.utility.SendEmailUtil;
import fa.sms.config.SpringApplicationContextConfig;
import fa.sms.config.SpringSecurityConfig;
import fa.sms.entities.User;
import fa.sms.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpringApplicationContextConfig.class, SpringSecurityConfig.class })
@WebAppConfiguration
public class SendEmailUtilTest {

	@Autowired
	SendEmailUtil sendEmailUtil;
	@Autowired
	private UserRepository userRepostiory;

	@Test
	public void sendEmail() {
		User user = userRepostiory.findUserByAccount("marocvi");
		sendEmailUtil.sendPasswordToUser(user);
	}
	
}
