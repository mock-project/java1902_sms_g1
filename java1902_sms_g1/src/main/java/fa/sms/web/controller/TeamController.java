package fa.sms.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fa.sms.entities.Department;
import fa.sms.entities.Team;
import fa.sms.service.DepartmentService;
import fa.sms.service.TeamService;

@Controller
@RequestMapping("/user")
public class TeamController {
	@Autowired
	private TeamService teamService;
	@Autowired
	private DepartmentService departmentService;
	@GetMapping("/team-list-foreign")
	public String getAllTeam(Model theModel,int departmentId)
	{
		try {
		Department department = departmentService.getById(departmentId);
		List<Team> teams = teamService.findByDepartmentByDepartmentId(department);
		System.out.println(teams.size());
		for (Team team : teams) {
			if(team.getActive().equals("T")) {
				team.setActive("Yes");}
			else {
				team.setActive("No");
			}
		}
		theModel.addAttribute("LIST_TEAM", teams);}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "list-teams";
	}
	@GetMapping("/team-list")
	public String getAllTeam(Model theModel)
	{
		try {
		
		List<Team> teams = teamService.findByDepartmentByDepartmentId();
		System.out.println(teams.size());
		for (Team team : teams) {
			if(team.getActive().equals("T")) {
				team.setActive("Yes");}
			else {
				team.setActive("No");
			}
		}
		theModel.addAttribute("LIST_TEAM", teams);}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "list-teams";
	}
	
	@GetMapping("/add-team")
	public String showFormAddTeam(String teamId,Model theModel)
	{
		Team team = new Team();
		if(teamId!=null)
		{

			int id=Integer.parseInt(teamId);
			team=teamService.getTeamById(id);
			theModel.addAttribute("TEAM", team);
		}
		
		return "add-team";
	}
	@PostMapping("/save-team")
	public String  saveTeam(@ModelAttribute Team team,Model theModel )
	{
		System.out.println("counrty"+team.getCountry()+"name"+team.getTeamName());
		teamService.saveOrUpdate(team);
		return "redirect:/user/team-list";
	}

}
