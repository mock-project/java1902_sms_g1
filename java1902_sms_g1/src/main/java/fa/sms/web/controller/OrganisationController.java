package fa.sms.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fa.sms.common.exception.OrganisationException;
import fa.sms.common.utility.HelperConvert;
import fa.sms.common.valueobjects.Message;
import fa.sms.common.valueobjects.OrganisationVO;
import fa.sms.common.valueobjects.OrganisationVOForUpdate;
import fa.sms.entities.Country;
import fa.sms.entities.County;
import fa.sms.entities.GovOfficeRegion;
import fa.sms.entities.Organisation;
import fa.sms.entities.OrganisationDetail;
import fa.sms.entities.Premise;
import fa.sms.entities.Service;
import fa.sms.entities.ServiceOrganization;
import fa.sms.service.BusinessTypeService;
import fa.sms.service.CountryService;
import fa.sms.service.CountyService;
import fa.sms.service.GovOfficeRegionService;
import fa.sms.service.OrganisationDetailService;
import fa.sms.service.OrganisationService;
import fa.sms.service.ServiceOrganizationService;
import fa.sms.service.ServiceService;

@Controller
@RequestMapping(value = "user")
public class OrganisationController {
  private OrganisationService organisationService;
  private CountryService countryService;
  private OrganisationDetailService organisationDetailService;
  private BusinessTypeService businessTypeService;
  private ServiceOrganizationService serviceOganisationService;
  private ServiceService serviceService;
  private GovOfficeRegionService govOfficeRegionService;
  private CountyService countyService;

  @Value("#{'${organisationSpecialism}'.split(',')}")
  private List<String> organisationSpecialismes;

  @Value("#{'${servicePersonalCircumstacesCapabilities}'.split(',')}")
  private List<String> servicePersonalCircumstacesCapabilities;

  @Value("#{'${serviceDisablitiesCapabilities}'.split(',')}")
  private List<String> serviceDisablitiesCapabilities;

  @Value("#{'${serviceEthnicity}'.split(',')}")
  private List<String> serviceEthnicity;

  @Value("#{'${serviceBarriersCapabilities}'.split(',')}")
  private List<String> serviceBarriersCapabilities;

  @Value("#{'${accreditation}'.split(',')}")
  private List<String> accreditation;

  @Value("#{'${serviceBenefitsCapabilities}'.split(',')}")
  private List<String> serviceBenefitsCapabilities;

  @Value("#{'${eoiProgrammes}'.split(',')}")
  private List<String> eoiProgrammes;

  @Value("#{'${eoiServices}'.split(',')}")
  private List<String> eoiServices;

  @Value("#{'${trustRegionName}'.split(',')}")
  private List<String> trustRegionName;

  @Value("#{'${trustDistrictName}'.split(',')}")
  private List<String> trustDistrictName;

  @RequestMapping(value = "list-organisations")
  public String listOrganisations(ModelMap modelMap) {
    List<Organisation> organisations = organisationService.getAll();
    modelMap.addAttribute("organisations", organisations);
    return "list-organisations";
  }

  @RequestMapping(value = "create-organisation")
  public String createOrganisation(ModelMap modelMap) {
    List<Country> countries = countryService.findAll();
    setModelAttributes(modelMap, countries, getAllGovOfficeRegion());
    return "create-organisation";
  }

  @PostMapping(value = "create-organisation")
  @ResponseBody
  public Message addOrganisation(ModelMap modelMap,
      @ModelAttribute OrganisationVO organisationVO) throws IOException {
    Organisation organisation = organisationService
        .convertToOrganisation(organisationVO);
    Organisation saved = null;
    List<String> oEoiServices = HelperConvert.convertOrganisationDetails(organisationVO.getEoiServices());
    List<Service> services = new ArrayList<Service>();
    if (oEoiServices.size() > 0) {
      for (String service : oEoiServices) {
        services.add(serviceService.findByName(service));
      }
    }
    OrganisationDetail organisationDetail = null;
    Message message = new Message();
    try {
      saved = organisationService.saveOrganisation(organisation);
      organisationDetail = organisationDetailService
          .convertToOrganisationDetail(organisationVO, saved);
      organisationDetailService.saveOrganisationDetail(organisationDetail);
      if (services.size() > 0) {
        for (Service s : services) {
          System.out.println(s.toString());
          ServiceOrganization serviceOrganization = new ServiceOrganization();
          serviceOrganization.setOrganisationByOrganizationId(saved);
          serviceOrganization.setServiceByServiceId(s);
          System.out.println(serviceOrganization.toString());
          serviceOganisationService.save(serviceOrganization);
        }
      }
      message.setId(saved.getOrganisationId());
      message.setName("success");
      message.setDescription("Save organisation successfully.");
    } catch (OrganisationException organisationException) {
      message.setName("error");
      message.setDescription(organisationException.getMessage());
    }
    return message;
  }

  @PostMapping(value = "update-organisation")
  @ResponseBody
  public Message UpdateOrganisation(ModelMap modelMap,
      @ModelAttribute OrganisationVOForUpdate organisationVOForUpdate)
      throws IOException {
    Organisation organisation = organisationService.findById(
        Integer.parseInt(organisationVOForUpdate.getOrganisationId()));
    OrganisationDetail updateOrganisationDetail = organisationDetailService
        .updateForOrganisationDetail(organisation, organisationVOForUpdate);
    organisationDetailService.saveOrganisationDetail(updateOrganisationDetail);
    String organisationOldName = organisation.getOrganisationName();
    Organisation update = organisationService
        .updateForOrganisation(organisation, organisationVOForUpdate);
    Organisation updated = null;
    Message message = new Message();
    if (!update.getOrganisationName().toString()
        .equals(organisationOldName.toString())) {
      try {
        updated = organisationService.saveOrganisation(update);
        message.setId(updated.getOrganisationId());
        message.setName("success");
        message.setDescription("Update for organisation "
            + organisation.getOrganisationName() + " successfully.");
      } catch (OrganisationException organisationException) {
        message.setName("error");
        message.setDescription(organisationException.getMessage());
      }
    } else {
      updated = organisationService.saveOrganisationWithOldName(update);
      message.setId(updated.getOrganisationId());
      message.setName("success");
      message.setDescription("Update for organisation "
          + organisation.getOrganisationName() + " successfully.");
    }
    return message;
  }

  @RequestMapping(value = "update-active-organisation")
  public String updateForActiveOrganisation(ModelMap modelMap,
      @RequestParam String organisationId) {
    Organisation organisation = organisationService
        .findById(Integer.parseInt(organisationId));
    OrganisationDetail organisationDetail = organisationDetailService
        .findByOrganistionId(organisation);
    List<Country> countries = countryService.findAll();
    setModelAttributes(modelMap, countries, getAllGovOfficeRegion(),
        getAllPremiseByOrganisationId(organisationId));
    setModelAttributes(modelMap, organisationDetail, organisation);
    return "create-organisation";
  }

  private Set<Premise> getAllPremiseByOrganisationId(String organisationId) {
    return organisationService.findAllPremise(Integer.parseInt(organisationId));
  }

  @RequestMapping(value = "inActive-organisation")
  public String inActiveOrganisation(ModelMap modelMap,
      @RequestParam String organisationId) {
    Organisation organisation = organisationService
        .findById(Integer.parseInt(organisationId));
    organisation.setActive("N");
    organisationService.saveOrganisationWithOldName(organisation);
    List<Organisation> organisations = organisationService.getAll();
    modelMap.addAttribute("organisations", organisations);
    return "list-organisations";
  }

  @RequestMapping(value = "active-organisation")
  public String activeOrganisation(ModelMap modelMap,
      @RequestParam String organisationId) {
    Organisation organisation = organisationService
        .findById(Integer.parseInt(organisationId));
    OrganisationDetail organisationDetail = organisationDetailService
        .findByOrganistionId(organisation);
    organisation.setActive("T");
    Organisation updated = organisationService
        .saveOrganisationWithOldName(organisation);
    List<Country> countries = countryService.findAll();
    setModelAttributes(modelMap, countries, getAllGovOfficeRegion(),
        getAllPremiseByOrganisationId(organisationId));
    setModelAttributes(modelMap, organisationDetail, updated);
    return "create-organisation";
  }
  
	@RequestMapping(value = "back-from-material")
	public String showAllMaterialWithTheCurrentOrganisation(ModelMap modelMap, @RequestParam String organisationId) {
		Organisation organisation = organisationService.findById(Integer.parseInt(organisationId));
		OrganisationDetail organisationDetail = organisationDetailService.findByOrganistionId(organisation);
		List<Country> countries = countryService.findAll();
		setModelAttributes(modelMap, countries, getAllGovOfficeRegion(), getAllPremiseByOrganisationId(organisationId));
		setModelAttributes(modelMap, organisationDetail, organisation);
		return "create-organisation";
	}

  private List<GovOfficeRegion> getAllGovOfficeRegion() {
    County county = countyService.findByCountyName("GreenWich");
    return govOfficeRegionService.findAllByCounty(county);
  }

  private void setModelAttributes(ModelMap modelMap,
      OrganisationDetail organisationDetail, Organisation organisation) {
    modelMap.addAttribute("organisation", organisation);
    modelMap.addAttribute("oOrganisationSpecialismes",
        HelperConvert.convertOrganisationDetails(
            organisationDetail.getOrganisationSpecialism()));
    modelMap.addAttribute("oServicePersonalCircumstacesCapabilities",
        HelperConvert.convertOrganisationDetails(
            organisationDetail.getServicePersonalCircumstacesCapabilities()));
    modelMap.addAttribute("oServiceDisablitiesCapabilities",
        HelperConvert.convertOrganisationDetails(
            organisationDetail.getServiceDisablitiesCapabilities()));
    modelMap.addAttribute("oServiceEthnicity", HelperConvert
        .convertOrganisationDetails(organisationDetail.getServiceEthnicity()));
    modelMap.addAttribute("oServiceBarriersCapabilities",
        HelperConvert.convertOrganisationDetails(
            organisationDetail.getServiceBarriersCapabilities()));
    modelMap.addAttribute("oAccreditation", HelperConvert
        .convertOrganisationDetails(organisationDetail.getAccreditation()));
    modelMap.addAttribute("oServiceBenefitsCapabilities",
        HelperConvert.convertOrganisationDetails(
            organisationDetail.getServiceBenefitsCapabilities()));
    modelMap.addAttribute("oEoiProgrammes", HelperConvert
        .convertOrganisationDetails(organisationDetail.getEoiProgrammes()));
    modelMap.addAttribute("oEoiServices", HelperConvert
        .convertOrganisationDetails(organisationDetail.getEoiServices()));
  }

  private void setModelAttributes(ModelMap modelMap, List<Country> countries,
      List<GovOfficeRegion> govOfficeRegionName, Set<Premise> premises) {
    modelMap.addAttribute("countries", countries);
    modelMap.addAttribute("organisationSpecialismes", organisationSpecialismes);
    modelMap.addAttribute("servicePersonalCircumstacesCapabilities",
        servicePersonalCircumstacesCapabilities);
    modelMap.addAttribute("serviceDisablitiesCapabilities",
        serviceDisablitiesCapabilities);
    modelMap.addAttribute("serviceEthnicity", serviceEthnicity);
    modelMap.addAttribute("serviceBarriersCapabilities",
        serviceBarriersCapabilities);
    modelMap.addAttribute("accreditation", accreditation);
    modelMap.addAttribute("serviceBenefitsCapabilities",
        serviceBenefitsCapabilities);
    modelMap.addAttribute("eoiProgrammes", eoiProgrammes);
    modelMap.addAttribute("eoiServices", eoiServices);
    modelMap.addAttribute("govOfficeRegionNames", govOfficeRegionName);
    modelMap.addAttribute("trustRegionNames", trustRegionName);
    modelMap.addAttribute("trustDistrictNames", trustDistrictName);
    modelMap.addAttribute("premises", premises);
  }

  private void setModelAttributes(ModelMap modelMap, List<Country> countries,
      List<GovOfficeRegion> govOfficeRegionName) {
    modelMap.addAttribute("countries", countries);
    modelMap.addAttribute("organisationSpecialismes", organisationSpecialismes);
    modelMap.addAttribute("servicePersonalCircumstacesCapabilities",
        servicePersonalCircumstacesCapabilities);
    modelMap.addAttribute("serviceDisablitiesCapabilities",
        serviceDisablitiesCapabilities);
    modelMap.addAttribute("serviceEthnicity", serviceEthnicity);
    modelMap.addAttribute("serviceBarriersCapabilities",
        serviceBarriersCapabilities);
    modelMap.addAttribute("accreditation", accreditation);
    modelMap.addAttribute("serviceBenefitsCapabilities",
        serviceBenefitsCapabilities);
    modelMap.addAttribute("eoiProgrammes", eoiProgrammes);
    modelMap.addAttribute("eoiServices", eoiServices);
    modelMap.addAttribute("govOfficeRegionNames", govOfficeRegionName);
    modelMap.addAttribute("trustRegionNames", trustRegionName);
    modelMap.addAttribute("trustDistrictNames", trustDistrictName);
  }

  @Autowired
  public OrganisationController(OrganisationService organisationService) {
    super();
    this.organisationService = organisationService;
  }

  public CountryService getCountryService() {
    return countryService;
  }

  @Autowired
  public void setCountryService(CountryService countryService) {
    this.countryService = countryService;
  }

  public OrganisationDetailService getOrganisationDetailService() {
    return organisationDetailService;
  }

  @Autowired
  public void setOrganisationDetailService(
      OrganisationDetailService organisationDetailService) {
    this.organisationDetailService = organisationDetailService;
  }

  public BusinessTypeService getBusinessTypeService() {
    return businessTypeService;
  }

  @Autowired
  public void setBusinessTypeService(BusinessTypeService businessTypeService) {
    this.businessTypeService = businessTypeService;
  }

  public ServiceOrganizationService getServiceOganisationService() {
    return serviceOganisationService;
  }

  @Autowired
  public void setServiceOganisationService(
      ServiceOrganizationService serviceOganisationService) {
    this.serviceOganisationService = serviceOganisationService;
  }

  public ServiceService getServiceService() {
    return serviceService;
  }

  @Autowired
  public void setServiceService(ServiceService serviceService) {
    this.serviceService = serviceService;
  }

  public GovOfficeRegionService getGovOfficeRegionService() {
    return govOfficeRegionService;
  }

  @Autowired
  public void setGovOfficeRegionService(
      GovOfficeRegionService govOfficeRegionService) {
    this.govOfficeRegionService = govOfficeRegionService;
  }

  public CountyService getCountyService() {
    return countyService;
  }

  @Autowired
  public void setCountyService(CountyService countyService) {
    this.countyService = countyService;
  }

}
