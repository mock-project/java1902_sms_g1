package fa.sms.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import fa.sms.entities.Directorate;
import fa.sms.service.DirectorateServices;

@Controller
public class DirectorateController {
	@Autowired
	private DirectorateServices directorateServices;
	@GetMapping(value = "list-directorates")
	public String listDirectorates(Model model) {
		List<Directorate> list=directorateServices.getAllDirectorate();
		for (Directorate directorate : list) {
			if(directorate.getActive().equals("T")) {
				directorate.setActive("Yes");
			}else{
				directorate.setActive("No");
			}
		}
		model.addAttribute("listDirectorate", list);
		return "list-directorates";
	}

	@GetMapping(value = "add-directorates")
	public String addDirectorates(Model model) {
		return "add-directorates";
	}
	@PostMapping(value = "/adddirectorates")
	public  String addDirectorate(@RequestBody Directorate directorate,Model model) {
		directorateServices.saveDirectorate(directorate);
		model.addAttribute("message","Save Directorate Successfully");
		return "redirect:list-directorates";
	}

	/*
	 * @GetMapping(value = "/amend-directorates") public String
	 * amendDirectorates(String directorateName,Model model) { Directorate
	 * directorate=directorateServices.findByDirectorateName(directorateName);
	 * model.addAttribute("directorate",directorate); return "add-directorates"; }
	 */
	@GetMapping(value = "/amend-directorates")
	public String amendDirectorates(Model model,@RequestParam("directorateId") int directorateId ) {
		Directorate directorate=directorateServices.findbyId(directorateId);
		model.addAttribute("directorate",directorate);
		return "add-directorates";
	}
	@PostMapping(value = "/change-active")
	public String changeActive(Model model,@RequestParam("directorateId") int directorateId ) {
		Directorate directorate= directorateServices.Active(directorateId);
//		directorateServices.changeActive(directorateId);
		directorateServices.saveDirectorate(directorate);
		model.addAttribute("directorate", directorate);
		return "add-directorates";
	}
	@PostMapping(value = "/change-inactive")
	public String changeinActive(@RequestParam String directorateId ,Model model) {
		int id=Integer.parseInt(directorateId);
		System.out.println("id"+id);
		Directorate directorate=directorateServices.InActive(id);
		directorateServices.saveDirectorate(directorate);
		model.addAttribute("directorate", directorate);
		return "add-directorates";
		
	}
	
}
