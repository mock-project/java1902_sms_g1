package fa.sms.web.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fa.sms.common.valueobjects.Message;
import fa.sms.common.valueobjects.SupportMaterialVO;
import fa.sms.entities.SupportingMaterial;
import fa.sms.entities.User;
import fa.sms.service.SupportingMaterialServices;

@RequestMapping(value = "user")
@Controller
public class SupportingMaterialController {
  private SupportingMaterialServices supportingMaterialServices;

  @GetMapping(value = "list-supportingmaterial")
  public String listSuportingMaterial(Model model) {
    List<SupportingMaterial> list = supportingMaterialServices
        .fillAllSupportingMaterial();
    model.addAttribute("listsupportingmaterial", list);
    for (SupportingMaterial supportingMaterial : list) {
      DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy ");
      dateFormat.format(supportingMaterial.getAddDate());
    }
    return "list-supportingmaterial";
  }

  @GetMapping(value = "add-suportingmaterial")
  public String addSupportingMaterial(SupportingMaterial supportingMaterial,
      Model model) throws ParseException {
    model.addAttribute("supportingMaterial", supportingMaterial);
    return "add-suportingmaterial";
  }

  @PostMapping(value = "add-suportingmaterial")
  public String addSuportingmaterial(
      @RequestBody SupportingMaterial supportingMaterial, Model model) {
    supportingMaterialServices.saveSupportingMaterial(supportingMaterial);
    model.addAttribute("message", "Save Supporting Material Success");
    return "add-suportingmaterial";
  }

  @PostMapping(value = "create-support-material")
  @ResponseBody
  public Message addSuportingMaterial(ModelMap modelMap,
      @ModelAttribute SupportMaterialVO supportMaterialVO) {
    Message message = new Message();
    try {

      SupportingMaterial saved = supportingMaterialServices
          .saveSupportingMaterial(supportingMaterialServices
              .convertToSupportingMaterial(supportMaterialVO));
      message.setId(saved.getSupportingMaterialId());
      message.setDescription("Add material successfully");
      message.setName("success");
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return message;
  }

  @Autowired
  public SupportingMaterialController(
      SupportingMaterialServices supportingMaterialServices) {
    super();
    this.supportingMaterialServices = supportingMaterialServices;
  }

//	@PostMapping(value = "/add-suportingmaterial")
//	public String addSuportingmaterial(@RequestBody SupportingMaterial supportingMaterial, Model model) {
//		supportingMaterialServices.saveSupportingMaterial(supportingMaterial);
//		model.addAttribute("message", "Save Supporting Material Success");
//		return "add-suportingmaterial";
//	}
//  @PostMapping(value = "/add-suportingmaterial")
//  public String addaddSuportingmaterial(
//      @RequestParam(required = false, name = "url") String url,
//      @RequestParam(required = false, name = "description") String description,
//      Date addedDate,
//      @RequestParam(required = false, name = "type") String type,
//      SupportingMaterial supportingMaterial, User user, Model model)
//      throws ParseException {
//    supportingMaterial.setUrl(url);
//    supportingMaterial.setDescription(description);
//    supportingMaterial.setType(type);
//    supportingMaterial.setActive("T");
//    user.setUserId(1);
//    supportingMaterial.setUserByUserId(user);
//    supportingMaterial.setAddDate(new Date());
//    supportingMaterialServices.saveSupportingMaterial(supportingMaterial);
//
//    return "list-supportingmaterial";
//
//  }
}
