package fa.sms.web.controller;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "user")
public class HomeController {
	@RequestMapping(value = {  "/homepage" })
	public String homePage(Model model) {
		return "home-page";
	}
}
