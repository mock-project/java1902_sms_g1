package fa.sms.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import fa.sms.common.utility.SendEmailUtil;
import fa.sms.common.valueobjects.UserVo;
import fa.sms.entities.User;
import fa.sms.service.UserService;

@Controller
public class UserController {
  @Autowired
  private UserService userService;
  @Autowired
  private SendEmailUtil sendEmailUtil;

  @GetMapping("/forget-password")
  public String forgetPassword() {
    return "forget-password";
  }

  @PostMapping("/get-password")
  public ResponseEntity<String> getPassword(@ModelAttribute UserVo userVo) {
    User user = userService.getUserByUserVo(userVo);
    if (user.getEmail().equals(userVo.getEmail())) {
      if(sendEmailUtil.sendPasswordToUser(user)) {
    	  return new ResponseEntity<String>("OK", HttpStatus.OK);
      }
      // Send email
      return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
    return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
  }

  @GetMapping("/login")
  public String getLogin() {
    return "login";
  }

  @GetMapping("/user/home")
  public String getHome() {
    return "home";
  }

}
