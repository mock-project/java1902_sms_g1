package fa.sms.web.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fa.sms.entities.Department;
import fa.sms.entities.Team;
import fa.sms.service.DepartmentService;
import fa.sms.service.TeamService;

@Controller
@RequestMapping("/user")
public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private TeamService teamService;
	@GetMapping("/dept-list")
	public String getAllDepartment(Model theModel)
	{
		try {
		List<Department> departments = departmentService.findAll();
		System.out.println(departments.size());
		for (Department department : departments) {
			if(department.getActive().equals("T")) {
			department.setActive("Yes");}
			else {
				department.setActive("No");
			}
		}
		theModel.addAttribute("LIST_DEPT", departments);}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return "dept-list";
	}
	@GetMapping("/add-dept")
	public String showFormAddDept(String departmentId,Model theModel)
	{
		
		Department dept = new Department();
		if(departmentId!=null)
		{

			int id=Integer.parseInt(departmentId);
			dept=departmentService.getById(id);
			theModel.addAttribute("DEPT", dept);
			List<Team> teams = teamService.findByDepartmentByDepartmentId(dept);
			theModel.addAttribute("TEAM", teams);
		}
		
		return "add-dept";
	}
	@PostMapping("/save-dept")
	public String  saveDepartment(@ModelAttribute Department department,Model theModel )
	{
		
		System.out.println("counrty"+department.getCountry());
		departmentService.saveOrUpdate(department);
		return "redirect:/user/dept-list";
	}
	@PostMapping("/in-active-dept")
	public String inActiveDepartment(@RequestParam int departmentId)
	{
		
		departmentService.inActiveDept(departmentId);
		return "redirect:/user/dept-list";
	}
	@PostMapping("/is-active-dept")
	public String isActiveDepartment(@RequestParam int departmentId)
	{
		
		departmentService.isActiveDept(departmentId);
		return "redirect:/user/dept-list";
	}

}
