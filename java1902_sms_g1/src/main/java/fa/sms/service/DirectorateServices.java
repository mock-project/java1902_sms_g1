package fa.sms.service;

import java.util.List;

import fa.sms.entities.Directorate;

public interface DirectorateServices {
	void saveDirectorate(Directorate directorate);

	List<Directorate> getAllDirectorate();

	Directorate findbyId(Integer directorateId);

	Directorate findByDirectorateName(String directorateName);

	void changeActive(Integer directorateId);
	Directorate Active(Integer directorateId);
	Directorate InActive(Integer directorateId);
}
