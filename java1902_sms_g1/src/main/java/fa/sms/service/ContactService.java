package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.Contact;
import fa.sms.repository.ContactRepository;

@Service
public class ContactService {
	private ContactRepository contactTypeRepository;

	public Contact getByContactName(String contactName) {
		return contactTypeRepository.findByContactName(contactName);
	}

	@Autowired
	public ContactService(ContactRepository contactTypeRepository) {
		super();
		this.contactTypeRepository = contactTypeRepository;
	}

}
