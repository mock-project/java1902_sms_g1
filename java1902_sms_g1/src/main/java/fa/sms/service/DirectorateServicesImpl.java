package fa.sms.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.Directorate;
import fa.sms.repository.DirectorateRepository;

@Service
public class DirectorateServicesImpl implements DirectorateServices {
	@Autowired
	private DirectorateRepository directorateRepository;

	@Override
	public void saveDirectorate(Directorate directorate) {
		directorateRepository.save(directorate);
	}

	@Override
	public List<Directorate> getAllDirectorate() {
		return (List<Directorate>) directorateRepository.findAll();
	}

	@Override
	public Directorate findbyId(Integer directorateId) {
		return directorateRepository.findById(directorateId).get();
	}

	@Override
	public Directorate findByDirectorateName(String directorateName) {
		return directorateRepository.findByDirectorateName(directorateName);
	}

	@Override
	public void changeActive(Integer directorateId) {
		Directorate directorate = directorateRepository.findById(directorateId).get();
		if (directorate.getActive().equals("T")) {
			directorate.setActive("F");
		} else if (directorate.getActive().equals("F")) {
			directorate.setActive("T");
		}

	}

	@Override
	public Directorate Active(Integer directorateId) {
		Directorate directorate=directorateRepository.findById(directorateId).get();
		directorate.setActive("T");
		return directorate;
	}

	@Override
	public Directorate InActive(Integer directorateId) {
		Directorate directorate=directorateRepository.findById(directorateId).get();
		directorate.setActive("F");
		return directorate;
	}

}
