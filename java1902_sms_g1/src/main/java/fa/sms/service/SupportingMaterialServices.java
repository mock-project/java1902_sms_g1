package fa.sms.service;

import java.text.ParseException;
import java.util.List;

import fa.sms.common.valueobjects.SupportMaterialVO;
import fa.sms.entities.OrganisationDetail;
import fa.sms.entities.SupportingMaterial;

public interface SupportingMaterialServices {
  SupportingMaterial saveSupportingMaterial(SupportingMaterial supportingMaterial);

  List<SupportingMaterial> fillAllSupportingMaterial();

  SupportingMaterial fillById(int supportingMaterialId);
  
  public SupportingMaterial convertToSupportingMaterial(SupportMaterialVO smVO) throws ParseException;
  
  public List<SupportingMaterial> findByOrganisationDetailByOrganisationDetailId(OrganisationDetail organisation);
}
