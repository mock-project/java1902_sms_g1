/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.ServiceOrganization;
import fa.sms.repository.ServiceOrganisationRepository;

@Service
public class ServiceOrganizationService {
  private ServiceOrganisationRepository serviceOrganisationRepository;
  
  @Autowired
  public ServiceOrganizationService(
      ServiceOrganisationRepository serviceOrganisationRepository) {
    super();
    this.serviceOrganisationRepository = serviceOrganisationRepository;
  }
  
  public ServiceOrganization save(ServiceOrganization serviceOrganization) {
    return serviceOrganisationRepository.save(serviceOrganization);
  }
  
  
}
