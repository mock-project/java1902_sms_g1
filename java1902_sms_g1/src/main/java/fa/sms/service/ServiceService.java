/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.repository.ServiceRepository;

@Service
public class ServiceService {
  private ServiceRepository serviceRepository;

  @Autowired
  public ServiceService(ServiceRepository serviceRepository) {
    super();
    this.serviceRepository = serviceRepository;
  }
  
  public fa.sms.entities.Service findByName(String serviceName) {
    return serviceRepository.findByServiceName(serviceName);
  }

}
