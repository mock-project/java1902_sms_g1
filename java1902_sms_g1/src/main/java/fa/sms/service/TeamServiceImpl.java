package fa.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.Department;
import fa.sms.entities.Team;
import fa.sms.repository.TeamRepository;
@Service
public class TeamServiceImpl implements TeamService {
	@Autowired
	private TeamRepository teamRepository;

	public Team getTeamById(int id) {

		return teamRepository.findByTeamId(id);
	}

	public List<Team> getAll() {
		List<Team> teams = (List<Team>) teamRepository.findAll();
		return teams;
	}

	public void saveOrUpdate(Team team) {

		if (team.getActive() == null) {
			team.setActive("T");
		}
		teamRepository.save(team);

	}

	@Override
	public List<Team> findByDepartmentByDepartmentId(Department departmentId){
		return teamRepository.findByDepartmentByDepartmentId(departmentId);
	}

	@Override
	public List<Team> findByDepartmentByDepartmentId() {
		// TODO Auto-generated method stub
		return teamRepository.findByDepartmentByDepartmentId();
	};

}
