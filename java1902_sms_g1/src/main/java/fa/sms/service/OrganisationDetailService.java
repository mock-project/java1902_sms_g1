package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.common.valueobjects.OrganisationVO;
import fa.sms.common.valueobjects.OrganisationVOForUpdate;
import fa.sms.entities.Organisation;
import fa.sms.entities.OrganisationDetail;
import fa.sms.repository.OrganisationDetailRepository;


@Service
public class OrganisationDetailService {

	private OrganisationDetailRepository organisationDetailRepository;

	public OrganisationDetail convertToOrganisationDetail(OrganisationVO organisationVO, Organisation organisation) {
		OrganisationDetail organisationDetail = new OrganisationDetail();
		organisationDetail.setOrganisationSpecialism(organisationVO.getOrganisationSpecialism());
		organisationDetail.setServiceDisablitiesCapabilities(organisationVO.getServiceDisablitiesCapabilities());
		organisationDetail.setServiceBarriersCapabilities(organisationVO.getServiceBarriersCapabilities());
		organisationDetail.setServiceBenefitsCapabilities(organisationVO.getServiceBenefitsCapabilities());
		organisationDetail.setServicePersonalCircumstacesCapabilities(
				organisationVO.getServicePersonalCircumstacesCapabilities());
		organisationDetail.setServiceEthnicity(organisationVO.getServiceEthnicity());
		organisationDetail.setAccreditation(organisationVO.getAccreditation());
		organisationDetail.setEoiProgrammes(organisationVO.getEoiProgrammes());
		organisationDetail.setEoiServices(organisationVO.getEoiServices());
		organisationDetail.setOrganisationByOrganisationId(organisation);
		return organisationDetail;
	}

	public OrganisationDetail findByOrganistionId(Organisation organisation) {
		return organisationDetailRepository.findByOrganisationByOrganisationId(organisation);
	}

	public void saveOrganisationDetail(OrganisationDetail organisationDetail) {
		organisationDetailRepository.save(organisationDetail);
	}

	public OrganisationDetail updateForOrganisationDetail(Organisation organisation,
			OrganisationVOForUpdate organisationVOForUpdate) {
		OrganisationDetail updateOrganisationDetail = organisationDetailRepository
				.findByOrganisationByOrganisationId(organisation);
		updateOrganisationDetail.setOrganisationSpecialism(organisationVOForUpdate.getOrganisationSpecialism());
		updateOrganisationDetail
				.setServiceDisablitiesCapabilities(organisationVOForUpdate.getServiceDisablitiesCapabilities());
		updateOrganisationDetail
				.setServiceBarriersCapabilities(organisationVOForUpdate.getServiceBarriersCapabilities());
		updateOrganisationDetail
				.setServiceBenefitsCapabilities(organisationVOForUpdate.getServiceBenefitsCapabilities());
		updateOrganisationDetail.setServicePersonalCircumstacesCapabilities(
				organisationVOForUpdate.getServicePersonalCircumstacesCapabilities());
		updateOrganisationDetail.setServiceEthnicity(organisationVOForUpdate.getServiceEthnicity());
		updateOrganisationDetail.setAccreditation(organisationVOForUpdate.getAccreditation());
		updateOrganisationDetail.setEoiProgrammes(organisationVOForUpdate.getEoiProgrammes());
		updateOrganisationDetail.setEoiServices(organisationVOForUpdate.getEoiServices());
		updateOrganisationDetail.setOrganisationByOrganisationId(organisation);
		return updateOrganisationDetail;
	}

	@Autowired
	public OrganisationDetailService(OrganisationDetailRepository organisationDetailRepository) {
		super();
		this.organisationDetailRepository = organisationDetailRepository;
	}

}
