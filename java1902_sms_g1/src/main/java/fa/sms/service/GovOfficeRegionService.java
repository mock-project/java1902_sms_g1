/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.County;
import fa.sms.entities.GovOfficeRegion;
import fa.sms.repository.GovOfficeRegionRepository;

@Service
public class GovOfficeRegionService {
  private GovOfficeRegionRepository govOfficeRegionRepository;

  public List<GovOfficeRegion> findAllByCounty(County county) {
    return govOfficeRegionRepository.findByCounty(county);
  }
  
  
  @Autowired
  public GovOfficeRegionService(
      GovOfficeRegionRepository govOfficeRegionRepository) {
    super();
    this.govOfficeRegionRepository = govOfficeRegionRepository;
  }
  

}
