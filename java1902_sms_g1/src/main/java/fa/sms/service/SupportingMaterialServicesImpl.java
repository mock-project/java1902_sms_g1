package fa.sms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.common.valueobjects.SupportMaterialVO;
import fa.sms.entities.OrganisationDetail;
import fa.sms.entities.SupportingMaterial;
import fa.sms.repository.SupportingMaterialRepository;
import fa.sms.repository.UserRepository;

@Service
public class SupportingMaterialServicesImpl implements SupportingMaterialServices {
	@Autowired
	private SupportingMaterialRepository supportingMaterialRepository;
	
	private UserRepository userRepository;

	@Override
	public SupportingMaterial saveSupportingMaterial(SupportingMaterial supportingMaterial) {
		return supportingMaterialRepository.save(supportingMaterial);
	}

	@Override
	public List<SupportingMaterial> fillAllSupportingMaterial() {
		return (List<SupportingMaterial>) supportingMaterialRepository.findAll();
	}

	@Override
	public SupportingMaterial fillById(int supportingMaterialId) {
		return supportingMaterialRepository.findById(supportingMaterialId).get();
	}

	@Override
	public SupportingMaterial convertToSupportingMaterial(SupportMaterialVO smVO) throws ParseException {
		SupportingMaterial sm = new SupportingMaterial();
		sm.setActive("T");
		Date date = new SimpleDateFormat("yyyy/MM/dd").parse(smVO.getAddDate());
		sm.setAddDate(date);
		sm.setDescription(smVO.getDescription());
		sm.setUrl(smVO.getUrl());
		sm.setUserByUserId(userRepository.findUserByAccount(smVO.getUserByUserId()));
		sm.setType(smVO.getType());
		return sm;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	@Override
	public List<SupportingMaterial> findByOrganisationDetailByOrganisationDetailId(OrganisationDetail organisation) {
		// TODO Auto-generated method stub
		return supportingMaterialRepository.findByOrganisationDetailByOrganisationDetailId(organisation);
	}
	
	/*
	 * @Autowired public void setEntityManager(EntityManager entityManager) {
	 * this.entityManager = entityManager; }
	 */

	/*
	 * @Override public List<SupportingMaterial>
	 * findByOrganisationDetailByOrganisationDetailId( OrganisationDetail
	 * organisation) { CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	 * CriteriaQuery<SupportingMaterial> query =
	 * cb.createQuery(SupportingMaterial.class); Root<SupportingMaterial> material =
	 * query.from(SupportingMaterial.class); List<Predicate> predicates = new
	 * ArrayList<>(); predicates.add(cb.equal(material.get(
	 * "organisationDetailByOrganisationDetailId"), organisation));
	 * predicates.add(cb.notEqual(material.get(
	 * "organisationDetailByOrganisationDetailId"), null)); query.select(material)
	 * .where(cb.or(predicates.toArray(new Predicate[predicates.size()]))); return
	 * entityManager.createQuery(query) .getResultList(); }
	 */

}
