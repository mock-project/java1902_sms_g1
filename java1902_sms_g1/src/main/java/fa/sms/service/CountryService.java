package fa.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.Country;
import fa.sms.repository.CountryRepository;

@Service
public class CountryService {
	private CountryRepository countryRepository;

	@Autowired
	public CountryService(CountryRepository countryRepository) {
		super();
		this.countryRepository = countryRepository;
	}

	public List<Country> findAll() {
		return countryRepository.findAll();
	}
	
	public Country findById(Integer id) {
		return countryRepository.findById(id).get();
	}
}
