package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fa.sms.common.valueobjects.UserVo;
import fa.sms.entities.MyUserDetail;
import fa.sms.entities.User;
import fa.sms.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  public UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String account)
      throws UsernameNotFoundException {
    User user = userRepository.findUserByAccount(account);
    MyUserDetail userDetail = new MyUserDetail(user);
    return userDetail;
  }

  @Override
  public User getUserByUserVo(UserVo userVo) {
    User user = userRepository.findUserByAccount(userVo.getUsername());
    return user;
  }
  
//  @Cacheable(cacheNames = {"userCache"})
  public User getUser(Integer id) {
    return userRepository.findById(id).get();
  }

}
