package fa.sms.service;

import java.util.List;

import fa.sms.entities.Department;
import fa.sms.entities.Team;

public interface TeamService {
public Team getTeamById(int id);
public List<Team> getAll();
public void saveOrUpdate(Team team);
public List<Team> findByDepartmentByDepartmentId(Department departmentId);
public List<Team> findByDepartmentByDepartmentId();
}
