/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.County;
import fa.sms.repository.CountyRepository;

@Service
public class CountyService {
  private CountyRepository countyRepository;

  @Autowired
  public CountyService(CountyRepository countyRepository) {
    super();
    this.countyRepository = countyRepository;
  }

  public County findByCountyName(String name) {
    return countyRepository.findByCountyName(name);
  }

}
