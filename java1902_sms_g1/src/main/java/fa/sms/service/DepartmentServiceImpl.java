package fa.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.Department;
import fa.sms.repository.DepartmentRepository;

@Service 
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Override
	public List<Department> findAll() {
		// TODO Auto-generated method stub
		List<Department> departments = (List<Department>) departmentRepository.findAll();
		return departments;
	}

	@Override
	public void saveOrUpdate(Department department) {
		// TODO departmentDao-generated method stub
		if(department.getActive()==null)
		{
			department.setActive("T");
		}
		departmentRepository.save(department);
		
	}

	@Override
	public Department getById(int id) {
		// TODO Auto-generated method stub
		Department dept =departmentRepository.findByDepartmentId(id);
		return dept;
	}

	@Override
	public Department findByDepartmentName(String departmentName) {
		// TODO Auto-generated method stub
		Department dept =departmentRepository.findByDepartmentName(departmentName);
		return dept;
	}
	@Override
	public void inActiveDept(int deptId) 
	{
		Department dept =departmentRepository.findByDepartmentId(deptId);
		dept.setActive("F");
		departmentRepository.save(dept);
	}
	@Override
	public void isActiveDept(int deptId)
	{
		Department dept =departmentRepository.findByDepartmentId(deptId);
		dept.setActive("T");
		departmentRepository.save(dept);
	}
	
}
