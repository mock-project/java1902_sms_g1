package fa.sms.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import fa.sms.common.valueobjects.UserVo;
import fa.sms.entities.User;

public interface UserService extends UserDetailsService {
	
	public User getUserByUserVo(UserVo userVo);
	public User getUser(Integer id);
  
}
