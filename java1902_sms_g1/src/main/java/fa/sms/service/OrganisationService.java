package fa.sms.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.common.exception.OrganisationException;
import fa.sms.common.valueobjects.OrganisationVO;
import fa.sms.common.valueobjects.OrganisationVOForUpdate;
import fa.sms.entities.Organisation;
import fa.sms.entities.Premise;
import fa.sms.entities.ServiceOrganization;
import fa.sms.entities.ServicePremise;
import fa.sms.repository.OrganisationRepository;


@Service
public class OrganisationService {
  private OrganisationRepository organisationRepository;
  private BusinessTypeService businessTypeService;
  private ContactService contactService;
  private CountryService countryService;
  private EntityManagerFactory entityManagerFactory;

  public Organisation convertToOrganisation(OrganisationVO organisationVO) {
    Organisation organisation = new Organisation();
    organisation.setOrganisationName(organisationVO.getOrganisationName());
    organisation.setOrganisationShortDescription(
        organisationVO.getOrganisationShortDescription());
    organisation.setAddressLine1(organisationVO.getAddressLine1());
    organisation.setAddressLine2(organisationVO.getAddressLine2());
    organisation.setAddressLine3(organisationVO.getAddressLine3());
    organisation.setPostcode(organisationVO.getPostcode());
    organisation.setTown(organisationVO.getTown());
    organisation.setCounty(organisationVO.getCounty());
    organisation.setCountry(
        countryService.findById(Integer.parseInt(organisationVO.getCountry()))
            .getCountryName());
    organisation
        .setPreferredOrganisation(organisationVO.getPreferredOrganisation());
    organisation.setOrganisationFullDescription(
        organisationVO.getOrganisationFullDescription());
    organisation.setPhoneNumber(organisationVO.getPhoneNumber());
    organisation.setFax(organisationVO.getFax());
    organisation.setEmail(organisationVO.getEmail());
    organisation.setWebAddress(organisationVO.getWebAddress());
    organisation.setCharityNumber(organisationVO.getCharityNumber());
    organisation.setCompanyNumber(organisationVO.getCompanyNumber());
    organisation.setActive("T");
    organisation.setBussinessTypeByBussinessTypeId(businessTypeService
        .getByBusinessName(organisationVO.getBussinessTypeByBussinessTypeId()));
    organisation.setContactByContactId(contactService
        .getByContactName(organisationVO.getContactByContactId()));
    return organisation;
  }

  public Organisation saveOrganisation(Organisation organisation)
      throws OrganisationException {
    if (organisationRepository
        .findByOrganisationName(organisation.getOrganisationName()) != null) {
      throw new OrganisationException("Organisation name already exists!");
    } else {
      return organisationRepository.save(organisation);
    }
  }

  public Organisation saveOrganisationWithOldName(Organisation organisation) {
    return organisationRepository.save(organisation);
  }

  public List<Organisation> getAll() {
    return organisationRepository.findAll();
  }

  public Organisation findById(Integer id) {
    return organisationRepository.findById(id).get();
  }

  @Transactional
  public Set<Premise> findAllPremise(int organisationId) {
    Optional<Organisation> optional = organisationRepository
        .findById(organisationId);
    Organisation organisation = optional.get();
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.merge(organisation);
    List<ServiceOrganization> serviceOrganizations = (List<ServiceOrganization>) organisation
        .getServiceOrganizationsByOrganisationId();
    List<fa.sms.entities.Service> services = new ArrayList<fa.sms.entities.Service>();
    for (ServiceOrganization so : serviceOrganizations) {
      services.add(so.getServiceByServiceId());
    }

    List<ServicePremise> servicePremises = new ArrayList<ServicePremise>();
    for (fa.sms.entities.Service s : services) {
      servicePremises.addAll(s.getServicePremisesByServiceId());
    }

    Set<Premise> premises = new HashSet<Premise>();

    for (ServicePremise sp : servicePremises) {
      premises.add(sp.getPremiseByPremisesId());
    }
    return premises;
  }

  public Organisation updateForOrganisation(Organisation organisation,
      OrganisationVOForUpdate organisationVOForUpdate) {
    organisation
        .setOrganisationName(organisationVOForUpdate.getOrganisationName());
    organisation.setOrganisationShortDescription(
        organisationVOForUpdate.getOrganisationShortDescription());
    organisation.setAddressLine1(organisationVOForUpdate.getAddressLine1());
    organisation.setAddressLine2(organisationVOForUpdate.getAddressLine2());
    organisation.setAddressLine3(organisationVOForUpdate.getAddressLine3());
    organisation.setPostcode(organisationVOForUpdate.getPostcode());
    organisation.setTown(organisationVOForUpdate.getTown());
    organisation.setCounty(organisationVOForUpdate.getCounty());
    organisation.setCountry(countryService
        .findById(Integer.parseInt(organisationVOForUpdate.getCountry()))
        .getCountryName());
    organisation.setPreferredOrganisation(
        organisationVOForUpdate.getPreferredOrganisation());
    organisation.setOrganisationFullDescription(
        organisationVOForUpdate.getOrganisationFullDescription());
    organisation.setPhoneNumber(organisationVOForUpdate.getPhoneNumber());
    organisation.setFax(organisationVOForUpdate.getFax());
    organisation.setEmail(organisationVOForUpdate.getEmail());
    organisation.setWebAddress(organisationVOForUpdate.getWebAddress());
    organisation.setCharityNumber(organisationVOForUpdate.getCharityNumber());
    organisation.setCompanyNumber(organisationVOForUpdate.getCompanyNumber());
    organisation.setGovOfficeRegionName(
        organisationVOForUpdate.getGovOfficeRegionName());
    organisation
        .setTrustRegionName(organisationVOForUpdate.getTrustRegionName());
    organisation
        .setTrustDistrictName(organisationVOForUpdate.getTrustDistrictName());
    return organisation;
  }

  @Autowired
  public OrganisationService(OrganisationRepository organisationRepository) {
    super();
    this.organisationRepository = organisationRepository;
  }

  public BusinessTypeService getBusinessTypeService() {
    return businessTypeService;
  }

  @Autowired
  public void setBusinessTypeService(BusinessTypeService businessTypeService) {
    this.businessTypeService = businessTypeService;
  }

  public ContactService getContactService() {
    return contactService;
  }

  @Autowired
  public void setContactService(ContactService contactService) {
    this.contactService = contactService;
  }

  public CountryService getCountryService() {
    return countryService;
  }

  @Autowired
  public void setCountryService(CountryService countryService) {
    this.countryService = countryService;
  }

  public EntityManagerFactory getEntityManagerFactory() {
    return entityManagerFactory;
  }

  @Autowired
  public void setEntityManagerFactory(
      EntityManagerFactory entityManagerFactory) {
    this.entityManagerFactory = entityManagerFactory;
  }

}
