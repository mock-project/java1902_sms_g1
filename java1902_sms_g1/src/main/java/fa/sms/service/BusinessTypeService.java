package fa.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.sms.entities.BussinessType;
import fa.sms.repository.BusinessTypeRepository;

@Service
public class BusinessTypeService {
	private BusinessTypeRepository businessTypeRepository;

	public BussinessType getByBusinessName(String businessName) {
		return businessTypeRepository.findByBussinessName(businessName);
	}

	@Autowired
	public BusinessTypeService(BusinessTypeRepository businessTypeRepository) {
		super();
		this.businessTypeRepository = businessTypeRepository;
	}

}
