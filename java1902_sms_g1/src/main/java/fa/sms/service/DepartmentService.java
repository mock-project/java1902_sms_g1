package fa.sms.service;

import java.util.List;

import fa.sms.entities.Department;

public interface DepartmentService {
	public List<Department> findAll();
	public void saveOrUpdate(Department department);
	public Department getById(int id);
	public Department findByDepartmentName(String departmentName);
	public void inActiveDept(int deptId);
	public void isActiveDept(int deptId);
	

}
