package fa.sms.common.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Logging {

  public static Logger getLog() {
    return LogManager.getLogger(Logging.class);
  }
}
