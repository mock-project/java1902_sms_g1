package fa.sms.common.utility;

import java.util.ArrayList;
import java.util.List;

public class HelperConvert {
	public static List<String> convertOrganisationDetails(String data) {
		List<String> convert = new ArrayList<String>();
		if (data.isEmpty()) {
			return convert;
		} else {
			String[] values = data.split(", ");
			for (String specificData : values) {
				convert.add(specificData);
			}
		}
		return convert;
	}
}
