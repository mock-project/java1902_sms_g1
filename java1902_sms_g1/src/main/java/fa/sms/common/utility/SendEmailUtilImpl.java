package fa.sms.common.utility;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import fa.sms.common.logging.Logging;
import fa.sms.entities.User;
import fa.sms.repository.UserRepository;

@Component
public class SendEmailUtilImpl implements SendEmailUtil{

	@Autowired
	private JavaMailSender sender;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public boolean sendPasswordToUser(User user) {
		String password = UUID.randomUUID().toString();
		user.setPassword(encoder.encode(password));
		SimpleMailMessage mailMessage  = new SimpleMailMessage();
		mailMessage.setText("Your password is: "+ password);
		mailMessage.setTo(user.getEmail());
		mailMessage.setFrom("marocvi89@gmail.com");
		mailMessage.setSubject("SMS FORGET PASSWORD");
		try {
			sender.send(mailMessage);
			userRepository.save(user);
			return true;
		}
		catch (Exception e) {
			Logging.getLog().error(e.getMessage());
			return false;
		}

	}

}
