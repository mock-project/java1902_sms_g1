package fa.sms.common.exception;

public class OrganisationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrganisationException(String message) {
		super(message);
	}

}
