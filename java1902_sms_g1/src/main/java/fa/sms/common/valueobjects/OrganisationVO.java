package fa.sms.common.valueobjects;

public class OrganisationVO {
  private String organisationName;
  private String organisationShortDescription;
  private String addressLine1;
  private String addressLine2;
  private String addressLine3;
  private String postcode;
  private String town;
  private String county;
  private String country;
  private String preferredOrganisation;
  private String organisationFullDescription;
  private String phoneNumber;
  private String fax;
  private String email;
  private String webAddress;
  private String charityNumber;
  private String companyNumber;
  private String bussinessTypeByBussinessTypeId;
  private String contactByContactId;
  private String organisationSpecialism;
  private String serviceDisablitiesCapabilities;
  private String serviceBarriersCapabilities;
  private String serviceBenefitsCapabilities;
  private String servicePersonalCircumstacesCapabilities;
  private String serviceEthnicity;
  private String accreditation;
  private String eoiProgrammes;
  private String eoiServices;
  private String csrf;

  public OrganisationVO(String organisationName,
      String organisationShortDescription, String addressLine1,
      String addressLine2, String addressLine3, String postcode, String town,
      String county, String country, String preferredOrganisation,
      String organisationFullDescription, String phoneNumber, String fax,
      String email, String webAddress, String charityNumber,
      String companyNumber, String bussinessTypeByBussinessTypeId,
      String contactByContactId, String organisationSpecialism,
      String serviceDisablitiesCapabilities, String serviceBarriersCapabilities,
      String serviceBenefitsCapabilities,
      String servicePersonalCircumstacesCapabilities, String serviceEthnicity,
      String accreditation, String eoiProgrammes, String eoiServices,
      String csrf) {
    super();
    this.organisationName = organisationName;
    this.organisationShortDescription = organisationShortDescription;
    this.addressLine1 = addressLine1;
    this.addressLine2 = addressLine2;
    this.addressLine3 = addressLine3;
    this.postcode = postcode;
    this.town = town;
    this.county = county;
    this.country = country;
    this.preferredOrganisation = preferredOrganisation;
    this.organisationFullDescription = organisationFullDescription;
    this.phoneNumber = phoneNumber;
    this.fax = fax;
    this.email = email;
    this.webAddress = webAddress;
    this.charityNumber = charityNumber;
    this.companyNumber = companyNumber;
    this.bussinessTypeByBussinessTypeId = bussinessTypeByBussinessTypeId;
    this.contactByContactId = contactByContactId;
    this.organisationSpecialism = organisationSpecialism;
    this.serviceDisablitiesCapabilities = serviceDisablitiesCapabilities;
    this.serviceBarriersCapabilities = serviceBarriersCapabilities;
    this.serviceBenefitsCapabilities = serviceBenefitsCapabilities;
    this.servicePersonalCircumstacesCapabilities = servicePersonalCircumstacesCapabilities;
    this.serviceEthnicity = serviceEthnicity;
    this.accreditation = accreditation;
    this.eoiProgrammes = eoiProgrammes;
    this.eoiServices = eoiServices;
    this.csrf = csrf;
  }

  public String getOrganisationName() {
    return organisationName;
  }

  public void setOrganisationName(String organisationName) {
    this.organisationName = organisationName;
  }

  public String getOrganisationShortDescription() {
    return organisationShortDescription;
  }

  public void setOrganisationShortDescription(
      String organisationShortDescription) {
    this.organisationShortDescription = organisationShortDescription;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getAddressLine3() {
    return addressLine3;
  }

  public void setAddressLine3(String addressLine3) {
    this.addressLine3 = addressLine3;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPreferredOrganisation() {
    return preferredOrganisation;
  }

  public void setPreferredOrganisation(String preferredOrganisation) {
    this.preferredOrganisation = preferredOrganisation;
  }

  public String getOrganisationFullDescription() {
    return organisationFullDescription;
  }

  public void setOrganisationFullDescription(
      String organisationFullDescription) {
    this.organisationFullDescription = organisationFullDescription;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getWebAddress() {
    return webAddress;
  }

  public void setWebAddress(String webAddress) {
    this.webAddress = webAddress;
  }

  public String getCharityNumber() {
    return charityNumber;
  }

  public void setCharityNumber(String charityNumber) {
    this.charityNumber = charityNumber;
  }

  public String getCompanyNumber() {
    return companyNumber;
  }

  public void setCompanyNumber(String companyNumber) {
    this.companyNumber = companyNumber;
  }

  public String getBussinessTypeByBussinessTypeId() {
    return bussinessTypeByBussinessTypeId;
  }

  public void setBussinessTypeByBussinessTypeId(
      String bussinessTypeByBussinessTypeId) {
    this.bussinessTypeByBussinessTypeId = bussinessTypeByBussinessTypeId;
  }

  public String getContactByContactId() {
    return contactByContactId;
  }

  public void setContactByContactId(String contactByContactId) {
    this.contactByContactId = contactByContactId;
  }

  public String getOrganisationSpecialism() {
    return organisationSpecialism;
  }

  public void setOrganisationSpecialism(String organisationSpecialism) {
    this.organisationSpecialism = organisationSpecialism;
  }

  public String getServiceDisablitiesCapabilities() {
    return serviceDisablitiesCapabilities;
  }

  public void setServiceDisablitiesCapabilities(
      String serviceDisablitiesCapabilities) {
    this.serviceDisablitiesCapabilities = serviceDisablitiesCapabilities;
  }

  public String getServiceBarriersCapabilities() {
    return serviceBarriersCapabilities;
  }

  public void setServiceBarriersCapabilities(
      String serviceBarriersCapabilities) {
    this.serviceBarriersCapabilities = serviceBarriersCapabilities;
  }

  public String getServiceBenefitsCapabilities() {
    return serviceBenefitsCapabilities;
  }

  public void setServiceBenefitsCapabilities(
      String serviceBenefitsCapabilities) {
    this.serviceBenefitsCapabilities = serviceBenefitsCapabilities;
  }

  public String getServicePersonalCircumstacesCapabilities() {
    return servicePersonalCircumstacesCapabilities;
  }

  public void setServicePersonalCircumstacesCapabilities(
      String servicePersonalCircumstacesCapabilities) {
    this.servicePersonalCircumstacesCapabilities = servicePersonalCircumstacesCapabilities;
  }

  public String getServiceEthnicity() {
    return serviceEthnicity;
  }

  public void setServiceEthnicity(String serviceEthnicity) {
    this.serviceEthnicity = serviceEthnicity;
  }

  public String getAccreditation() {
    return accreditation;
  }

  public void setAccreditation(String accreditation) {
    this.accreditation = accreditation;
  }

  public String getEoiProgrammes() {
    return eoiProgrammes;
  }

  public void setEoiProgrammes(String eoiProgrammes) {
    this.eoiProgrammes = eoiProgrammes;
  }

  public String getEoiServices() {
    return eoiServices;
  }

  public void setEoiServices(String eoiServices) {
    this.eoiServices = eoiServices;
  }

  public String getCsrf() {
    return csrf;
  }

  public void setCsrf(String csrf) {
    this.csrf = csrf;
  }

}