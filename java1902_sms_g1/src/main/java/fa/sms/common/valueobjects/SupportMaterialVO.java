/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.common.valueobjects;

public class SupportMaterialVO {
	private String url;
	private String description;
	private String type;
	private String addDate;
	private String userByUserId;
	private String csrf;

	public SupportMaterialVO(String url, String description, String type, String addDate, String userByUserId,
			String csrf) {
		super();
		this.url = url;
		this.description = description;
		this.type = type;
		this.addDate = addDate;
		this.userByUserId = userByUserId;
		this.csrf = csrf;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	public String getUserByUserId() {
		return userByUserId;
	}

	public void setUserByUserId(String userByUserId) {
		this.userByUserId = userByUserId;
	}

	public String getCsrf() {
		return csrf;
	}

	public void setCsrf(String csrf) {
		this.csrf = csrf;
	}

}
