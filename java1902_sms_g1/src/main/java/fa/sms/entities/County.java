package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@IdClass(CountyPK.class)
public class County {
  private int countyId;
  private String countyName;
  private Country countryByCountryId;
  private Collection<GovOfficeRegion> govOfficeRegions;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "county_id", nullable = false)
  public int getCountyId() {
    return countyId;
  }

  public void setCountyId(int countyId) {
    this.countyId = countyId;
  }



  @Basic
  @Column(name = "county_name", nullable = false, length = 100)
  public String getCountyName() {
    return countyName;
  }

  public void setCountyName(String countyName) {
    this.countyName = countyName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    County county = (County) o;
    return countyId == county.countyId &&
            Objects.equals(countyName, county.countyName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(countyId, countyName);
  }

  @ManyToOne
  @Id
  @JoinColumn(name = "country_id", referencedColumnName = "country_id", nullable = false)
  public Country getCountryByCountryId() {
    return countryByCountryId;
  }

  public void setCountryByCountryId(Country countryByCountryId) {
    this.countryByCountryId = countryByCountryId;
  }

  @OneToMany(mappedBy = "county")
  public Collection<GovOfficeRegion> getGovOfficeRegions() {
    return govOfficeRegions;
  }

  public void setGovOfficeRegions(Collection<GovOfficeRegion> govOfficeRegions) {
    this.govOfficeRegions = govOfficeRegions;
  }
}
