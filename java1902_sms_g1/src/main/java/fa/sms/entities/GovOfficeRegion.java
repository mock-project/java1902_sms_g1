package fa.sms.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class GovOfficeRegion {
	private int govOfficeRegionId;
	private String govOfficeRegionName;
	private County county;

	@Id
	@Column(name = "gov_office_region_id", nullable = false)
	public int getGovOfficeRegionId() {
		return govOfficeRegionId;
	}

	public void setGovOfficeRegionId(int govOfficeRegionId) {
		this.govOfficeRegionId = govOfficeRegionId;
	}

	@Basic
	@Column(name = "gov_office_region_name", nullable = false, length = 100)
	public String getGovOfficeRegionName() {
		return govOfficeRegionName;
	}

	public void setGovOfficeRegionName(String govOfficeRegionName) {
		this.govOfficeRegionName = govOfficeRegionName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		GovOfficeRegion that = (GovOfficeRegion) o;
		return govOfficeRegionId == that.govOfficeRegionId
				&& Objects.equals(govOfficeRegionName, that.govOfficeRegionName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(govOfficeRegionId, govOfficeRegionName);
	}

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "county_id", referencedColumnName = "county_id", nullable = false),
			@JoinColumn(name = "country_id", referencedColumnName = "country_id", nullable = false) })
	public County getCounty() {
		return county;
	}

	public void setCounty(County county) {
		this.county = county;
	}
}
