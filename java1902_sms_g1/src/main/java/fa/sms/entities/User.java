package fa.sms.entities;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "[User]",schema = "organisation",catalog = "SMS")
@Cacheable
public class User {
  private int userId;
  private String account;
  private String email;
  private String role;
  private String password;
  private Collection<SupportingMaterial> supportingMaterialsByUserId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id", nullable = false)
  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  @Basic
  @Column(name = "account", nullable = false, length = 255)
  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  @Basic
  @Column(name = "email", nullable = true, length = 255)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "role", nullable = true)
  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Basic
  @Column(name = "password", nullable = false, length = 200)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return userId == user.userId &&
            Objects.equals(account, user.account) &&
            Objects.equals(email, user.email) &&
            Objects.equals(role, user.role) &&
            Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, account, email, role, password);
  }

  @OneToMany(mappedBy = "userByUserId")
  public Collection<SupportingMaterial> getSupportingMaterialsByUserId() {
    return supportingMaterialsByUserId;
  }

  public void setSupportingMaterialsByUserId(Collection<SupportingMaterial> supportingMaterialsByUserId) {
    this.supportingMaterialsByUserId = supportingMaterialsByUserId;
  }
}
