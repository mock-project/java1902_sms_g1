package fa.sms.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class ServiceOrganization {
  private int serviceOrganizationId;
  private String roles;
  private Service serviceByServiceId;
  private Organisation organisationByOrganizationId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "service_organization_id", nullable = false)
  public int getServiceOrganizationId() {
    return serviceOrganizationId;
  }

  public void setServiceOrganizationId(int serviceOrganizationId) {
    this.serviceOrganizationId = serviceOrganizationId;
  }


  @Basic
  @Column(name = "roles", nullable = true, length = 100)
  public String getRoles() {
    return roles;
  }

  public void setRoles(String roles) {
    this.roles = roles;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ServiceOrganization that = (ServiceOrganization) o;
    return serviceOrganizationId == that.serviceOrganizationId &&
            Objects.equals(roles, that.roles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceOrganizationId, roles);
  }

  @ManyToOne
  @JoinColumn(name = "service_id", referencedColumnName = "service_id", nullable = false)
  public Service getServiceByServiceId() {
    return serviceByServiceId;
  }

  public void setServiceByServiceId(Service serviceByServiceId) {
    this.serviceByServiceId = serviceByServiceId;
  }

  @ManyToOne
  @JoinColumn(name = "organization_id", referencedColumnName = "organisation_id", nullable = false)
  public Organisation getOrganisationByOrganizationId() {
    return organisationByOrganizationId;
  }

  public void setOrganisationByOrganizationId(Organisation organisationByOrganizationId) {
    this.organisationByOrganizationId = organisationByOrganizationId;
  }
}
