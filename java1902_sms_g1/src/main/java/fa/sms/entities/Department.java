package fa.sms.entities;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Department", schema = "organisation", catalog = "SMS")
public class Department {
	private int departmentId;
	private String departmentName;
	private String departmentShortDescription;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String postcode;
	private String town;
	private String county;
	private String country;
	private String departmentFullDescription;
	private String phoneNumber;
	private String fax;
	private String email;
	private String webAddress;
	private String active;
	private BussinessType bussinessTypeByBussinessTypeId;
	private Contact contactByContactId;
	private Directorate directorateByDirectorateId;
	private Collection<Team> teamsByDepartmentId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "department_id", nullable = false)
	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	@Basic
	@Column(name = "department_name", nullable = false, length = 50, unique = true)
	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@Basic
	@Column(name = "department_short_description", nullable = false, length = 200)
	public String getDepartmentShortDescription() {
		return departmentShortDescription;
	}

	public void setDepartmentShortDescription(String departmentShortDescription) {
		this.departmentShortDescription = departmentShortDescription;
	}

	@Basic
	@Column(name = "address_line1", nullable = true, length = 100)
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	@Basic
	@Column(name = "address_line2", nullable = true, length = 100)
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Basic
	@Column(name = "address_line3", nullable = true, length = 100)
	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	@Basic
	@Column(name = "postcode", nullable = true, length = 20)
	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	@Basic
	@Column(name = "town", nullable = true, length = 50)
	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	@Basic
	@Column(name = "county", nullable = true, length = 50)
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Basic
	@Column(name = "country", nullable = true, length = 50)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Basic
	@Column(name = "department_full_description", nullable = true, length = 1000)
	public String getDepartmentFullDescription() {
		return departmentFullDescription;
	}

	public void setDepartmentFullDescription(String departmentFullDescription) {
		this.departmentFullDescription = departmentFullDescription;
	}

	@Basic
	@Column(name = "phone_number", nullable = true, length = 20)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Basic
	@Column(name = "fax", nullable = true, length = 100)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Basic
	@Column(name = "email", nullable = true, length = 200)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Basic
	@Column(name = "web_address", nullable = true, length = 200)
	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	@Basic
	@Column(name = "active", nullable = true, length = 1)
	@ColumnDefault("'T'")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Department that = (Department) o;
		return departmentId == that.departmentId && Objects.equals(departmentName, that.departmentName)
				&& Objects.equals(departmentShortDescription, that.departmentShortDescription)
				&& Objects.equals(addressLine1, that.addressLine1) && Objects.equals(addressLine2, that.addressLine2)
				&& Objects.equals(addressLine3, that.addressLine3) && Objects.equals(postcode, that.postcode)
				&& Objects.equals(town, that.town) && Objects.equals(county, that.county)
				&& Objects.equals(country, that.country)
				&& Objects.equals(departmentFullDescription, that.departmentFullDescription)
				&& Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(fax, that.fax)
				&& Objects.equals(email, that.email) && Objects.equals(webAddress, that.webAddress)
				&& Objects.equals(active, that.active);
	}

	@Override
	public int hashCode() {
		return Objects.hash(departmentId, departmentName, departmentShortDescription, addressLine1, addressLine2,
				addressLine3, postcode, town, county, country, departmentFullDescription, phoneNumber, fax, email,
				webAddress, active);
	}

	@ManyToOne
	@JoinColumn(name = "bussiness_type_id", referencedColumnName = "bussiness_type_id")
	public BussinessType getBussinessTypeByBussinessTypeId() {
		return bussinessTypeByBussinessTypeId;
	}

	public void setBussinessTypeByBussinessTypeId(BussinessType bussinessTypeByBussinessTypeId) {
		this.bussinessTypeByBussinessTypeId = bussinessTypeByBussinessTypeId;
	}

	@ManyToOne
	@JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
	public Contact getContactByContactId() {
		return contactByContactId;
	}

	public void setContactByContactId(Contact contactByContactId) {
		this.contactByContactId = contactByContactId;
	}

	@ManyToOne
	@JoinColumn(name = "directorate_id", referencedColumnName = "directorate_id")
	public Directorate getDirectorateByDirectorateId() {
		return directorateByDirectorateId;
	}

	public void setDirectorateByDirectorateId(Directorate directorateByDirectorateId) {
		this.directorateByDirectorateId = directorateByDirectorateId;
	}

	@OneToMany(mappedBy = "departmentByDepartmentId")
	public Collection<Team> getTeamsByDepartmentId() {
		return teamsByDepartmentId;
	}

	public void setTeamsByDepartmentId(Collection<Team> teamsByDepartmentId) {
		this.teamsByDepartmentId = teamsByDepartmentId;
	}
}
