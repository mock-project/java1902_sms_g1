package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Organisation_Detail", schema = "organisation", catalog = "SMS")
public class OrganisationDetail {
	private int organisationDetailId;
	private String organisationSpecialism;
	private String serviceDisablitiesCapabilities;
	private String serviceBarriersCapabilities;
	private String serviceBenefitsCapabilities;
	private String servicePersonalCircumstacesCapabilities;
	private String serviceEthnicity;
	private String accreditation;
	private String eoiProgrammes;
	private String eoiServices;
	private Collection<Directorate> directoratesByOrganisationDetailId;
	private Organisation organisationByOrganisationId;
	private Collection<SupportingMaterial> supportingMaterialsByOrganisationDetailId;

	public OrganisationDetail() {
		super();
	}

	public OrganisationDetail(String organisationSpecialism, String serviceDisablitiesCapabilities,
			String serviceBarriersCapabilities, String serviceBenefitsCapabilities,
			String servicePersonalCircumstacesCapabilities, String serviceEthnicity, String accreditation,
			String eoiProgrammes, String eoiServices, Organisation organisationByOrganisationId) {
		super();
		this.organisationSpecialism = organisationSpecialism;
		this.serviceDisablitiesCapabilities = serviceDisablitiesCapabilities;
		this.serviceBarriersCapabilities = serviceBarriersCapabilities;
		this.serviceBenefitsCapabilities = serviceBenefitsCapabilities;
		this.servicePersonalCircumstacesCapabilities = servicePersonalCircumstacesCapabilities;
		this.serviceEthnicity = serviceEthnicity;
		this.accreditation = accreditation;
		this.eoiProgrammes = eoiProgrammes;
		this.eoiServices = eoiServices;
		this.organisationByOrganisationId = organisationByOrganisationId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "organisation_detail_id", nullable = false)
	public int getOrganisationDetailId() {
		return organisationDetailId;
	}

	public void setOrganisationDetailId(int organisationDetailId) {
		this.organisationDetailId = organisationDetailId;
	}

	@Basic
	@Column(name = "organisation_specialism", nullable = true, length = 1000)
	public String getOrganisationSpecialism() {
		return organisationSpecialism;
	}

	public void setOrganisationSpecialism(String organisationSpecialism) {
		this.organisationSpecialism = organisationSpecialism;
	}
	
	

	@Basic
	@Column(name = "service_disablities_capabilities", nullable = true, length = 1000)
	public String getServiceDisablitiesCapabilities() {
		return serviceDisablitiesCapabilities;
	}

	public void setServiceDisablitiesCapabilities(String serviceDisablitiesCapabilities) {
		this.serviceDisablitiesCapabilities = serviceDisablitiesCapabilities;
	}

	@Basic
	@Column(name = "service_barriers_capabilities", nullable = true, length = 1000)
	public String getServiceBarriersCapabilities() {
		return serviceBarriersCapabilities;
	}

	public void setServiceBarriersCapabilities(String serviceBarriersCapabilities) {
		this.serviceBarriersCapabilities = serviceBarriersCapabilities;
	}

	@Basic
	@Column(name = "service_benefits_capabilities", nullable = true, length = 1000)
	public String getServiceBenefitsCapabilities() {
		return serviceBenefitsCapabilities;
	}

	public void setServiceBenefitsCapabilities(String serviceBenefitsCapabilities) {
		this.serviceBenefitsCapabilities = serviceBenefitsCapabilities;
	}

	@Basic
	@Column(name = "service_personal_circumstaces_capabilities", nullable = true, length = 1000)
	public String getServicePersonalCircumstacesCapabilities() {
		return servicePersonalCircumstacesCapabilities;
	}

	public void setServicePersonalCircumstacesCapabilities(String servicePersonalCircumstacesCapabilities) {
		this.servicePersonalCircumstacesCapabilities = servicePersonalCircumstacesCapabilities;
	}

	@Basic
	@Column(name = "service_ethnicity", nullable = true, length = 1000)
	public String getServiceEthnicity() {
		return serviceEthnicity;
	}

	public void setServiceEthnicity(String serviceEthnicity) {
		this.serviceEthnicity = serviceEthnicity;
	}

	@Basic
	@Column(name = "accreditation", nullable = true, length = 1000)
	public String getAccreditation() {
		return accreditation;
	}

	public void setAccreditation(String accreditation) {
		this.accreditation = accreditation;
	}

	@Basic
	@Column(name = "eoi_programmes", nullable = true, length = 1000)
	public String getEoiProgrammes() {
		return eoiProgrammes;
	}

	public void setEoiProgrammes(String eoiProgrammes) {
		this.eoiProgrammes = eoiProgrammes;
	}

	@Basic
	@Column(name = "eoi_services", nullable = true, length = 1000)
	public String getEoiServices() {
		return eoiServices;
	}

	public void setEoiServices(String eoiServices) {
		this.eoiServices = eoiServices;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		OrganisationDetail that = (OrganisationDetail) o;
		return organisationDetailId == that.organisationDetailId
				&& Objects.equals(organisationSpecialism, that.organisationSpecialism)
				&& Objects.equals(serviceDisablitiesCapabilities, that.serviceDisablitiesCapabilities)
				&& Objects.equals(serviceBarriersCapabilities, that.serviceBarriersCapabilities)
				&& Objects.equals(serviceBenefitsCapabilities, that.serviceBenefitsCapabilities)
				&& Objects.equals(servicePersonalCircumstacesCapabilities, that.servicePersonalCircumstacesCapabilities)
				&& Objects.equals(serviceEthnicity, that.serviceEthnicity)
				&& Objects.equals(accreditation, that.accreditation)
				&& Objects.equals(eoiProgrammes, that.eoiProgrammes) && Objects.equals(eoiServices, that.eoiServices);
	}

	@Override
	public int hashCode() {
		return Objects.hash(organisationDetailId, organisationSpecialism, serviceDisablitiesCapabilities,
				serviceBarriersCapabilities, serviceBenefitsCapabilities, servicePersonalCircumstacesCapabilities,
				serviceEthnicity, accreditation, eoiProgrammes, eoiServices);
	}

	@OneToMany(mappedBy = "organisationDetailByOrganisationDetailId")
	public Collection<Directorate> getDirectoratesByOrganisationDetailId() {
		return directoratesByOrganisationDetailId;
	}

	public void setDirectoratesByOrganisationDetailId(Collection<Directorate> directoratesByOrganisationDetailId) {
		this.directoratesByOrganisationDetailId = directoratesByOrganisationDetailId;
	}

	@OneToOne
	@JoinColumn(name = "organisation_id", nullable = false)
	public Organisation getOrganisationByOrganisationId() {
		return organisationByOrganisationId;
	}

	public void setOrganisationByOrganisationId(Organisation organisationByOrganisationId) {
		this.organisationByOrganisationId = organisationByOrganisationId;
	}

	@OneToMany(mappedBy = "organisationDetailByOrganisationDetailId")
	public Collection<SupportingMaterial> getSupportingMaterialsByOrganisationDetailId() {
		return supportingMaterialsByOrganisationDetailId;
	}

	public void setSupportingMaterialsByOrganisationDetailId(
			Collection<SupportingMaterial> supportingMaterialsByOrganisationDetailId) {
		this.supportingMaterialsByOrganisationDetailId = supportingMaterialsByOrganisationDetailId;
	}
}
