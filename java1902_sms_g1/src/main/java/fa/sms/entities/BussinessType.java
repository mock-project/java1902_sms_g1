package fa.sms.entities;

import java.util.Collection;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Bussiness_Type", schema = "dbo", catalog = "SMS")
public class BussinessType {
	private int bussinessTypeId;
	private String bussinessName;
	private String sicCode;
	private Collection<Department> departmentsByBussinessTypeId;
	private Collection<Directorate> directoratesByBussinessTypeId;
	private Collection<Organisation> organisationsByBussinessTypeId;
	private Collection<Team> teamsByBussinessTypeId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bussiness_type_id", nullable = false)
	public int getBussinessTypeId() {
		return bussinessTypeId;
	}

	public void setBussinessTypeId(int bussinessTypeId) {
		this.bussinessTypeId = bussinessTypeId;
	}

	@Basic
	@Column(name = "bussiness_name", nullable = true, length = 100)
	public String getBussinessName() {
		return bussinessName;
	}

	public void setBussinessName(String bussinessName) {
		this.bussinessName = bussinessName;
	}

	@Basic
	@Column(name = "sic_code", nullable = true, length = 50)
	public String getSicCode() {
		return sicCode;
	}

	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		BussinessType that = (BussinessType) o;
		return bussinessTypeId == that.bussinessTypeId && Objects.equals(bussinessName, that.bussinessName)
				&& Objects.equals(sicCode, that.sicCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(bussinessTypeId, bussinessName, sicCode);
	}

	@OneToMany(mappedBy = "bussinessTypeByBussinessTypeId")
	public Collection<Department> getDepartmentsByBussinessTypeId() {
		return departmentsByBussinessTypeId;
	}

	public void setDepartmentsByBussinessTypeId(Collection<Department> departmentsByBussinessTypeId) {
		this.departmentsByBussinessTypeId = departmentsByBussinessTypeId;
	}

	@OneToMany(mappedBy = "bussinessTypeByBussinessTypeId")
	public Collection<Directorate> getDirectoratesByBussinessTypeId() {
		return directoratesByBussinessTypeId;
	}

	public void setDirectoratesByBussinessTypeId(Collection<Directorate> directoratesByBussinessTypeId) {
		this.directoratesByBussinessTypeId = directoratesByBussinessTypeId;
	}

	@OneToMany(mappedBy = "bussinessTypeByBussinessTypeId")
	public Collection<Organisation> getOrganisationsByBussinessTypeId() {
		return organisationsByBussinessTypeId;
	}

	public void setOrganisationsByBussinessTypeId(Collection<Organisation> organisationsByBussinessTypeId) {
		this.organisationsByBussinessTypeId = organisationsByBussinessTypeId;
	}

	@OneToMany(mappedBy = "bussinessTypeByBussinessTypeId")
	public Collection<Team> getTeamsByBussinessTypeId() {
		return teamsByBussinessTypeId;
	}

	public void setTeamsByBussinessTypeId(Collection<Team> teamsByBussinessTypeId) {
		this.teamsByBussinessTypeId = teamsByBussinessTypeId;
	}
}
