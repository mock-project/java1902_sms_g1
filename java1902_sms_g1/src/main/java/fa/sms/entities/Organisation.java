package fa.sms.entities;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Organisation", schema = "organisation", catalog = "SMS")
public class Organisation {
	private int organisationId;
	private String organisationName;
	private String organisationShortDescription;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String postcode;
	private String town;
	private String county;
	private String country;
	private String preferredOrganisation;
	private String organisationFullDescription;
	private String phoneNumber;
	private String fax;
	private String email;
	private String webAddress;
	private String charityNumber;
	private String companyNumber;
	private String active;
	private BussinessType bussinessTypeByBussinessTypeId;
	private Contact contactByContactId;
	private OrganisationDetail organisationDetail;
	private String govOfficeRegionName;
	private String trustRegionName;
	private String trustDistrictName;
	private Collection<ServiceOrganization> serviceOrganizationsByOrganisationId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "organisation_id", nullable = false)
	public int getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(int organisationId) {
		this.organisationId = organisationId;
	}

	@Basic
	@Column(name = "organisation_name", nullable = false, length = 50, unique = true)
	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	@Basic
	@Column(name = "organisation_short_description", nullable = false, length = 200)
	public String getOrganisationShortDescription() {
		return organisationShortDescription;
	}

	public void setOrganisationShortDescription(String organisationShortDescription) {
		this.organisationShortDescription = organisationShortDescription;
	}

	@Basic
	@Column(name = "address_line1", nullable = false, length = 100)
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	@Basic
	@Column(name = "address_line2", nullable = true, length = 100)
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Basic
	@Column(name = "address_line3", nullable = true, length = 100)
	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	@Basic
	@Column(name = "postcode", nullable = false, length = 20)
	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	@Basic
	@Column(name = "town", nullable = true, length = 50)
	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	@Basic
	@Column(name = "county", nullable = true, length = 50)
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Basic
	@Column(name = "country", nullable = true, length = 50)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Basic
	@Column(name = "preferred_organisation", nullable = true, length = 1)
	public String getPreferredOrganisation() {
		return preferredOrganisation;
	}

	public void setPreferredOrganisation(String preferredOrganisation) {
		this.preferredOrganisation = preferredOrganisation;
	}

	@Basic
	@Column(name = "organisation_full_description", nullable = true, length = 1000)
	public String getOrganisationFullDescription() {
		return organisationFullDescription;
	}

	public void setOrganisationFullDescription(String organisationFullDescription) {
		this.organisationFullDescription = organisationFullDescription;
	}

	@Basic
	@Column(name = "phone_number", nullable = false, length = 20)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Basic
	@Column(name = "fax", nullable = true, length = 100)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Basic
	@Column(name = "email", nullable = true, length = 200)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Basic
	@Column(name = "web_address", nullable = true, length = 200)
	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	@Basic
	@Column(name = "charity_number", nullable = true, length = 30)
	public String getCharityNumber() {
		return charityNumber;
	}

	public void setCharityNumber(String charityNumber) {
		this.charityNumber = charityNumber;
	}

	@Basic
	@Column(name = "company_number", nullable = true, length = 30)
	public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	@Basic
	@Column(name = "active", nullable = true, length = 1)
	@ColumnDefault("'T'")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
	@Basic
	@Column(name = "gov_office_region_name", nullable = true, length = 100)
	public String getGovOfficeRegionName() {
		return govOfficeRegionName;
	}

	public void setGovOfficeRegionName(String govOfficeRegionName) {
		this.govOfficeRegionName = govOfficeRegionName;
	}

	@Basic
	@Column(name = "trust_region_name", nullable = true, length = 100)
	public String getTrustRegionName() {
		return trustRegionName;
	}

	public void setTrustRegionName(String trustRegionName) {
		this.trustRegionName = trustRegionName;
	}

	public String getTrustDistrictName() {
		return trustDistrictName;
	}

	@Basic
	@Column(name = "trust_district_name", nullable = true, length = 100)
	public void setTrustDistrictName(String trustDistrictName) {
		this.trustDistrictName = trustDistrictName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Organisation that = (Organisation) o;
		return organisationId == that.organisationId && Objects.equals(organisationName, that.organisationName)
				&& Objects.equals(organisationShortDescription, that.organisationShortDescription)
				&& Objects.equals(addressLine1, that.addressLine1) && Objects.equals(addressLine2, that.addressLine2)
				&& Objects.equals(addressLine3, that.addressLine3) && Objects.equals(postcode, that.postcode)
				&& Objects.equals(town, that.town) && Objects.equals(county, that.county)
				&& Objects.equals(country, that.country)
				&& Objects.equals(preferredOrganisation, that.preferredOrganisation)
				&& Objects.equals(organisationFullDescription, that.organisationFullDescription)
				&& Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(fax, that.fax)
				&& Objects.equals(email, that.email) && Objects.equals(webAddress, that.webAddress)
				&& Objects.equals(charityNumber, that.charityNumber)
				&& Objects.equals(companyNumber, that.companyNumber) && Objects.equals(active, that.active)
				&& Objects.equals(govOfficeRegionName, that.govOfficeRegionName) && Objects.equals(trustRegionName, that.trustRegionName)
				&& Objects.equals(trustDistrictName, that.trustDistrictName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(organisationId, organisationName, organisationShortDescription, addressLine1, addressLine2,
				addressLine3, postcode, town, county, country, preferredOrganisation, organisationFullDescription,
				phoneNumber, fax, email, webAddress, charityNumber, companyNumber, active, govOfficeRegionName, trustRegionName, trustDistrictName);
	}

	@ManyToOne
	@JoinColumn(name = "bussiness_type_id", referencedColumnName = "bussiness_type_id", nullable = false)
	public BussinessType getBussinessTypeByBussinessTypeId() {
		return bussinessTypeByBussinessTypeId;
	}

	public void setBussinessTypeByBussinessTypeId(BussinessType bussinessTypeByBussinessTypeId) {
		this.bussinessTypeByBussinessTypeId = bussinessTypeByBussinessTypeId;
	}

	@ManyToOne
	@JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
	public Contact getContactByContactId() {
		return contactByContactId;
	}

	public void setContactByContactId(Contact contactByContactId) {
		this.contactByContactId = contactByContactId;
	}

	@OneToOne(mappedBy = "organisationByOrganisationId")
	public OrganisationDetail getOrganisationDetail() {
		return organisationDetail;
	}

	public void setOrganisationDetail(OrganisationDetail organisationDetail) {
		this.organisationDetail = organisationDetail;
	}

	@OneToMany(mappedBy = "organisationByOrganizationId")
	public Collection<ServiceOrganization> getServiceOrganizationsByOrganisationId() {
		return serviceOrganizationsByOrganisationId;
	}

	public void setServiceOrganizationsByOrganisationId(
			Collection<ServiceOrganization> serviceOrganizationsByOrganisationId) {
		this.serviceOrganizationsByOrganisationId = serviceOrganizationsByOrganisationId;
	}
}
