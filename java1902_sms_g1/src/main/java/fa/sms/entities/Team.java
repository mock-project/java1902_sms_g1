package fa.sms.entities;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import java.util.Objects;

@Entity
@Table(name = "Team",schema = "organisation",catalog = "SMS")
public class Team {
  private int teamId;
  private String teamName;
  private String teamShortDescription;
  private String addressLine1;
  private String addressLine2;
  private String addressLine3;
  private String postcode;
  private String town;
  private String county;
  private String country;
  private String teamFullDescription;
  private String phoneNumber;
  private String fax;
  private String email;
  private String webAddress;
  private String active;
  private BussinessType bussinessTypeByBussinessTypeId;
  private Contact contactByContactId;
  private Department departmentByDepartmentId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "team_id", nullable = false)
  public int getTeamId() {
    return teamId;
  }

  public void setTeamId(int teamId) {
    this.teamId = teamId;
  }

  @Basic
  @Column(name = "team_name", nullable = false, length = 50,unique = true)
  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }

  @Basic
  @Column(name = "team_short_description", nullable = true, length = 200)
  public String getTeamShortDescription() {
    return teamShortDescription;
  }

  public void setTeamShortDescription(String teamShortDescription) {
    this.teamShortDescription = teamShortDescription;
  }

  @Basic
  @Column(name = "address_line1", nullable = true, length = 100)
  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  @Basic
  @Column(name = "address_line2", nullable = true, length = 100)
  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  @Basic
  @Column(name = "address_line3", nullable = true, length = 100)
  public String getAddressLine3() {
    return addressLine3;
  }

  public void setAddressLine3(String addressLine3) {
    this.addressLine3 = addressLine3;
  }

  @Basic
  @Column(name = "postcode", nullable = true, length = 20)
  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  @Basic
  @Column(name = "town", nullable = true, length = 50)
  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  @Basic
  @Column(name = "county", nullable = true, length = 50)
  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  @Basic
  @Column(name = "country", nullable = true, length = 50)
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "team_full_description", nullable = true, length = 1000)
  public String getTeamFullDescription() {
    return teamFullDescription;
  }

  public void setTeamFullDescription(String teamFullDescription) {
    this.teamFullDescription = teamFullDescription;
  }

  @Basic
  @Column(name = "phone_number", nullable = true, length = 20)
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Basic
  @Column(name = "fax", nullable = true, length = 100)
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  @Basic
  @Column(name = "email", nullable = true, length = 200)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "web_address", nullable = true, length = 200)
  public String getWebAddress() {
    return webAddress;
  }

  public void setWebAddress(String webAddress) {
    this.webAddress = webAddress;
  }

  @Basic
  @Column(name = "active", nullable = true, length = 1)
  @ColumnDefault("'T'")
  public String getActive() {
    return active;
  }

  public void setActive(String active) {
    this.active = active;
  }



  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Team team = (Team) o;
    return teamId == team.teamId &&
            Objects.equals(teamName, team.teamName) &&
            Objects.equals(teamShortDescription, team.teamShortDescription) &&
            Objects.equals(addressLine1, team.addressLine1) &&
            Objects.equals(addressLine2, team.addressLine2) &&
            Objects.equals(addressLine3, team.addressLine3) &&
            Objects.equals(postcode, team.postcode) &&
            Objects.equals(town, team.town) &&
            Objects.equals(county, team.county) &&
            Objects.equals(country, team.country) &&
            Objects.equals(teamFullDescription, team.teamFullDescription) &&
            Objects.equals(phoneNumber, team.phoneNumber) &&
            Objects.equals(fax, team.fax) &&
            Objects.equals(email, team.email) &&
            Objects.equals(webAddress, team.webAddress) &&
            Objects.equals(active, team.active) ;
  }

  @Override
  public int hashCode() {
    return Objects.hash(teamId, teamName, teamShortDescription, addressLine1, addressLine2, addressLine3, postcode, town, county, country, teamFullDescription, phoneNumber, fax, email, webAddress, active);
  }

  @ManyToOne
  @JoinColumn(name = "bussiness_type_id", referencedColumnName = "bussiness_type_id")
  public BussinessType getBussinessTypeByBussinessTypeId() {
    return bussinessTypeByBussinessTypeId;
  }

  public void setBussinessTypeByBussinessTypeId(BussinessType bussinessTypeByBussinessTypeId) {
    this.bussinessTypeByBussinessTypeId = bussinessTypeByBussinessTypeId;
  }

  @ManyToOne
  @JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
  public Contact getContactByContactId() {
    return contactByContactId;
  }

  public void setContactByContactId(Contact contactByContactId) {
    this.contactByContactId = contactByContactId;
  }

  @ManyToOne
  @JoinColumn(name = "department_id", referencedColumnName = "department_id")
  public Department getDepartmentByDepartmentId() {
    return departmentByDepartmentId;
  }

  public void setDepartmentByDepartmentId(Department departmentByDepartmentId) {
    this.departmentByDepartmentId = departmentByDepartmentId;
  }
}
