package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Open_day", schema = "dbo", catalog = "SMS")
public class OpenDay {
  private int id;
  private String day;
  private Collection<OpenDayPremise> openDayPremisesById;

  @Id
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "day", nullable = true, length = 10)
  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    this.day = day;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenDay openDay = (OpenDay) o;
    return id == openDay.id &&
            Objects.equals(day, openDay.day);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, day);
  }

  @OneToMany(mappedBy = "openDayByOpenDayid")
  public Collection<OpenDayPremise> getOpenDayPremisesById() {
    return openDayPremisesById;
  }

  public void setOpenDayPremisesById(Collection<OpenDayPremise> openDayPremisesById) {
    this.openDayPremisesById = openDayPremisesById;
  }
}
