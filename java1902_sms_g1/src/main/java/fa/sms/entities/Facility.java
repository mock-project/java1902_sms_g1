package fa.sms.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Facility {
  private int facilityId;
  private String facilityType;
  private String facilityDescription;
  private Integer roomCapacity;
  private Integer roomSize;
  private Boolean roomConnectivity;
  private String connectivityType;
  private String wirelessAccessInformation;
  private String leadContact;
  private String roomHost;
  private Boolean equipmentAvailable;
  private String roomEquipmentNotes;
  private Premise premiseByPremiseId;

  @Id
  @Column(name = "facility_id", nullable = false)
  public int getFacilityId() {
    return facilityId;
  }

  public void setFacilityId(int facilityId) {
    this.facilityId = facilityId;
  }

  @Basic
  @Column(name = "facility_type", nullable = false, length = 30)
  public String getFacilityType() {
    return facilityType;
  }

  public void setFacilityType(String facilityType) {
    this.facilityType = facilityType;
  }

  @Basic
  @Column(name = "facility_description", nullable = false, length = 100)
  public String getFacilityDescription() {
    return facilityDescription;
  }

  public void setFacilityDescription(String facilityDescription) {
    this.facilityDescription = facilityDescription;
  }

  @Basic
  @Column(name = "room_capacity", nullable = true)
  public Integer getRoomCapacity() {
    return roomCapacity;
  }

  public void setRoomCapacity(Integer roomCapacity) {
    this.roomCapacity = roomCapacity;
  }

  @Basic
  @Column(name = "room_size", nullable = true)
  public Integer getRoomSize() {
    return roomSize;
  }

  public void setRoomSize(Integer roomSize) {
    this.roomSize = roomSize;
  }

  @Basic
  @Column(name = "room_connectivity", nullable = true)
  public Boolean getRoomConnectivity() {
    return roomConnectivity;
  }

  public void setRoomConnectivity(Boolean roomConnectivity) {
    this.roomConnectivity = roomConnectivity;
  }

  @Basic
  @Column(name = "connectivity_type", nullable = true, length = 30)
  public String getConnectivityType() {
    return connectivityType;
  }

  public void setConnectivityType(String connectivityType) {
    this.connectivityType = connectivityType;
  }

  @Basic
  @Column(name = "wireless_access_information", nullable = true, length = 30)
  public String getWirelessAccessInformation() {
    return wirelessAccessInformation;
  }

  public void setWirelessAccessInformation(String wirelessAccessInformation) {
    this.wirelessAccessInformation = wirelessAccessInformation;
  }

  @Basic
  @Column(name = "lead_contact", nullable = true, length = 30)
  public String getLeadContact() {
    return leadContact;
  }

  public void setLeadContact(String leadContact) {
    this.leadContact = leadContact;
  }

  @Basic
  @Column(name = "room_host", nullable = true, length = 30)
  public String getRoomHost() {
    return roomHost;
  }

  public void setRoomHost(String roomHost) {
    this.roomHost = roomHost;
  }

  @Basic
  @Column(name = "equipment_available", nullable = true)
  public Boolean getEquipmentAvailable() {
    return equipmentAvailable;
  }

  public void setEquipmentAvailable(Boolean equipmentAvailable) {
    this.equipmentAvailable = equipmentAvailable;
  }

  @Basic
  @Column(name = "room_equipment_notes", nullable = true, length = 200)
  public String getRoomEquipmentNotes() {
    return roomEquipmentNotes;
  }

  public void setRoomEquipmentNotes(String roomEquipmentNotes) {
    this.roomEquipmentNotes = roomEquipmentNotes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Facility facility = (Facility) o;
    return facilityId == facility.facilityId &&
            Objects.equals(facilityType, facility.facilityType) &&
            Objects.equals(facilityDescription, facility.facilityDescription) &&
            Objects.equals(roomCapacity, facility.roomCapacity) &&
            Objects.equals(roomSize, facility.roomSize) &&
            Objects.equals(roomConnectivity, facility.roomConnectivity) &&
            Objects.equals(connectivityType, facility.connectivityType) &&
            Objects.equals(wirelessAccessInformation, facility.wirelessAccessInformation) &&
            Objects.equals(leadContact, facility.leadContact) &&
            Objects.equals(roomHost, facility.roomHost) &&
            Objects.equals(equipmentAvailable, facility.equipmentAvailable) &&
            Objects.equals(roomEquipmentNotes, facility.roomEquipmentNotes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(facilityId, facilityType, facilityDescription, roomCapacity, roomSize, roomConnectivity, connectivityType, wirelessAccessInformation, leadContact, roomHost, equipmentAvailable, roomEquipmentNotes);
  }

  @ManyToOne
  @JoinColumn(name = "premise_id", referencedColumnName = "premise_id", nullable = false)
  public Premise getPremiseByPremiseId() {
    return premiseByPremiseId;
  }

  public void setPremiseByPremiseId(Premise premiseByPremiseId) {
    this.premiseByPremiseId = premiseByPremiseId;
  }
}
