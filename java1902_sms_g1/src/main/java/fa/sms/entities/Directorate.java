package fa.sms.entities;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Directorate",schema = "organisation",catalog = "SMS")
public class Directorate {
  private int directorateId;
  private String directorateName;
  private String directorateShortDescription;
  private String addressLine1;
  private String addressLine2;
  private String addressLine3;
  private String postcode;
  private String town;
  private String county;
  private String country;
  private String directorateFullDescription;
  private String phoneNumber;
  private String fax;
  private String email;
  private String webAddress;
  private String charityNumber;
  private String companyNumber;
  private String active;
  private Collection<Department> departmentsByDirectorateId;
  private BussinessType bussinessTypeByBussinessTypeId;
  private Contact contactByContactId;
  private OrganisationDetail organisationDetailByOrganisationDetailId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "directorate_id", nullable = false)
  public int getDirectorateId() {
    return directorateId;
  }

  public void setDirectorateId(int directorateId) {
    this.directorateId = directorateId;
  }

  @Basic
  @Column(name = "directorate_name", nullable = false, length = 50,unique = true)
  public String getDirectorateName() {
    return directorateName;
  }

  public void setDirectorateName(String directorateName) {
    this.directorateName = directorateName;
  }

  @Basic
  @Column(name = "directorate_short_description", nullable = true, length = 200)
  public String getDirectorateShortDescription() {
    return directorateShortDescription;
  }

  public void setDirectorateShortDescription(String directorateShortDescription) {
    this.directorateShortDescription = directorateShortDescription;
  }

  @Basic
  @Column(name = "address_line1", nullable = false, length = 100)
  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  @Basic
  @Column(name = "address_line2", nullable = true, length = 100)
  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  @Basic
  @Column(name = "address_line3", nullable = true, length = 100)
  public String getAddressLine3() {
    return addressLine3;
  }

  public void setAddressLine3(String addressLine3) {
    this.addressLine3 = addressLine3;
  }

  @Basic
  @Column(name = "postcode", nullable = false, length = 20)
  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  @Basic
  @Column(name = "town", nullable = true, length = 50)
  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  @Basic
  @Column(name = "county", nullable = true, length = 50)
  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  @Basic
  @Column(name = "country", nullable = true, length = 50)
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "directorate_full_description", nullable = true, length = 1000)
  public String getDirectorateFullDescription() {
    return directorateFullDescription;
  }

  public void setDirectorateFullDescription(String directorateFullDescription) {
    this.directorateFullDescription = directorateFullDescription;
  }

  @Basic
  @Column(name = "phone_number", nullable = true, length = 20)
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Basic
  @Column(name = "fax", nullable = true, length = 100)
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  @Basic
  @Column(name = "email", nullable = true, length = 200)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "web_address", nullable = true, length = 200)
  public String getWebAddress() {
    return webAddress;
  }

  public void setWebAddress(String webAddress) {
    this.webAddress = webAddress;
  }

  @Basic
  @Column(name = "charity_number", nullable = true, length = 30)
  public String getCharityNumber() {
    return charityNumber;
  }

  public void setCharityNumber(String charityNumber) {
    this.charityNumber = charityNumber;
  }

  @Basic
  @Column(name = "company_number", nullable = true, length = 30)
  public String getCompanyNumber() {
    return companyNumber;
  }

  public void setCompanyNumber(String companyNumber) {
    this.companyNumber = companyNumber;
  }

  @Basic
  @Column(name = "active", nullable = true, length = 1)
  @ColumnDefault("'T'")
  public String getActive() {
    return active;
  }

  public void setActive(String active) {
    this.active = active;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Directorate that = (Directorate) o;
    return directorateId == that.directorateId &&
            Objects.equals(directorateName, that.directorateName) &&
            Objects.equals(directorateShortDescription, that.directorateShortDescription) &&
            Objects.equals(addressLine1, that.addressLine1) &&
            Objects.equals(addressLine2, that.addressLine2) &&
            Objects.equals(addressLine3, that.addressLine3) &&
            Objects.equals(postcode, that.postcode) &&
            Objects.equals(town, that.town) &&
            Objects.equals(county, that.county) &&
            Objects.equals(country, that.country) &&
            Objects.equals(directorateFullDescription, that.directorateFullDescription) &&
            Objects.equals(phoneNumber, that.phoneNumber) &&
            Objects.equals(fax, that.fax) &&
            Objects.equals(email, that.email) &&
            Objects.equals(webAddress, that.webAddress) &&
            Objects.equals(charityNumber, that.charityNumber) &&
            Objects.equals(companyNumber, that.companyNumber) &&
            Objects.equals(active, that.active) ;
  }

  @Override
  public int hashCode() {
    return Objects.hash(directorateId, directorateName, directorateShortDescription, addressLine1, addressLine2, addressLine3, postcode, town, county, country, directorateFullDescription, phoneNumber, fax, email, webAddress, charityNumber, companyNumber, active);
  }

  @OneToMany(mappedBy = "directorateByDirectorateId")
  public Collection<Department> getDepartmentsByDirectorateId() {
    return departmentsByDirectorateId;
  }

  public void setDepartmentsByDirectorateId(Collection<Department> departmentsByDirectorateId) {
    this.departmentsByDirectorateId = departmentsByDirectorateId;
  }

  @ManyToOne
  @JoinColumn(name = "bussiness_type_id", referencedColumnName = "bussiness_type_id", nullable = false)
  public BussinessType getBussinessTypeByBussinessTypeId() {
    return bussinessTypeByBussinessTypeId;
  }

  public void setBussinessTypeByBussinessTypeId(BussinessType bussinessTypeByBussinessTypeId) {
    this.bussinessTypeByBussinessTypeId = bussinessTypeByBussinessTypeId;
  }

  @ManyToOne
  @JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
  public Contact getContactByContactId() {
    return contactByContactId;
  }

  public void setContactByContactId(Contact contactByContactId) {
    this.contactByContactId = contactByContactId;
  }

  @ManyToOne
  @JoinColumn(name = "organisation_detail_id", referencedColumnName = "organisation_detail_id")
  public OrganisationDetail getOrganisationDetailByOrganisationDetailId() {
    return organisationDetailByOrganisationDetailId;
  }

  public void setOrganisationDetailByOrganisationDetailId(OrganisationDetail organisationDetailByOrganisationDetailId) {
    this.organisationDetailByOrganisationDetailId = organisationDetailByOrganisationDetailId;
  }
}
