package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Progamme {
  private int programmeId;
  private String programmeName;
  private String programmeDescription;
  private Contact contactByContactId;
  private Collection<Service> servicesByProgrammeId;

  @Id
  @Column(name = "programme_id", nullable = false)
  public int getProgrammeId() {
    return programmeId;
  }

  public void setProgrammeId(int programmeId) {
    this.programmeId = programmeId;
  }

  @Basic
  @Column(name = "programme_name", nullable = false, length = 100)
  public String getProgrammeName() {
    return programmeName;
  }

  public void setProgrammeName(String programmeName) {
    this.programmeName = programmeName;
  }

  @Basic
  @Column(name = "programme_description", nullable = true, length = 255)
  public String getProgrammeDescription() {
    return programmeDescription;
  }

  public void setProgrammeDescription(String programmeDescription) {
    this.programmeDescription = programmeDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Progamme progamme = (Progamme) o;
    return programmeId == progamme.programmeId &&
            Objects.equals(programmeName, progamme.programmeName) &&
            Objects.equals(programmeDescription, progamme.programmeDescription);
  }

  @Override
  public int hashCode() {
    return Objects.hash(programmeId, programmeName, programmeDescription);
  }

  @ManyToOne
  @JoinColumn(name = "contact_id", referencedColumnName = "contact_id", nullable = false)
  public Contact getContactByContactId() {
    return contactByContactId;
  }

  public void setContactByContactId(Contact contactByContactId) {
    this.contactByContactId = contactByContactId;
  }

  @OneToMany(mappedBy = "progammeByProgrammeId")
  public Collection<Service> getServicesByProgrammeId() {
    return servicesByProgrammeId;
  }

  public void setServicesByProgrammeId(Collection<Service> servicesByProgrammeId) {
    this.servicesByProgrammeId = servicesByProgrammeId;
  }
}
