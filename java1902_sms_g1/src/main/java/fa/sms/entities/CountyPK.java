package fa.sms.entities;

import java.io.Serializable;
import java.util.Objects;




public class CountyPK implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private int countyId;
  private Country countryByCountryId;

  public int getCountyId() {
    return countyId;
  }


  public void setCountyId(int countyId) {
    this.countyId = countyId;
  }
  

  public Country getCountryByCountryId() {
    return countryByCountryId;
  }


  public void setCountryByCountryId(Country countryByCountryId) {
    this.countryByCountryId = countryByCountryId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CountyPK countyPK = (CountyPK) o;
    return countyId == countyPK.countyId &&
        countryByCountryId == countyPK.countryByCountryId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(countyId, countryByCountryId);
  }
}
