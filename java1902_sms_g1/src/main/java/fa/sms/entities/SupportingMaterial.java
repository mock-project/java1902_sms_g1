package fa.sms.entities;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Supporting_Material", schema = "organisation", catalog = "SMS")
public class SupportingMaterial {
	private int supportingMaterialId;
	private String url;
	private String description;
	private String type;
	private String active;
	private Date addDate;

	private User userByUserId;
	private OrganisationDetail organisationDetailByOrganisationDetailId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "supporting_material_id", nullable = false)
	public int getSupportingMaterialId() {
		return supportingMaterialId;
	}

	public void setSupportingMaterialId(int supportingMaterialId) {
		this.supportingMaterialId = supportingMaterialId;
	}

	@Basic
	@Column(name = "url", nullable = false, length = 500, unique = true)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Basic
	@Column(name = "description", nullable = true, length = 1000)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Basic
	@Column(name = "add_date", nullable = false)
	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	@Basic
	@Column(name = "type", nullable = true, length = 20)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Basic
	@Column(name = "active", nullable = true, length = 1)
	@ColumnDefault("'T'")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		SupportingMaterial that = (SupportingMaterial) o;
		return supportingMaterialId == that.supportingMaterialId && Objects.equals(url, that.url)
				&& Objects.equals(description, that.description) && Objects.equals(type, that.type)
				&& Objects.equals(active, that.active);
	}

	@Override
	public int hashCode() {
		return Objects.hash(supportingMaterialId, url, description, type, active);
	}

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	public User getUserByUserId() {
		return userByUserId;
	}

	public void setUserByUserId(User userByUserId) {
		this.userByUserId = userByUserId;
	}

	@ManyToOne
	@JoinColumn(name = "organisation_detail_id", referencedColumnName = "organisation_detail_id")
	public OrganisationDetail getOrganisationDetailByOrganisationDetailId() {
		return organisationDetailByOrganisationDetailId;
	}

	public void setOrganisationDetailByOrganisationDetailId(
			OrganisationDetail organisationDetailByOrganisationDetailId) {
		this.organisationDetailByOrganisationDetailId = organisationDetailByOrganisationDetailId;
	}

}
