package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Contact {
	private int contactId;
	private String contactName;
	private Collection<Department> departmentsByContactId;
	private Collection<Directorate> directoratesByContactId;
	private Collection<Organisation> organisationsByContactId;
	private Collection<Progamme> progammesByContactId;
	private Collection<Service> servicesByContactId;
	private Collection<Team> teamsByContactId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contact_id", nullable = false)
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	@Basic
	@Column(name = "contact_name", nullable = true, length = 100)
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Contact contact = (Contact) o;
		return contactId == contact.contactId && Objects.equals(contactName, contact.contactName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(contactId, contactName);
	}

	@OneToMany(mappedBy = "contactByContactId")
	public Collection<Department> getDepartmentsByContactId() {
		return departmentsByContactId;
	}

	public void setDepartmentsByContactId(Collection<Department> departmentsByContactId) {
		this.departmentsByContactId = departmentsByContactId;
	}

	@OneToMany(mappedBy = "contactByContactId")
	public Collection<Directorate> getDirectoratesByContactId() {
		return directoratesByContactId;
	}

	public void setDirectoratesByContactId(Collection<Directorate> directoratesByContactId) {
		this.directoratesByContactId = directoratesByContactId;
	}

	@OneToMany(mappedBy = "contactByContactId")
	public Collection<Organisation> getOrganisationsByContactId() {
		return organisationsByContactId;
	}

	public void setOrganisationsByContactId(Collection<Organisation> organisationsByContactId) {
		this.organisationsByContactId = organisationsByContactId;
	}

	@OneToMany(mappedBy = "contactByContactId")
	public Collection<Progamme> getProgammesByContactId() {
		return progammesByContactId;
	}

	public void setProgammesByContactId(Collection<Progamme> progammesByContactId) {
		this.progammesByContactId = progammesByContactId;
	}

	@OneToMany(mappedBy = "contactByLeadContact")
	public Collection<Service> getServicesByContactId() {
		return servicesByContactId;
	}

	public void setServicesByContactId(Collection<Service> servicesByContactId) {
		this.servicesByContactId = servicesByContactId;
	}

	@OneToMany(mappedBy = "contactByContactId")
	public Collection<Team> getTeamsByContactId() {
		return teamsByContactId;
	}

	public void setTeamsByContactId(Collection<Team> teamsByContactId) {
		this.teamsByContactId = teamsByContactId;
	}
}
