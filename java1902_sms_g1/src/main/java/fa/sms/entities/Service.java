package fa.sms.entities;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Service {
	private int serviceId;
	private String serviceName;
	private String serviceShortDescription;
	private String subType;
	private String clientDescription;
	private String serviceAttendance;
	private Timestamp serviceStartExpected;
	private Timestamp serviceStartDate;
	private Timestamp serviceEndDate;
	private Byte serviceExtendable;
	private Integer serviceExtendableYears;
	private Integer serviceExtendableMonths;
	private Byte serviceActive;
	private String serviceFullDescription;
	private String deptCode;
	private String serviceType;
	private String serviceDescriptionDelivery;
	private String serviceContractCode;
	private String serviceContractValue;
	private Byte contractStagedPayment;
	private String referralProcess;
	private Byte serviceTimeLimited;
	private Integer serviceTimeLimitedYear;
	private Integer serviceTimeLimitedMonth;
	private String contractOutcome;
	private String contractObigation;
	private String participation;
	private String fundingSource;
	private String fundingContactDetails;
	private BigDecimal fundingAmount;
	private Timestamp fundingStart;
	private Timestamp fundingEnd;
	private BigDecimal fundingNeeds;
	private Byte fundingContinuationNeeded;
	private BigDecimal fundingContinuationAmount;
	private String fundingContinuationDetails;
	private String fundraisingForText;
	private String fundraisingWhy;
	private BigDecimal fundraisingNeeds;
	private Timestamp fundraisingRequiredBy;
	private Byte fundraisingComplete;
	private Timestamp fundraisingCompletedDate;
	private Byte fundraisingDonorAnonymous;
	private BigDecimal fundraisingDonorAmount;
	private Timestamp fundraisingDonationDate;
	private Byte fundraisingDonationIncremental;
	private String serviceBenefitsCriterion;
	private String serviceBarriersCriterion;
	private String serviceEthnicityCriterion;
	private String serviceDisabilityCriterion;
	private String servicePersonalCircumstancesCriterion;
	private String otherServiceParticipationCriterion;
	private String clientSupportProcess;
	private String clientOutcome;
	private String intervention;
	private String targetClient;
	private String clientJourney;
	private String accreditations;
	private String referralSources;
	private String supportCentres;
	private Contact contactByLeadContact;
	private Service serviceByOtherServicesId;
	private Collection<Service> servicesByServiceId;
	private Progamme progammeByProgrammeId;
	private Collection<ServiceOrganization> serviceOrganizationsByServiceId;
	private Collection<ServicePremise> servicePremisesByServiceId;

	@Id
	@Column(name = "service_id", nullable = false)
	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	@Basic
	@Column(name = "service_name", nullable = false, length = 100)
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Basic
	@Column(name = "service_short_description", nullable = false, length = 150)
	public String getServiceShortDescription() {
		return serviceShortDescription;
	}

	public void setServiceShortDescription(String serviceShortDescription) {
		this.serviceShortDescription = serviceShortDescription;
	}

	@Basic
	@Column(name = "sub_type", nullable = true, length = 100)
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	@Basic
	@Column(name = "client_description", nullable = true, length = 100)
	public String getClientDescription() {
		return clientDescription;
	}

	public void setClientDescription(String clientDescription) {
		this.clientDescription = clientDescription;
	}

	@Basic
	@Column(name = "service_attendance", nullable = true, length = 100)
	public String getServiceAttendance() {
		return serviceAttendance;
	}

	public void setServiceAttendance(String serviceAttendance) {
		this.serviceAttendance = serviceAttendance;
	}

	@Basic
	@Column(name = "service_start_expected", nullable = true)
	public Timestamp getServiceStartExpected() {
		return serviceStartExpected;
	}

	public void setServiceStartExpected(Timestamp serviceStartExpected) {
		this.serviceStartExpected = serviceStartExpected;
	}

	@Basic
	@Column(name = "service_start_date", nullable = true)
	public Timestamp getServiceStartDate() {
		return serviceStartDate;
	}

	public void setServiceStartDate(Timestamp serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}

	@Basic
	@Column(name = "service_end_date", nullable = true)
	public Timestamp getServiceEndDate() {
		return serviceEndDate;
	}

	public void setServiceEndDate(Timestamp serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}

	@Basic
	@Column(name = "service_extendable", nullable = true)
	public Byte getServiceExtendable() {
		return serviceExtendable;
	}

	public void setServiceExtendable(Byte serviceExtendable) {
		this.serviceExtendable = serviceExtendable;
	}

	@Basic
	@Column(name = "service_extendable_years", nullable = true)
	public Integer getServiceExtendableYears() {
		return serviceExtendableYears;
	}

	public void setServiceExtendableYears(Integer serviceExtendableYears) {
		this.serviceExtendableYears = serviceExtendableYears;
	}

	@Basic
	@Column(name = "service_extendable_months", nullable = true)
	public Integer getServiceExtendableMonths() {
		return serviceExtendableMonths;
	}

	public void setServiceExtendableMonths(Integer serviceExtendableMonths) {
		this.serviceExtendableMonths = serviceExtendableMonths;
	}

	@Basic
	@Column(name = "service_active", nullable = true)
	public Byte getServiceActive() {
		return serviceActive;
	}

	public void setServiceActive(Byte serviceActive) {
		this.serviceActive = serviceActive;
	}

	@Basic
	@Column(name = "service_full_description", nullable = true, length = 255)
	public String getServiceFullDescription() {
		return serviceFullDescription;
	}

	public void setServiceFullDescription(String serviceFullDescription) {
		this.serviceFullDescription = serviceFullDescription;
	}

	@Basic
	@Column(name = "dept_code", nullable = false, length = 100)
	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	@Basic
	@Column(name = "service_type", nullable = false, length = 100)
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Basic
	@Column(name = "service_description_delivery", nullable = true, length = 100)
	public String getServiceDescriptionDelivery() {
		return serviceDescriptionDelivery;
	}

	public void setServiceDescriptionDelivery(String serviceDescriptionDelivery) {
		this.serviceDescriptionDelivery = serviceDescriptionDelivery;
	}

	@Basic
	@Column(name = "service_contract_code", nullable = true, length = 100)
	public String getServiceContractCode() {
		return serviceContractCode;
	}

	public void setServiceContractCode(String serviceContractCode) {
		this.serviceContractCode = serviceContractCode;
	}

	@Basic
	@Column(name = "service_contract_value", nullable = true, length = 100)
	public String getServiceContractValue() {
		return serviceContractValue;
	}

	public void setServiceContractValue(String serviceContractValue) {
		this.serviceContractValue = serviceContractValue;
	}

	@Basic
	@Column(name = "contract_staged_payment", nullable = true)
	public Byte getContractStagedPayment() {
		return contractStagedPayment;
	}

	public void setContractStagedPayment(Byte contractStagedPayment) {
		this.contractStagedPayment = contractStagedPayment;
	}

	@Basic
	@Column(name = "referral_process", nullable = true, length = 100)
	public String getReferralProcess() {
		return referralProcess;
	}

	public void setReferralProcess(String referralProcess) {
		this.referralProcess = referralProcess;
	}

	@Basic
	@Column(name = "service_time_limited", nullable = true)
	public Byte getServiceTimeLimited() {
		return serviceTimeLimited;
	}

	public void setServiceTimeLimited(Byte serviceTimeLimited) {
		this.serviceTimeLimited = serviceTimeLimited;
	}

	@Basic
	@Column(name = "service_time_limited_year", nullable = true)
	public Integer getServiceTimeLimitedYear() {
		return serviceTimeLimitedYear;
	}

	public void setServiceTimeLimitedYear(Integer serviceTimeLimitedYear) {
		this.serviceTimeLimitedYear = serviceTimeLimitedYear;
	}

	@Basic
	@Column(name = "service_time_limited_month", nullable = true)
	public Integer getServiceTimeLimitedMonth() {
		return serviceTimeLimitedMonth;
	}

	public void setServiceTimeLimitedMonth(Integer serviceTimeLimitedMonth) {
		this.serviceTimeLimitedMonth = serviceTimeLimitedMonth;
	}

	@Basic
	@Column(name = "contract_outcome", nullable = true, length = 100)
	public String getContractOutcome() {
		return contractOutcome;
	}

	public void setContractOutcome(String contractOutcome) {
		this.contractOutcome = contractOutcome;
	}

	@Basic
	@Column(name = "contract_obigation", nullable = true, length = 100)
	public String getContractObigation() {
		return contractObigation;
	}

	public void setContractObigation(String contractObigation) {
		this.contractObigation = contractObigation;
	}

	@Basic
	@Column(name = "participation", nullable = true, length = 100)
	public String getParticipation() {
		return participation;
	}

	public void setParticipation(String participation) {
		this.participation = participation;
	}

	@Basic
	@Column(name = "funding_source", nullable = true, length = 100)
	public String getFundingSource() {
		return fundingSource;
	}

	public void setFundingSource(String fundingSource) {
		this.fundingSource = fundingSource;
	}

	@Basic
	@Column(name = "funding_contact_details", nullable = true, length = 100)
	public String getFundingContactDetails() {
		return fundingContactDetails;
	}

	public void setFundingContactDetails(String fundingContactDetails) {
		this.fundingContactDetails = fundingContactDetails;
	}

	@Basic
	@Column(name = "funding_amount", nullable = true)
	public BigDecimal getFundingAmount() {
		return fundingAmount;
	}

	@Basic
	@Column(name = "funding_start", nullable = true)
	public Timestamp getFundingStart() {
		return fundingStart;
	}

	public void setFundingStart(Timestamp fundingStart) {
		this.fundingStart = fundingStart;
	}

	@Basic
	@Column(name = "funding_end", nullable = true)
	public Timestamp getFundingEnd() {
		return fundingEnd;
	}

	public void setFundingEnd(Timestamp fundingEnd) {
		this.fundingEnd = fundingEnd;
	}

	@Basic
	@Column(name = "funding_needs", nullable = true)
	public BigDecimal getFundingNeeds() {
		return fundingNeeds;
	}

	public void setFundingNeeds(BigDecimal fundingNeeds) {
		this.fundingNeeds = fundingNeeds;
	}

	@Basic
	@Column(name = "funding_continuation_needed", nullable = true)
	public Byte getFundingContinuationNeeded() {
		return fundingContinuationNeeded;
	}

	public void setFundingContinuationNeeded(Byte fundingContinuationNeeded) {
		this.fundingContinuationNeeded = fundingContinuationNeeded;
	}

	@Basic
	@Column(name = "funding_continuation_amount", nullable = true)
	public BigDecimal getFundingContinuationAmount() {
		return fundingContinuationAmount;
	}

	public void setFundingContinuationAmount(BigDecimal fundingContinuationAmount) {
		this.fundingContinuationAmount = fundingContinuationAmount;
	}

	@Basic
	@Column(name = "funding_continuation_details", nullable = true, length = 100)
	public String getFundingContinuationDetails() {
		return fundingContinuationDetails;
	}

	public void setFundingContinuationDetails(String fundingContinuationDetails) {
		this.fundingContinuationDetails = fundingContinuationDetails;
	}

	@Basic
	@Column(name = "fundraising_for_text", nullable = true, length = 100)
	public String getFundraisingForText() {
		return fundraisingForText;
	}

	public void setFundraisingForText(String fundraisingForText) {
		this.fundraisingForText = fundraisingForText;
	}

	@Basic
	@Column(name = "fundraising_why", nullable = true, length = 100)
	public String getFundraisingWhy() {
		return fundraisingWhy;
	}

	public void setFundraisingWhy(String fundraisingWhy) {
		this.fundraisingWhy = fundraisingWhy;
	}

	@Basic
	@Column(name = "fundraising_needs", nullable = true)
	public BigDecimal getFundraisingNeeds() {
		return fundraisingNeeds;
	}

	public void setFundraisingNeeds(BigDecimal fundraisingNeeds) {
		this.fundraisingNeeds = fundraisingNeeds;
	}

	@Basic
	@Column(name = "fundraising_required_by", nullable = true)
	public Timestamp getFundraisingRequiredBy() {
		return fundraisingRequiredBy;
	}

	public void setFundraisingRequiredBy(Timestamp fundraisingRequiredBy) {
		this.fundraisingRequiredBy = fundraisingRequiredBy;
	}

	@Basic
	@Column(name = "fundraising_complete", nullable = true)
	public Byte getFundraisingComplete() {
		return fundraisingComplete;
	}

	public void setFundraisingComplete(Byte fundraisingComplete) {
		this.fundraisingComplete = fundraisingComplete;
	}

	@Basic
	@Column(name = "fundraising_completed_date", nullable = true)
	public Timestamp getFundraisingCompletedDate() {
		return fundraisingCompletedDate;
	}

	public void setFundraisingCompletedDate(Timestamp fundraisingCompletedDate) {
		this.fundraisingCompletedDate = fundraisingCompletedDate;
	}

	@Basic
	@Column(name = "fundraising_donor_anonymous", nullable = true)
	public Byte getFundraisingDonorAnonymous() {
		return fundraisingDonorAnonymous;
	}

	public void setFundraisingDonorAnonymous(Byte fundraisingDonorAnonymous) {
		this.fundraisingDonorAnonymous = fundraisingDonorAnonymous;
	}

	@Basic
	@Column(name = "fundraising_donor_amount", nullable = true)
	public BigDecimal getFundraisingDonorAmount() {
		return fundraisingDonorAmount;
	}

	public void setFundraisingDonorAmount(BigDecimal fundraisingDonorAmount) {
		this.fundraisingDonorAmount = fundraisingDonorAmount;
	}

	@Basic
	@Column(name = "fundraising_donation_date", nullable = true)
	public Timestamp getFundraisingDonationDate() {
		return fundraisingDonationDate;
	}

	public void setFundraisingDonationDate(Timestamp fundraisingDonationDate) {
		this.fundraisingDonationDate = fundraisingDonationDate;
	}

	@Basic
	@Column(name = "fundraising_donation_incremental", nullable = true)
	public Byte getFundraisingDonationIncremental() {
		return fundraisingDonationIncremental;
	}

	public void setFundraisingDonationIncremental(Byte fundraisingDonationIncremental) {
		this.fundraisingDonationIncremental = fundraisingDonationIncremental;
	}

	@Basic
	@Column(name = "service_benefits_criterion", nullable = true, length = 100)
	public String getServiceBenefitsCriterion() {
		return serviceBenefitsCriterion;
	}

	public void setServiceBenefitsCriterion(String serviceBenefitsCriterion) {
		this.serviceBenefitsCriterion = serviceBenefitsCriterion;
	}

	@Basic
	@Column(name = "service_barriers_criterion", nullable = true, length = 100)
	public String getServiceBarriersCriterion() {
		return serviceBarriersCriterion;
	}

	public void setServiceBarriersCriterion(String serviceBarriersCriterion) {
		this.serviceBarriersCriterion = serviceBarriersCriterion;
	}

	@Basic
	@Column(name = "service_ethnicity_criterion", nullable = true, length = 100)
	public String getServiceEthnicityCriterion() {
		return serviceEthnicityCriterion;
	}

	public void setServiceEthnicityCriterion(String serviceEthnicityCriterion) {
		this.serviceEthnicityCriterion = serviceEthnicityCriterion;
	}

	@Basic
	@Column(name = "service_disability_criterion", nullable = true, length = 100)
	public String getServiceDisabilityCriterion() {
		return serviceDisabilityCriterion;
	}

	public void setServiceDisabilityCriterion(String serviceDisabilityCriterion) {
		this.serviceDisabilityCriterion = serviceDisabilityCriterion;
	}

	@Basic
	@Column(name = "service_personal_circumstances_criterion", nullable = true, length = 100)
	public String getServicePersonalCircumstancesCriterion() {
		return servicePersonalCircumstancesCriterion;
	}

	public void setServicePersonalCircumstancesCriterion(String servicePersonalCircumstancesCriterion) {
		this.servicePersonalCircumstancesCriterion = servicePersonalCircumstancesCriterion;
	}

	@Basic
	@Column(name = "other_service_participation_criterion", nullable = true, length = 100)
	public String getOtherServiceParticipationCriterion() {
		return otherServiceParticipationCriterion;
	}

	public void setOtherServiceParticipationCriterion(String otherServiceParticipationCriterion) {
		this.otherServiceParticipationCriterion = otherServiceParticipationCriterion;
	}

	@Basic
	@Column(name = "client_support_process", nullable = true, length = 100)
	public String getClientSupportProcess() {
		return clientSupportProcess;
	}

	public void setClientSupportProcess(String clientSupportProcess) {
		this.clientSupportProcess = clientSupportProcess;
	}

	@Basic
	@Column(name = "client_outcome", nullable = true, length = 100)
	public String getClientOutcome() {
		return clientOutcome;
	}

	public void setClientOutcome(String clientOutcome) {
		this.clientOutcome = clientOutcome;
	}

	@Basic
	@Column(name = "intervention", nullable = true, length = 100)
	public String getIntervention() {
		return intervention;
	}

	public void setIntervention(String intervention) {
		this.intervention = intervention;
	}

	@Basic
	@Column(name = "target_client", nullable = true, length = 100)
	public String getTargetClient() {
		return targetClient;
	}

	public void setTargetClient(String targetClient) {
		this.targetClient = targetClient;
	}

	@Basic
	@Column(name = "client_journey", nullable = true, length = 100)
	public String getClientJourney() {
		return clientJourney;
	}

	public void setClientJourney(String clientJourney) {
		this.clientJourney = clientJourney;
	}

	@Basic
	@Column(name = "accreditations", nullable = true, length = 100)
	public String getAccreditations() {
		return accreditations;
	}

	public void setAccreditations(String accreditations) {
		this.accreditations = accreditations;
	}

	@Basic
	@Column(name = "referral_sources", nullable = true, length = 100)
	public String getReferralSources() {
		return referralSources;
	}

	public void setReferralSources(String referralSources) {
		this.referralSources = referralSources;
	}

	@Basic
	@Column(name = "support_centres", nullable = true, length = 100)
	public String getSupportCentres() {
		return supportCentres;
	}

	public void setSupportCentres(String supportCentres) {
		this.supportCentres = supportCentres;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Service service = (Service) o;
		return serviceId == service.serviceId && Objects.equals(serviceName, service.serviceName)
				&& Objects.equals(serviceShortDescription, service.serviceShortDescription)
				&& Objects.equals(subType, service.subType)
				&& Objects.equals(clientDescription, service.clientDescription)
				&& Objects.equals(serviceAttendance, service.serviceAttendance)
				&& Objects.equals(serviceStartExpected, service.serviceStartExpected)
				&& Objects.equals(serviceStartDate, service.serviceStartDate)
				&& Objects.equals(serviceEndDate, service.serviceEndDate)
				&& Objects.equals(serviceExtendable, service.serviceExtendable)
				&& Objects.equals(serviceExtendableYears, service.serviceExtendableYears)
				&& Objects.equals(serviceExtendableMonths, service.serviceExtendableMonths)
				&& Objects.equals(serviceActive, service.serviceActive)
				&& Objects.equals(serviceFullDescription, service.serviceFullDescription)
				&& Objects.equals(deptCode, service.deptCode) && Objects.equals(serviceType, service.serviceType)
				&& Objects.equals(serviceDescriptionDelivery, service.serviceDescriptionDelivery)
				&& Objects.equals(serviceContractCode, service.serviceContractCode)
				&& Objects.equals(serviceContractValue, service.serviceContractValue)
				&& Objects.equals(contractStagedPayment, service.contractStagedPayment)
				&& Objects.equals(referralProcess, service.referralProcess)
				&& Objects.equals(serviceTimeLimited, service.serviceTimeLimited)
				&& Objects.equals(serviceTimeLimitedYear, service.serviceTimeLimitedYear)
				&& Objects.equals(serviceTimeLimitedMonth, service.serviceTimeLimitedMonth)
				&& Objects.equals(contractOutcome, service.contractOutcome)
				&& Objects.equals(contractObigation, service.contractObigation)
				&& Objects.equals(participation, service.participation)
				&& Objects.equals(fundingSource, service.fundingSource)
				&& Objects.equals(fundingContactDetails, service.fundingContactDetails)
				&& Objects.equals(fundingAmount, service.fundingAmount)
				&& Objects.equals(fundingStart, service.fundingStart) && Objects.equals(fundingEnd, service.fundingEnd)
				&& Objects.equals(fundingNeeds, service.fundingNeeds)
				&& Objects.equals(fundingContinuationNeeded, service.fundingContinuationNeeded)
				&& Objects.equals(fundingContinuationAmount, service.fundingContinuationAmount)
				&& Objects.equals(fundingContinuationDetails, service.fundingContinuationDetails)
				&& Objects.equals(fundraisingForText, service.fundraisingForText)
				&& Objects.equals(fundraisingWhy, service.fundraisingWhy)
				&& Objects.equals(fundraisingNeeds, service.fundraisingNeeds)
				&& Objects.equals(fundraisingRequiredBy, service.fundraisingRequiredBy)
				&& Objects.equals(fundraisingComplete, service.fundraisingComplete)
				&& Objects.equals(fundraisingCompletedDate, service.fundraisingCompletedDate)
				&& Objects.equals(fundraisingDonorAnonymous, service.fundraisingDonorAnonymous)
				&& Objects.equals(fundraisingDonorAmount, service.fundraisingDonorAmount)
				&& Objects.equals(fundraisingDonationDate, service.fundraisingDonationDate)
				&& Objects.equals(fundraisingDonationIncremental, service.fundraisingDonationIncremental)
				&& Objects.equals(serviceBenefitsCriterion, service.serviceBenefitsCriterion)
				&& Objects.equals(serviceBarriersCriterion, service.serviceBarriersCriterion)
				&& Objects.equals(serviceEthnicityCriterion, service.serviceEthnicityCriterion)
				&& Objects.equals(serviceDisabilityCriterion, service.serviceDisabilityCriterion)
				&& Objects.equals(servicePersonalCircumstancesCriterion, service.servicePersonalCircumstancesCriterion)
				&& Objects.equals(otherServiceParticipationCriterion, service.otherServiceParticipationCriterion)
				&& Objects.equals(clientSupportProcess, service.clientSupportProcess)
				&& Objects.equals(clientOutcome, service.clientOutcome)
				&& Objects.equals(intervention, service.intervention)
				&& Objects.equals(targetClient, service.targetClient)
				&& Objects.equals(clientJourney, service.clientJourney)
				&& Objects.equals(accreditations, service.accreditations)
				&& Objects.equals(referralSources, service.referralSources)
				&& Objects.equals(supportCentres, service.supportCentres);
	}

	@Override
	public int hashCode() {
		return Objects.hash(serviceId, serviceName, serviceShortDescription, subType, clientDescription,
				serviceAttendance, serviceStartExpected, serviceStartDate, serviceEndDate, serviceExtendable,
				serviceExtendableYears, serviceExtendableMonths, serviceActive, serviceFullDescription, deptCode,
				serviceType, serviceDescriptionDelivery, serviceContractCode, serviceContractValue,
				contractStagedPayment, referralProcess, serviceTimeLimited, serviceTimeLimitedYear,
				serviceTimeLimitedMonth, contractOutcome, contractObigation, participation, fundingSource,
				fundingContactDetails, fundingAmount, fundingStart, fundingEnd, fundingNeeds, fundingContinuationNeeded,
				fundingContinuationAmount, fundingContinuationDetails, fundraisingForText, fundraisingWhy,
				fundraisingNeeds, fundraisingRequiredBy, fundraisingComplete, fundraisingCompletedDate,
				fundraisingDonorAnonymous, fundraisingDonorAmount, fundraisingDonationDate,
				fundraisingDonationIncremental, serviceBenefitsCriterion, serviceBarriersCriterion,
				serviceEthnicityCriterion, serviceDisabilityCriterion, servicePersonalCircumstancesCriterion,
				otherServiceParticipationCriterion, clientSupportProcess, clientOutcome, intervention, targetClient,
				clientJourney, accreditations, referralSources, supportCentres);
	}

	@ManyToOne
	@JoinColumn(name = "lead_contact", referencedColumnName = "contact_id", nullable = false)
	public Contact getContactByLeadContact() {
		return contactByLeadContact;
	}

	public void setContactByLeadContact(Contact contactByLeadContact) {
		this.contactByLeadContact = contactByLeadContact;
	}

	@ManyToOne
	@JoinColumn(name = "other_services_id", referencedColumnName = "service_id")
	public Service getServiceByOtherServicesId() {
		return serviceByOtherServicesId;
	}

	public void setFundingAmount(BigDecimal fundingAmount) {
		this.fundingAmount = fundingAmount;
	}

	public void setServiceByOtherServicesId(Service serviceByOtherServicesId) {
		this.serviceByOtherServicesId = serviceByOtherServicesId;
	}

	@OneToMany(mappedBy = "serviceByOtherServicesId")
	public Collection<Service> getServicesByServiceId() {
		return servicesByServiceId;
	}

	public void setServicesByServiceId(Collection<Service> servicesByServiceId) {
		this.servicesByServiceId = servicesByServiceId;
	}

	@ManyToOne
	@JoinColumn(name = "programme_id", referencedColumnName = "programme_id", nullable = false)
	public Progamme getProgammeByProgrammeId() {
		return progammeByProgrammeId;
	}

	public void setProgammeByProgrammeId(Progamme progammeByProgrammeId) {
		this.progammeByProgrammeId = progammeByProgrammeId;
	}

	@OneToMany(mappedBy = "serviceByServiceId")
	public Collection<ServiceOrganization> getServiceOrganizationsByServiceId() {
		return serviceOrganizationsByServiceId;
	}

	public void setServiceOrganizationsByServiceId(Collection<ServiceOrganization> serviceOrganizationsByServiceId) {
		this.serviceOrganizationsByServiceId = serviceOrganizationsByServiceId;
	}

	@OneToMany(mappedBy = "serviceByServiceId")
	public Collection<ServicePremise> getServicePremisesByServiceId() {
		return servicePremisesByServiceId;
	}

	public void setServicePremisesByServiceId(Collection<ServicePremise> servicePremisesByServiceId) {
		this.servicePremisesByServiceId = servicePremisesByServiceId;
	}
}
