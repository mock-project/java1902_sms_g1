package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Country {
	private int countryId;
	private String countryName;
	private Collection<County> countiesByCountryId;
	private Collection<TrustRegion> trustRegionsByCountryId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "country_id", nullable = false)
	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	@Basic
	@Column(name = "country_name", nullable = false, length = 100)
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Country country = (Country) o;
		return countryId == country.countryId && Objects.equals(countryName, country.countryName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(countryId, countryName);
	}

	@OneToMany(mappedBy = "countryByCountryId")
	public Collection<County> getCountiesByCountryId() {
		return countiesByCountryId;
	}

	public void setCountiesByCountryId(Collection<County> countiesByCountryId) {
		this.countiesByCountryId = countiesByCountryId;
	}

	@OneToMany(mappedBy = "countryByCountryId")
	public Collection<TrustRegion> getTrustRegionsByCountryId() {
		return trustRegionsByCountryId;
	}

	public void setTrustRegionsByCountryId(Collection<TrustRegion> trustRegionsByCountryId) {
		this.trustRegionsByCountryId = trustRegionsByCountryId;
	}
}
