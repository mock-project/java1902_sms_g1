package fa.sms.entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class TrustRegion {
  private int trustRegionId;
  private String trustRegionName;
  private String trustRegionDescription;
  private Byte trustRegionStatus;
  private Collection<TrustDistrict> trustDistrictsByTrustRegionId;
  private Country countryByCountryId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "trust_region_id", nullable = false)
  public int getTrustRegionId() {
    return trustRegionId;
  }

  public void setTrustRegionId(int trustRegionId) {
    this.trustRegionId = trustRegionId;
  }

  @Basic
  @Column(name = "trust_region_name", nullable = false, length = 100)
  public String getTrustRegionName() {
    return trustRegionName;
  }

  public void setTrustRegionName(String trustRegionName) {
    this.trustRegionName = trustRegionName;
  }

  @Basic
  @Column(name = "trust_region_description", nullable = true, length = 255)
  public String getTrustRegionDescription() {
    return trustRegionDescription;
  }

  public void setTrustRegionDescription(String trustRegionDescription) {
    this.trustRegionDescription = trustRegionDescription;
  }

  @Basic
  @Column(name = "trust_region_status", nullable = true)
  public Byte getTrustRegionStatus() {
    return trustRegionStatus;
  }

  public void setTrustRegionStatus(Byte trustRegionStatus) {
    this.trustRegionStatus = trustRegionStatus;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TrustRegion that = (TrustRegion) o;
    return trustRegionId == that.trustRegionId &&
            Objects.equals(trustRegionName, that.trustRegionName) &&
            Objects.equals(trustRegionDescription, that.trustRegionDescription) &&
            Objects.equals(trustRegionStatus, that.trustRegionStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(trustRegionId, trustRegionName, trustRegionDescription, trustRegionStatus);
  }

  @OneToMany(mappedBy = "trustRegionByTrustRegionId")
  public Collection<TrustDistrict> getTrustDistrictsByTrustRegionId() {
    return trustDistrictsByTrustRegionId;
  }

  public void setTrustDistrictsByTrustRegionId(Collection<TrustDistrict> trustDistrictsByTrustRegionId) {
    this.trustDistrictsByTrustRegionId = trustDistrictsByTrustRegionId;
  }

  @ManyToOne
  @JoinColumn(name = "country_id", referencedColumnName = "country_id", nullable = false)
  public Country getCountryByCountryId() {
    return countryByCountryId;
  }

  public void setCountryByCountryId(Country countryByCountryId) {
    this.countryByCountryId = countryByCountryId;
  }
}
