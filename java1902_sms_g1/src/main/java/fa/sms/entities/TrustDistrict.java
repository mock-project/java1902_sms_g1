package fa.sms.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class TrustDistrict {
  private int trustDistrictId;
  private String trustDistrictName;
  private String trustDistrictDescription;
  private Byte trustDistrictStatus;
  private TrustRegion trustRegionByTrustRegionId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "trust_district_id", nullable = false)
  public int getTrustDistrictId() {
    return trustDistrictId;
  }

  public void setTrustDistrictId(int trustDistrictId) {
    this.trustDistrictId = trustDistrictId;
  }

  @Basic
  @Column(name = "trust_district_name", nullable = false, length = 100)
  public String getTrustDistrictName() {
    return trustDistrictName;
  }

  public void setTrustDistrictName(String trustDistrictName) {
    this.trustDistrictName = trustDistrictName;
  }

  @Basic
  @Column(name = "trust_district_description", nullable = true, length = 255)
  public String getTrustDistrictDescription() {
    return trustDistrictDescription;
  }

  public void setTrustDistrictDescription(String trustDistrictDescription) {
    this.trustDistrictDescription = trustDistrictDescription;
  }

  @Basic
  @Column(name = "trust_district_status", nullable = true)
  public Byte getTrustDistrictStatus() {
    return trustDistrictStatus;
  }

  public void setTrustDistrictStatus(Byte trustDistrictStatus) {
    this.trustDistrictStatus = trustDistrictStatus;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TrustDistrict that = (TrustDistrict) o;
    return trustDistrictId == that.trustDistrictId &&
            Objects.equals(trustDistrictName, that.trustDistrictName) &&
            Objects.equals(trustDistrictDescription, that.trustDistrictDescription) &&
            Objects.equals(trustDistrictStatus, that.trustDistrictStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(trustDistrictId, trustDistrictName, trustDistrictDescription, trustDistrictStatus);
  }

  @ManyToOne
  @JoinColumn(name = "trust_region_id", referencedColumnName = "trust_region_id", nullable = false)
  public TrustRegion getTrustRegionByTrustRegionId() {
    return trustRegionByTrustRegionId;
  }

  public void setTrustRegionByTrustRegionId(TrustRegion trustRegionByTrustRegionId) {
    this.trustRegionByTrustRegionId = trustRegionByTrustRegionId;
  }
}
