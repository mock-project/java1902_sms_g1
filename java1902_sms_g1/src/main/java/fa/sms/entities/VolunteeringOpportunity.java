package fa.sms.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "Volunteering_Opportunity", schema = "dbo", catalog = "SMS")
public class VolunteeringOpportunity {
  private int volunteeringId;
  private String volunteeringContact;
  private String voluteeringPurpose;
  private String volunteeringOpportunityDetail;
  private Timestamp startDate;
  private Timestamp endDate;
  private Integer volunteeringNos;
  private Premise premiseByPremiseId;

  @Id
  @Column(name = "volunteering_id", nullable = false)
  public int getVolunteeringId() {
    return volunteeringId;
  }

  public void setVolunteeringId(int volunteeringId) {
    this.volunteeringId = volunteeringId;
  }

  @Basic
  @Column(name = "volunteering_contact", nullable = true, length = 30)
  public String getVolunteeringContact() {
    return volunteeringContact;
  }

  public void setVolunteeringContact(String volunteeringContact) {
    this.volunteeringContact = volunteeringContact;
  }

  @Basic
  @Column(name = "voluteering_purpose", nullable = true, length = 20)
  public String getVoluteeringPurpose() {
    return voluteeringPurpose;
  }

  public void setVoluteeringPurpose(String voluteeringPurpose) {
    this.voluteeringPurpose = voluteeringPurpose;
  }

  @Basic
  @Column(name = "volunteering_opportunity_detail", nullable = true, length = 20)
  public String getVolunteeringOpportunityDetail() {
    return volunteeringOpportunityDetail;
  }

  public void setVolunteeringOpportunityDetail(String volunteeringOpportunityDetail) {
    this.volunteeringOpportunityDetail = volunteeringOpportunityDetail;
  }

  @Basic
  @Column(name = "start_date", nullable = true)
  public Timestamp getStartDate() {
    return startDate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  @Basic
  @Column(name = "end_date", nullable = true)
  public Timestamp getEndDate() {
    return endDate;
  }

  public void setEndDate(Timestamp endDate) {
    this.endDate = endDate;
  }

  @Basic
  @Column(name = "volunteering_nos", nullable = true)
  public Integer getVolunteeringNos() {
    return volunteeringNos;
  }

  public void setVolunteeringNos(Integer volunteeringNos) {
    this.volunteeringNos = volunteeringNos;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VolunteeringOpportunity that = (VolunteeringOpportunity) o;
    return volunteeringId == that.volunteeringId &&
            Objects.equals(volunteeringContact, that.volunteeringContact) &&
            Objects.equals(voluteeringPurpose, that.voluteeringPurpose) &&
            Objects.equals(volunteeringOpportunityDetail, that.volunteeringOpportunityDetail) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(volunteeringNos, that.volunteeringNos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(volunteeringId, volunteeringContact, voluteeringPurpose, volunteeringOpportunityDetail, startDate, endDate, volunteeringNos);
  }

  @ManyToOne
  @JoinColumn(name = "premise_id", referencedColumnName = "premise_id", nullable = false)
  public Premise getPremiseByPremiseId() {
    return premiseByPremiseId;
  }

  public void setPremiseByPremiseId(Premise premiseByPremiseId) {
    this.premiseByPremiseId = premiseByPremiseId;
  }
}
