package fa.sms.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "Open_day_Premise", schema = "dbo", catalog = "SMS")
@IdClass(OpenDayPremisePK.class)
public class OpenDayPremise {
  private Timestamp startTime;
  private Timestamp endTime;
  private OpenDay openDayByOpenDayid;
  private Premise premiseByPremiseId;



  @Basic
  @Column(name = "start_time", nullable = false)
  public Timestamp getStartTime() {
    return startTime;
  }

  public void setStartTime(Timestamp startTime) {
    this.startTime = startTime;
  }

  @Basic
  @Column(name = "end_time", nullable = false)
  public Timestamp getEndTime() {
    return endTime;
  }

  public void setEndTime(Timestamp endTime) {
    this.endTime = endTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    OpenDayPremise that = (OpenDayPremise) o;
    return
        Objects.equals(startTime, that.startTime)
        && Objects.equals(endTime, that.endTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash( startTime, endTime);
  }

  @ManyToOne
  @Id
  @JoinColumn(name = "Open_dayid", referencedColumnName = "id", nullable = false)
  public OpenDay getOpenDayByOpenDayid() {
    return openDayByOpenDayid;
  }

  public void setOpenDayByOpenDayid(OpenDay openDayByOpenDayid) {
    this.openDayByOpenDayid = openDayByOpenDayid;
  }

  @ManyToOne
  @Id
  @JoinColumn(name = "premise_id", referencedColumnName = "premise_id", nullable = false)
  public Premise getPremiseByPremiseId() {
    return premiseByPremiseId;
  }

  public void setPremiseByPremiseId(Premise premiseByPremiseId) {
    this.premiseByPremiseId = premiseByPremiseId;
  }
}
