package fa.sms.entities;

import java.io.Serializable;
import java.util.Objects;

public class OpenDayPremisePK implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private OpenDay openDayByOpenDayid;
  private Premise premiseByPremiseId;
  


  public OpenDay getOpenDayByOpenDayid() {
    return openDayByOpenDayid;
  }

  public void setOpenDayByOpenDayid(OpenDay openDayByOpenDayid) {
    this.openDayByOpenDayid = openDayByOpenDayid;
  }

  public Premise getPremiseByPremiseId() {
    return premiseByPremiseId;
  }

  public void setPremiseByPremiseId(Premise premiseByPremiseId) {
    this.premiseByPremiseId = premiseByPremiseId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenDayPremisePK that = (OpenDayPremisePK) o;
    return openDayByOpenDayid == that.openDayByOpenDayid &&
        premiseByPremiseId == that.premiseByPremiseId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(openDayByOpenDayid, premiseByPremiseId);
  }
}
