package fa.sms.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Premise {
  private int premiseId;
  private String locationName;
  private String premiseName;
  private Integer locationOrganization;
  private String locationStatus;
  private Timestamp locationStatusDate;
  private Boolean primaryLocation;
  private Boolean locationManaged;
  private Boolean stNetworkConnectivity;
  private String address1;
  private String address2;
  private String address3;
  private String postcode;
  private String cityTown;
  private String county;
  private String nation;
  private String locationDescription;
  private String phoneNumber;
  private String genaralFaxNumber;
  private String minicomNumber;
  private Boolean isNewShop;
  private Boolean specialistShop;
  private String locationType;
  private String accreditatios;
  private Boolean mediaContact;
  private String mediaContactName;
  private String localDemographicIssues;
  private String localDemographicNotes;
  private String jcpOffices;
  private Boolean cateringFacilities;
  private String cateringContact;
  private String cateringType;
  private String isNetwork;
  private Boolean clientItFacilities;
  private String clientItFacilitiesDetails;
  private Boolean roomAvailability;
  private Boolean volunteeringOpportunities;
  private String outreachLocation;
  private String travelDetail;
  private String travelNearestBus;
  private String travelNearestRail;
  private String travelNearestAirport;
  private Boolean hostVisits;
  private String hostingContact;
  private String localHotels;
  private Boolean visitorParkingOnsite;
  private Integer visitorParkingSpaces;
  private String visitorParkigAlternative;
  private BigDecimal roomOnlyRate;
  private BigDecimal bbRate;
  private BigDecimal dbbRate;
  private BigDecimal ddRate;
  private BigDecimal h24Rate;
  private BigDecimal teaAndCoffe;
  private BigDecimal negotiatedRoomOnly;
  private BigDecimal bbNegotiatedRate;
  private BigDecimal dbbNegotiatedRate;
  private BigDecimal ddNegotiatedRate;
  private BigDecimal h24NegotiatedRate;
  private Timestamp lastNegotiatedDate;
  private Timestamp reNegotiateOn;
  private Boolean lunch;
  private Integer noOfMeeting;
  private BigDecimal meetingRoomRatePerDate;
  private String codings;
  private String preferredStatus;
  private String comments;
  private Collection<Facility> facilitiesByPremiseId;
  private Collection<MinorWorkProject> minorWorkProjectsByPremiseId;
  private Collection<OpenDayPremise> openDayPremisesByPremiseId;
  private Collection<ServicePremise> servicePremisesByPremiseId;
  private Collection<VolunteeringOpportunity> volunteeringOpportunitiesByPremiseId;

  @Id
  @Column(name = "premise_id", nullable = false)
  public int getPremiseId() {
    return premiseId;
  }

  public void setPremiseId(int premiseId) {
    this.premiseId = premiseId;
  }

  @Basic
  @Column(name = "location_name", nullable = false, length = 30)
  public String getLocationName() {
    return locationName;
  }

  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }

  @Basic
  @Column(name = "premise_name", nullable = true, length = 30)
  public String getPremiseName() {
    return premiseName;
  }

  public void setPremiseName(String premiseName) {
    this.premiseName = premiseName;
  }

  @Basic
  @Column(name = "location_organization", nullable = true)
  public Integer getLocationOrganization() {
    return locationOrganization;
  }

  public void setLocationOrganization(Integer locationOrganization) {
    this.locationOrganization = locationOrganization;
  }

  @Basic
  @Column(name = "location_status", nullable = true, length = 10)
  public String getLocationStatus() {
    return locationStatus;
  }

  public void setLocationStatus(String locationStatus) {
    this.locationStatus = locationStatus;
  }

  @Basic
  @Column(name = "location_status_date", nullable = true)
  public Timestamp getLocationStatusDate() {
    return locationStatusDate;
  }

  public void setLocationStatusDate(Timestamp locationStatusDate) {
    this.locationStatusDate = locationStatusDate;
  }

  @Basic
  @Column(name = "primary_location", nullable = true)
  public Boolean getPrimaryLocation() {
    return primaryLocation;
  }

  public void setPrimaryLocation(Boolean primaryLocation) {
    this.primaryLocation = primaryLocation;
  }

  @Basic
  @Column(name = "location_managed", nullable = true)
  public Boolean getLocationManaged() {
    return locationManaged;
  }

  public void setLocationManaged(Boolean locationManaged) {
    this.locationManaged = locationManaged;
  }

  @Basic
  @Column(name = "st_network_connectivity", nullable = true)
  public Boolean getStNetworkConnectivity() {
    return stNetworkConnectivity;
  }

  public void setStNetworkConnectivity(Boolean stNetworkConnectivity) {
    this.stNetworkConnectivity = stNetworkConnectivity;
  }

  @Basic
  @Column(name = "address1", nullable = false, length = 50)
  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  @Basic
  @Column(name = "address2", nullable = true, length = 50)
  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  @Basic
  @Column(name = "address3", nullable = true, length = 50)
  public String getAddress3() {
    return address3;
  }

  public void setAddress3(String address3) {
    this.address3 = address3;
  }

  @Basic
  @Column(name = "postcode", nullable = true, length = 6)
  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  @Basic
  @Column(name = "city_town", nullable = true, length = 20)
  public String getCityTown() {
    return cityTown;
  }

  public void setCityTown(String cityTown) {
    this.cityTown = cityTown;
  }

  @Basic
  @Column(name = "county", nullable = true, length = 20)
  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  @Basic
  @Column(name = "nation", nullable = true, length = 20)
  public String getNation() {
    return nation;
  }

  public void setNation(String nation) {
    this.nation = nation;
  }

  @Basic
  @Column(name = "location_description", nullable = true, length = 300)
  public String getLocationDescription() {
    return locationDescription;
  }

  public void setLocationDescription(String locationDescription) {
    this.locationDescription = locationDescription;
  }

  @Basic
  @Column(name = "phone_number", nullable = false, length = 15)
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Basic
  @Column(name = "genaral_fax_number", nullable = true, length = 15)
  public String getGenaralFaxNumber() {
    return genaralFaxNumber;
  }

  public void setGenaralFaxNumber(String genaralFaxNumber) {
    this.genaralFaxNumber = genaralFaxNumber;
  }

  @Basic
  @Column(name = "minicom_number", nullable = true, length = 15)
  public String getMinicomNumber() {
    return minicomNumber;
  }

  public void setMinicomNumber(String minicomNumber) {
    this.minicomNumber = minicomNumber;
  }

  @Basic
  @Column(name = "is_new_shop", nullable = true)
  public Boolean getNewShop() {
    return isNewShop;
  }

  public void setNewShop(Boolean newShop) {
    isNewShop = newShop;
  }

  @Basic
  @Column(name = "specialist_shop", nullable = true)
  public Boolean getSpecialistShop() {
    return specialistShop;
  }

  public void setSpecialistShop(Boolean specialistShop) {
    this.specialistShop = specialistShop;
  }

  @Basic
  @Column(name = "location_type", nullable = false, length = 2147483647)
  public String getLocationType() {
    return locationType;
  }

  public void setLocationType(String locationType) {
    this.locationType = locationType;
  }

  @Basic
  @Column(name = "accreditatios", nullable = true, length = 2147483647)
  public String getAccreditatios() {
    return accreditatios;
  }

  public void setAccreditatios(String accreditatios) {
    this.accreditatios = accreditatios;
  }

  @Basic
  @Column(name = "media_contact", nullable = true)
  public Boolean getMediaContact() {
    return mediaContact;
  }

  public void setMediaContact(Boolean mediaContact) {
    this.mediaContact = mediaContact;
  }

  @Basic
  @Column(name = "media_contact_name", nullable = true, length = 30)
  public String getMediaContactName() {
    return mediaContactName;
  }

  public void setMediaContactName(String mediaContactName) {
    this.mediaContactName = mediaContactName;
  }

  @Basic
  @Column(name = "local_demographic_issues", nullable = true, length = 2147483647)
  public String getLocalDemographicIssues() {
    return localDemographicIssues;
  }

  public void setLocalDemographicIssues(String localDemographicIssues) {
    this.localDemographicIssues = localDemographicIssues;
  }

  @Basic
  @Column(name = "local_demographic_notes", nullable = true, length = 100)
  public String getLocalDemographicNotes() {
    return localDemographicNotes;
  }

  public void setLocalDemographicNotes(String localDemographicNotes) {
    this.localDemographicNotes = localDemographicNotes;
  }

  @Basic
  @Column(name = "jcp_offices", nullable = true, length = 2147483647)
  public String getJcpOffices() {
    return jcpOffices;
  }

  public void setJcpOffices(String jcpOffices) {
    this.jcpOffices = jcpOffices;
  }

  @Basic
  @Column(name = "catering_facilities", nullable = true)
  public Boolean getCateringFacilities() {
    return cateringFacilities;
  }

  public void setCateringFacilities(Boolean cateringFacilities) {
    this.cateringFacilities = cateringFacilities;
  }

  @Basic
  @Column(name = "catering_contact", nullable = true, length = 30)
  public String getCateringContact() {
    return cateringContact;
  }

  public void setCateringContact(String cateringContact) {
    this.cateringContact = cateringContact;
  }

  @Basic
  @Column(name = "catering_type", nullable = true, length = 20)
  public String getCateringType() {
    return cateringType;
  }

  public void setCateringType(String cateringType) {
    this.cateringType = cateringType;
  }

  @Basic
  @Column(name = "is_network", nullable = true, length = 30)
  public String getIsNetwork() {
    return isNetwork;
  }

  public void setIsNetwork(String isNetwork) {
    this.isNetwork = isNetwork;
  }

  @Basic
  @Column(name = "client_it_facilities", nullable = true)
  public Boolean getClientItFacilities() {
    return clientItFacilities;
  }

  public void setClientItFacilities(Boolean clientItFacilities) {
    this.clientItFacilities = clientItFacilities;
  }

  @Basic
  @Column(name = "client_it_facilities_details", nullable = true, length = 50)
  public String getClientItFacilitiesDetails() {
    return clientItFacilitiesDetails;
  }

  public void setClientItFacilitiesDetails(String clientItFacilitiesDetails) {
    this.clientItFacilitiesDetails = clientItFacilitiesDetails;
  }

  @Basic
  @Column(name = "room_availability", nullable = true)
  public Boolean getRoomAvailability() {
    return roomAvailability;
  }

  public void setRoomAvailability(Boolean roomAvailability) {
    this.roomAvailability = roomAvailability;
  }

  @Basic
  @Column(name = "volunteering_opportunities", nullable = true)
  public Boolean getVolunteeringOpportunities() {
    return volunteeringOpportunities;
  }

  public void setVolunteeringOpportunities(Boolean volunteeringOpportunities) {
    this.volunteeringOpportunities = volunteeringOpportunities;
  }

  @Basic
  @Column(name = "outreach_location", nullable = true, length = 2147483647)
  public String getOutreachLocation() {
    return outreachLocation;
  }

  public void setOutreachLocation(String outreachLocation) {
    this.outreachLocation = outreachLocation;
  }

  @Basic
  @Column(name = "travel_detail", nullable = true, length = 30)
  public String getTravelDetail() {
    return travelDetail;
  }

  public void setTravelDetail(String travelDetail) {
    this.travelDetail = travelDetail;
  }

  @Basic
  @Column(name = "travel_nearest_bus", nullable = true, length = 30)
  public String getTravelNearestBus() {
    return travelNearestBus;
  }

  public void setTravelNearestBus(String travelNearestBus) {
    this.travelNearestBus = travelNearestBus;
  }

  @Basic
  @Column(name = "travel_nearest_rail", nullable = true, length = 30)
  public String getTravelNearestRail() {
    return travelNearestRail;
  }

  public void setTravelNearestRail(String travelNearestRail) {
    this.travelNearestRail = travelNearestRail;
  }

  @Basic
  @Column(name = "travel_nearest_airport", nullable = true, length = 30)
  public String getTravelNearestAirport() {
    return travelNearestAirport;
  }

  public void setTravelNearestAirport(String travelNearestAirport) {
    this.travelNearestAirport = travelNearestAirport;
  }

  @Basic
  @Column(name = "host_visits", nullable = true)
  public Boolean getHostVisits() {
    return hostVisits;
  }

  public void setHostVisits(Boolean hostVisits) {
    this.hostVisits = hostVisits;
  }

  @Basic
  @Column(name = "hosting_contact", nullable = true, length = 30)
  public String getHostingContact() {
    return hostingContact;
  }

  public void setHostingContact(String hostingContact) {
    this.hostingContact = hostingContact;
  }

  @Basic
  @Column(name = "local_hotels", nullable = true, length = 2147483647)
  public String getLocalHotels() {
    return localHotels;
  }

  public void setLocalHotels(String localHotels) {
    this.localHotels = localHotels;
  }

  @Basic
  @Column(name = "visitor_parking_onsite", nullable = true)
  public Boolean getVisitorParkingOnsite() {
    return visitorParkingOnsite;
  }

  public void setVisitorParkingOnsite(Boolean visitorParkingOnsite) {
    this.visitorParkingOnsite = visitorParkingOnsite;
  }

  @Basic
  @Column(name = "visitor_parking_spaces", nullable = true)
  public Integer getVisitorParkingSpaces() {
    return visitorParkingSpaces;
  }

  public void setVisitorParkingSpaces(Integer visitorParkingSpaces) {
    this.visitorParkingSpaces = visitorParkingSpaces;
  }

  @Basic
  @Column(name = "visitor_parkig_alternative", nullable = true, length = 50)
  public String getVisitorParkigAlternative() {
    return visitorParkigAlternative;
  }

  public void setVisitorParkigAlternative(String visitorParkigAlternative) {
    this.visitorParkigAlternative = visitorParkigAlternative;
  }

  @Basic
  @Column(name = "room_only_rate", nullable = true, precision = 2)
  public BigDecimal getRoomOnlyRate() {
    return roomOnlyRate;
  }

  public void setRoomOnlyRate(BigDecimal roomOnlyRate) {
    this.roomOnlyRate = roomOnlyRate;
  }

  @Basic
  @Column(name = "bb_rate", nullable = true, precision = 2)
  public BigDecimal getBbRate() {
    return bbRate;
  }

  public void setBbRate(BigDecimal bbRate) {
    this.bbRate = bbRate;
  }

  @Basic
  @Column(name = "dbb_rate", nullable = true, precision = 2)
  public BigDecimal getDbbRate() {
    return dbbRate;
  }

  public void setDbbRate(BigDecimal dbbRate) {
    this.dbbRate = dbbRate;
  }

  @Basic
  @Column(name = "dd_rate", nullable = true, precision = 2)
  public BigDecimal getDdRate() {
    return ddRate;
  }

  public void setDdRate(BigDecimal ddRate) {
    this.ddRate = ddRate;
  }

  @Basic
  @Column(name = "h24_rate", nullable = true, precision = 2)
  public BigDecimal getH24Rate() {
    return h24Rate;
  }

  public void setH24Rate(BigDecimal h24Rate) {
    this.h24Rate = h24Rate;
  }

  @Basic
  @Column(name = "tea_and_coffe", nullable = true, precision = 2)
  public BigDecimal getTeaAndCoffe() {
    return teaAndCoffe;
  }

  public void setTeaAndCoffe(BigDecimal teaAndCoffe) {
    this.teaAndCoffe = teaAndCoffe;
  }

  @Basic
  @Column(name = "negotiated_room_only", nullable = true, precision = 2)
  public BigDecimal getNegotiatedRoomOnly() {
    return negotiatedRoomOnly;
  }

  public void setNegotiatedRoomOnly(BigDecimal negotiatedRoomOnly) {
    this.negotiatedRoomOnly = negotiatedRoomOnly;
  }

  @Basic
  @Column(name = "bb_negotiated_rate", nullable = true, precision = 2)
  public BigDecimal getBbNegotiatedRate() {
    return bbNegotiatedRate;
  }

  public void setBbNegotiatedRate(BigDecimal bbNegotiatedRate) {
    this.bbNegotiatedRate = bbNegotiatedRate;
  }

  @Basic
  @Column(name = "dbb_negotiated_rate", nullable = true, precision = 2)
  public BigDecimal getDbbNegotiatedRate() {
    return dbbNegotiatedRate;
  }

  public void setDbbNegotiatedRate(BigDecimal dbbNegotiatedRate) {
    this.dbbNegotiatedRate = dbbNegotiatedRate;
  }

  @Basic
  @Column(name = "dd_negotiated_rate", nullable = true, precision = 2)
  public BigDecimal getDdNegotiatedRate() {
    return ddNegotiatedRate;
  }

  public void setDdNegotiatedRate(BigDecimal ddNegotiatedRate) {
    this.ddNegotiatedRate = ddNegotiatedRate;
  }

  @Basic
  @Column(name = "h24_negotiated_rate", nullable = true, precision = 2)
  public BigDecimal getH24NegotiatedRate() {
    return h24NegotiatedRate;
  }

  public void setH24NegotiatedRate(BigDecimal h24NegotiatedRate) {
    this.h24NegotiatedRate = h24NegotiatedRate;
  }

  @Basic
  @Column(name = "last_negotiated_date", nullable = true)
  public Timestamp getLastNegotiatedDate() {
    return lastNegotiatedDate;
  }

  public void setLastNegotiatedDate(Timestamp lastNegotiatedDate) {
    this.lastNegotiatedDate = lastNegotiatedDate;
  }

  @Basic
  @Column(name = "re_negotiate_on", nullable = true)
  public Timestamp getReNegotiateOn() {
    return reNegotiateOn;
  }

  public void setReNegotiateOn(Timestamp reNegotiateOn) {
    this.reNegotiateOn = reNegotiateOn;
  }

  @Basic
  @Column(name = "lunch", nullable = true)
  public Boolean getLunch() {
    return lunch;
  }

  public void setLunch(Boolean lunch) {
    this.lunch = lunch;
  }

  @Basic
  @Column(name = "no_of_meeting", nullable = true)
  public Integer getNoOfMeeting() {
    return noOfMeeting;
  }

  public void setNoOfMeeting(Integer noOfMeeting) {
    this.noOfMeeting = noOfMeeting;
  }

  @Basic
  @Column(name = "meeting_room_rate_per_date", nullable = true, precision = 2)
  public BigDecimal getMeetingRoomRatePerDate() {
    return meetingRoomRatePerDate;
  }

  public void setMeetingRoomRatePerDate(BigDecimal meetingRoomRatePerDate) {
    this.meetingRoomRatePerDate = meetingRoomRatePerDate;
  }

  @Basic
  @Column(name = "codings", nullable = true, length = 30)
  public String getCodings() {
    return codings;
  }

  public void setCodings(String codings) {
    this.codings = codings;
  }

  @Basic
  @Column(name = "preferred_status", nullable = true, length = 30)
  public String getPreferredStatus() {
    return preferredStatus;
  }

  public void setPreferredStatus(String preferredStatus) {
    this.preferredStatus = preferredStatus;
  }

  @Basic
  @Column(name = "comments", nullable = true, length = 100)
  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Premise premise = (Premise) o;
    return premiseId == premise.premiseId &&
            Objects.equals(locationName, premise.locationName) &&
            Objects.equals(premiseName, premise.premiseName) &&
            Objects.equals(locationOrganization, premise.locationOrganization) &&
            Objects.equals(locationStatus, premise.locationStatus) &&
            Objects.equals(locationStatusDate, premise.locationStatusDate) &&
            Objects.equals(primaryLocation, premise.primaryLocation) &&
            Objects.equals(locationManaged, premise.locationManaged) &&
            Objects.equals(stNetworkConnectivity, premise.stNetworkConnectivity) &&
            Objects.equals(address1, premise.address1) &&
            Objects.equals(address2, premise.address2) &&
            Objects.equals(address3, premise.address3) &&
            Objects.equals(postcode, premise.postcode) &&
            Objects.equals(cityTown, premise.cityTown) &&
            Objects.equals(county, premise.county) &&
            Objects.equals(nation, premise.nation) &&
            Objects.equals(locationDescription, premise.locationDescription) &&
            Objects.equals(phoneNumber, premise.phoneNumber) &&
            Objects.equals(genaralFaxNumber, premise.genaralFaxNumber) &&
            Objects.equals(minicomNumber, premise.minicomNumber) &&
            Objects.equals(isNewShop, premise.isNewShop) &&
            Objects.equals(specialistShop, premise.specialistShop) &&
            Objects.equals(locationType, premise.locationType) &&
            Objects.equals(accreditatios, premise.accreditatios) &&
            Objects.equals(mediaContact, premise.mediaContact) &&
            Objects.equals(mediaContactName, premise.mediaContactName) &&
            Objects.equals(localDemographicIssues, premise.localDemographicIssues) &&
            Objects.equals(localDemographicNotes, premise.localDemographicNotes) &&
            Objects.equals(jcpOffices, premise.jcpOffices) &&
            Objects.equals(cateringFacilities, premise.cateringFacilities) &&
            Objects.equals(cateringContact, premise.cateringContact) &&
            Objects.equals(cateringType, premise.cateringType) &&
            Objects.equals(isNetwork, premise.isNetwork) &&
            Objects.equals(clientItFacilities, premise.clientItFacilities) &&
            Objects.equals(clientItFacilitiesDetails, premise.clientItFacilitiesDetails) &&
            Objects.equals(roomAvailability, premise.roomAvailability) &&
            Objects.equals(volunteeringOpportunities, premise.volunteeringOpportunities) &&
            Objects.equals(outreachLocation, premise.outreachLocation) &&
            Objects.equals(travelDetail, premise.travelDetail) &&
            Objects.equals(travelNearestBus, premise.travelNearestBus) &&
            Objects.equals(travelNearestRail, premise.travelNearestRail) &&
            Objects.equals(travelNearestAirport, premise.travelNearestAirport) &&
            Objects.equals(hostVisits, premise.hostVisits) &&
            Objects.equals(hostingContact, premise.hostingContact) &&
            Objects.equals(localHotels, premise.localHotels) &&
            Objects.equals(visitorParkingOnsite, premise.visitorParkingOnsite) &&
            Objects.equals(visitorParkingSpaces, premise.visitorParkingSpaces) &&
            Objects.equals(visitorParkigAlternative, premise.visitorParkigAlternative) &&
            Objects.equals(roomOnlyRate, premise.roomOnlyRate) &&
            Objects.equals(bbRate, premise.bbRate) &&
            Objects.equals(dbbRate, premise.dbbRate) &&
            Objects.equals(ddRate, premise.ddRate) &&
            Objects.equals(h24Rate, premise.h24Rate) &&
            Objects.equals(teaAndCoffe, premise.teaAndCoffe) &&
            Objects.equals(negotiatedRoomOnly, premise.negotiatedRoomOnly) &&
            Objects.equals(bbNegotiatedRate, premise.bbNegotiatedRate) &&
            Objects.equals(dbbNegotiatedRate, premise.dbbNegotiatedRate) &&
            Objects.equals(ddNegotiatedRate, premise.ddNegotiatedRate) &&
            Objects.equals(h24NegotiatedRate, premise.h24NegotiatedRate) &&
            Objects.equals(lastNegotiatedDate, premise.lastNegotiatedDate) &&
            Objects.equals(reNegotiateOn, premise.reNegotiateOn) &&
            Objects.equals(lunch, premise.lunch) &&
            Objects.equals(noOfMeeting, premise.noOfMeeting) &&
            Objects.equals(meetingRoomRatePerDate, premise.meetingRoomRatePerDate) &&
            Objects.equals(codings, premise.codings) &&
            Objects.equals(preferredStatus, premise.preferredStatus) &&
            Objects.equals(comments, premise.comments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(premiseId, locationName, premiseName, locationOrganization, locationStatus, locationStatusDate, primaryLocation, locationManaged, stNetworkConnectivity, address1, address2, address3, postcode, cityTown, county, nation, locationDescription, phoneNumber, genaralFaxNumber, minicomNumber, isNewShop, specialistShop, locationType, accreditatios, mediaContact, mediaContactName, localDemographicIssues, localDemographicNotes, jcpOffices, cateringFacilities, cateringContact, cateringType, isNetwork, clientItFacilities, clientItFacilitiesDetails, roomAvailability, volunteeringOpportunities, outreachLocation, travelDetail, travelNearestBus, travelNearestRail, travelNearestAirport, hostVisits, hostingContact, localHotels, visitorParkingOnsite, visitorParkingSpaces, visitorParkigAlternative, roomOnlyRate, bbRate, dbbRate, ddRate, h24Rate, teaAndCoffe, negotiatedRoomOnly, bbNegotiatedRate, dbbNegotiatedRate, ddNegotiatedRate, h24NegotiatedRate, lastNegotiatedDate, reNegotiateOn, lunch, noOfMeeting, meetingRoomRatePerDate, codings, preferredStatus, comments);
  }

  @OneToMany(mappedBy = "premiseByPremiseId")
  public Collection<Facility> getFacilitiesByPremiseId() {
    return facilitiesByPremiseId;
  }

  public void setFacilitiesByPremiseId(Collection<Facility> facilitiesByPremiseId) {
    this.facilitiesByPremiseId = facilitiesByPremiseId;
  }

  @OneToMany(mappedBy = "premiseByPremisepremiseId")
  public Collection<MinorWorkProject> getMinorWorkProjectsByPremiseId() {
    return minorWorkProjectsByPremiseId;
  }

  public void setMinorWorkProjectsByPremiseId(Collection<MinorWorkProject> minorWorkProjectsByPremiseId) {
    this.minorWorkProjectsByPremiseId = minorWorkProjectsByPremiseId;
  }

  @OneToMany(mappedBy = "premiseByPremiseId")
  public Collection<OpenDayPremise> getOpenDayPremisesByPremiseId() {
    return openDayPremisesByPremiseId;
  }

  public void setOpenDayPremisesByPremiseId(Collection<OpenDayPremise> openDayPremisesByPremiseId) {
    this.openDayPremisesByPremiseId = openDayPremisesByPremiseId;
  }

  @OneToMany(mappedBy = "premiseByPremisesId")
  public Collection<ServicePremise> getServicePremisesByPremiseId() {
    return servicePremisesByPremiseId;
  }

  public void setServicePremisesByPremiseId(Collection<ServicePremise> servicePremisesByPremiseId) {
    this.servicePremisesByPremiseId = servicePremisesByPremiseId;
  }

  @OneToMany(mappedBy = "premiseByPremiseId")
  public Collection<VolunteeringOpportunity> getVolunteeringOpportunitiesByPremiseId() {
    return volunteeringOpportunitiesByPremiseId;
  }

  public void setVolunteeringOpportunitiesByPremiseId(Collection<VolunteeringOpportunity> volunteeringOpportunitiesByPremiseId) {
    this.volunteeringOpportunitiesByPremiseId = volunteeringOpportunitiesByPremiseId;
  }
}
