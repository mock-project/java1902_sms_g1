package fa.sms.entities;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserDetail extends User implements UserDetails{

  private User user;
  
  
  public MyUserDetail() {
    
  }
  
  public MyUserDetail(User user) {
    this.user = user;
  }
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
    setAuths.add(new SimpleGrantedAuthority("ROLE_"+user.getRole()));
    return setAuths;
  }

  @Override
  public String getUsername() {
    // TODO Auto-generated method stub
    return user.getAccount();
  }

  @Override
  public boolean isAccountNonExpired() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    // TODO Auto-generated method stub
    return true;
  }

  @Override
  public boolean isEnabled() {
    // TODO Auto-generated method stub
    return true;
  }
  
  @Override
  public String getPassword() {
    return user.getPassword();
  }

  public User getUser() {
    return user;
  }
  
  

  public void setUser(User user) {
    this.user = user;
  }

}
