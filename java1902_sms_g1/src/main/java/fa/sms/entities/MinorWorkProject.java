package fa.sms.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "Minor_work_project", schema = "dbo", catalog = "SMS")
public class MinorWorkProject {
  private int minorworkProjectId;
  private String description;
  private String type;
  private Boolean isTba;
  private String notesActions;
  private Integer estimatedCost;
  private Integer actualCost;
  private Integer directorate;
  private int contact;
  private int authorisedByName;
  private String status;
  private Timestamp enquiryReceivedDate;
  private Timestamp authorisedDate;
  private Timestamp actualStartDate;
  private Timestamp anticipatedCompletionDate;
  private Timestamp actualCompletionDate;
  private Premise premiseByPremisepremiseId;

  @Id
  @Column(name = "minorwork_project_id", nullable = false)
  public int getMinorworkProjectId() {
    return minorworkProjectId;
  }

  public void setMinorworkProjectId(int minorworkProjectId) {
    this.minorworkProjectId = minorworkProjectId;
  }

  @Basic
  @Column(name = "description", nullable = true, length = 100)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "type", nullable = true, length = 20)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Basic
  @Column(name = "is_tba", nullable = true)
  public Boolean getTba() {
    return isTba;
  }

  public void setTba(Boolean tba) {
    isTba = tba;
  }

  @Basic
  @Column(name = "notes_actions", nullable = true, length = 100)
  public String getNotesActions() {
    return notesActions;
  }

  public void setNotesActions(String notesActions) {
    this.notesActions = notesActions;
  }

  @Basic
  @Column(name = "estimated_cost", nullable = true)
  public Integer getEstimatedCost() {
    return estimatedCost;
  }

  public void setEstimatedCost(Integer estimatedCost) {
    this.estimatedCost = estimatedCost;
  }

  @Basic
  @Column(name = "actual_cost", nullable = true)
  public Integer getActualCost() {
    return actualCost;
  }

  public void setActualCost(Integer actualCost) {
    this.actualCost = actualCost;
  }

  @Basic
  @Column(name = "directorate", nullable = true)
  public Integer getDirectorate() {
    return directorate;
  }

  public void setDirectorate(Integer directorate) {
    this.directorate = directorate;
  }

  @Basic
  @Column(name = "contact", nullable = false)
  public int getContact() {
    return contact;
  }

  public void setContact(int contact) {
    this.contact = contact;
  }

  @Basic
  @Column(name = "authorised_by_name", nullable = false)
  public int getAuthorisedByName() {
    return authorisedByName;
  }

  public void setAuthorisedByName(int authorisedByName) {
    this.authorisedByName = authorisedByName;
  }

  @Basic
  @Column(name = "status", nullable = true, length = 30)
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Basic
  @Column(name = "enquiry_received_date", nullable = false)
  public Timestamp getEnquiryReceivedDate() {
    return enquiryReceivedDate;
  }

  public void setEnquiryReceivedDate(Timestamp enquiryReceivedDate) {
    this.enquiryReceivedDate = enquiryReceivedDate;
  }

  @Basic
  @Column(name = "authorised_date", nullable = true)
  public Timestamp getAuthorisedDate() {
    return authorisedDate;
  }

  public void setAuthorisedDate(Timestamp authorisedDate) {
    this.authorisedDate = authorisedDate;
  }

  @Basic
  @Column(name = "actual_start_date", nullable = true)
  public Timestamp getActualStartDate() {
    return actualStartDate;
  }

  public void setActualStartDate(Timestamp actualStartDate) {
    this.actualStartDate = actualStartDate;
  }

  @Basic
  @Column(name = "anticipated_completion_date", nullable = true)
  public Timestamp getAnticipatedCompletionDate() {
    return anticipatedCompletionDate;
  }

  public void setAnticipatedCompletionDate(Timestamp anticipatedCompletionDate) {
    this.anticipatedCompletionDate = anticipatedCompletionDate;
  }

  @Basic
  @Column(name = "actual_completion_date", nullable = true)
  public Timestamp getActualCompletionDate() {
    return actualCompletionDate;
  }

  public void setActualCompletionDate(Timestamp actualCompletionDate) {
    this.actualCompletionDate = actualCompletionDate;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MinorWorkProject that = (MinorWorkProject) o;
    return minorworkProjectId == that.minorworkProjectId &&
            contact == that.contact &&
            authorisedByName == that.authorisedByName &&
            Objects.equals(description, that.description) &&
            Objects.equals(type, that.type) &&
            Objects.equals(isTba, that.isTba) &&
            Objects.equals(notesActions, that.notesActions) &&
            Objects.equals(estimatedCost, that.estimatedCost) &&
            Objects.equals(actualCost, that.actualCost) &&
            Objects.equals(directorate, that.directorate) &&
            Objects.equals(status, that.status) &&
            Objects.equals(enquiryReceivedDate, that.enquiryReceivedDate) &&
            Objects.equals(authorisedDate, that.authorisedDate) &&
            Objects.equals(actualStartDate, that.actualStartDate) &&
            Objects.equals(anticipatedCompletionDate, that.anticipatedCompletionDate) &&
            Objects.equals(actualCompletionDate, that.actualCompletionDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(minorworkProjectId, description, type, isTba, notesActions, estimatedCost, actualCost, directorate, contact, authorisedByName, status, enquiryReceivedDate, authorisedDate, actualStartDate, anticipatedCompletionDate, actualCompletionDate);
  }

  @ManyToOne
  @JoinColumn(name = "Premisepremise_id", referencedColumnName = "premise_id", nullable = false)
  public Premise getPremiseByPremisepremiseId() {
    return premiseByPremisepremiseId;
  }

  public void setPremiseByPremisepremiseId(Premise premiseByPremisepremiseId) {
    this.premiseByPremisepremiseId = premiseByPremisepremiseId;
  }
}
