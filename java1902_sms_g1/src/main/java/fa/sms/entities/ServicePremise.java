package fa.sms.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class ServicePremise {
	private int servicePremiseId;
	private String projectCode;
	private Service serviceByServiceId;
	private Premise premiseByPremisesId;

	@Id
	@Column(name = "service_premise_id", nullable = false)
	public int getServicePremiseId() {
		return servicePremiseId;
	}

	public void setServicePremiseId(int servicePremiseId) {
		this.servicePremiseId = servicePremiseId;
	}

	@Basic
	@Column(name = "project_code", nullable = true, length = 100)
	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ServicePremise that = (ServicePremise) o;
		return servicePremiseId == that.servicePremiseId && Objects.equals(projectCode, that.projectCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(servicePremiseId, projectCode);
	}

	@ManyToOne
	@JoinColumn(name = "service_id", referencedColumnName = "service_id", nullable = false)
	public Service getServiceByServiceId() {
		return serviceByServiceId;
	}

	public void setServiceByServiceId(Service serviceByServiceId) {
		this.serviceByServiceId = serviceByServiceId;
	}

	@ManyToOne
	@JoinColumn(name = "premises_id", referencedColumnName = "premise_id", nullable = false)
	public Premise getPremiseByPremisesId() {
		return premiseByPremisesId;
	}

	public void setPremiseByPremisesId(Premise premiseByPremisesId) {
		this.premiseByPremisesId = premiseByPremisesId;
	}
}
