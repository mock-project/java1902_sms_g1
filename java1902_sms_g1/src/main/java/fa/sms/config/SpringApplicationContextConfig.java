package fa.sms.config;

import java.util.Properties;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@Import({SpringWebConfig.class})
@ComponentScan("fa.sms")
@EnableCaching
public class SpringApplicationContextConfig {
  
  @Bean
  public static PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer() {
    PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
    propertyPlaceholderConfigurer.setLocation(new ClassPathResource("message.properties"));
    return propertyPlaceholderConfigurer;
  }
  
  @Bean
  public JavaMailSender getJavaMailSender() {
	  JavaMailSenderImpl sender = new JavaMailSenderImpl();
	  sender.setHost("smtp.gmail.com");
	  sender.setPort(587);
	  sender.setPassword("1qazsedcvgyhnmju");
	  sender.setUsername("marocvi89@gmail.com");
	  Properties props = sender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.debug", "true");
	  return sender;
  }
  
 

}
