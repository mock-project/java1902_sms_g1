package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {
	public Contact findByContactName(String contactName);
}
