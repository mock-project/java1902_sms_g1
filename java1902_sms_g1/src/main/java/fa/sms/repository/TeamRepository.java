package fa.sms.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fa.sms.entities.Department;
import fa.sms.entities.Team;

public interface TeamRepository extends CrudRepository<Team, Integer> {
  public Team findByTeamId(int id);

  public List<Team> findByDepartmentByDepartmentId(Department departmentId);

  @Query("SELECT t FROM Team t WHERE t.departmentByDepartmentId IS NULL")
  public List<Team> findByDepartmentByDepartmentId();

}
