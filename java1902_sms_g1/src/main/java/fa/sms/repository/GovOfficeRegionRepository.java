/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.County;
import fa.sms.entities.GovOfficeRegion;

public interface GovOfficeRegionRepository
    extends JpaRepository<GovOfficeRegion, Integer> {
  public List<GovOfficeRegion> findByCounty(County county);
}
