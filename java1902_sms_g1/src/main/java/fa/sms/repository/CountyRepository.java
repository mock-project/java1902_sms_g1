/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.County;

public interface CountyRepository extends JpaRepository<County, Integer> {
  public County findByCountyName(String name);
}
