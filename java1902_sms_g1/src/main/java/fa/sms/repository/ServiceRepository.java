package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Service;

public interface ServiceRepository extends JpaRepository<Service, Integer> {
	public Service findByServiceName(String contactName);
}
