package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Premise;

public interface PremiseRepository  extends JpaRepository<Premise, Integer> {
	
}
