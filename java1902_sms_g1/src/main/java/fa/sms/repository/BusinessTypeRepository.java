package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.BussinessType;

public interface BusinessTypeRepository extends JpaRepository<BussinessType, Integer>{
	public BussinessType findByBussinessName(String bussinessName);
}
