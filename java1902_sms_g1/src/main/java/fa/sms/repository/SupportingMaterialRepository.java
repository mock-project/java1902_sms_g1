package fa.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fa.sms.entities.OrganisationDetail;
import fa.sms.entities.SupportingMaterial;

public interface SupportingMaterialRepository extends JpaRepository<SupportingMaterial, Integer> {
	
	@Query(value = "SELECT * FROM organisation.Supporting_Material sm WHERE sm.organisation_detail_id = :organisation_detail_id or sm.organisation_detail_id IS NULL", 
			  nativeQuery = true)
	public List<SupportingMaterial> findByOrganisationDetailByOrganisationDetailId(@Param("organisation_detail_id") OrganisationDetail organisationDetailByOrganisationDetailId);
}
