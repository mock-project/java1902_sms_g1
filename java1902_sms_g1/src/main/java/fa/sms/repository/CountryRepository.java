package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {

}
