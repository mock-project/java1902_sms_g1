package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Organisation;

public interface OrganisationRepository extends JpaRepository<Organisation, Integer> {
	public Organisation findByOrganisationName(String name);
}
