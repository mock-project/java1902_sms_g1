package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Directorate;

public interface DirectorateRepository extends JpaRepository<Directorate, Integer> {
	public Directorate findByDirectorateName(String directorateName);
  
}
