package fa.sms.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fa.sms.entities.Department;
@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer>{
	//public void saveOrUpdate(Department department);
	public Department findByDepartmentId(int id);
	public Department findByDepartmentName(String departmentName);
    
}
