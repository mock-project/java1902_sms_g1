/*
 * @author Le Duy Hien
 * version 1.0
 */
package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import fa.sms.entities.ServiceOrganization;

public interface ServiceOrganisationRepository extends JpaRepository<ServiceOrganization, Integer> {

}
