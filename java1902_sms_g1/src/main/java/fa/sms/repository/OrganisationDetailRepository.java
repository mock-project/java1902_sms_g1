package fa.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fa.sms.entities.Organisation;
import fa.sms.entities.OrganisationDetail;

public interface OrganisationDetailRepository extends JpaRepository<OrganisationDetail, Integer> {
	public OrganisationDetail findByOrganisationByOrganisationId(Organisation organisation);
}
