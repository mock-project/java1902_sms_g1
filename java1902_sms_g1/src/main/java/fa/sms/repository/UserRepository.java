package fa.sms.repository;

import org.springframework.data.repository.CrudRepository;

import fa.sms.entities.User;

public interface UserRepository extends CrudRepository<User, Integer> {
  
  public User findUserByAccount(String account);
  public User findUserByEmail(String email);
  
  
}
