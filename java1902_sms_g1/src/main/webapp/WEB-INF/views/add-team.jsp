
<div class="borders">
	<a href="#" style="text-decoration: underline;">Organization</a>> <a
		href="#" style="text-decoration: underline;">Directorate</a>> <a
		href="#" style="text-decoration: underline;">Department</a> <br>
	<h5 style="color: #00cc00;">Team Details</h5>
	<div id="containerOrganisation">
		<div id="sortListOrganisations">
			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<a class="nav-item nav-link active mr-auto" id="nav-detail-1-tab"
						data-toggle="tab" href="#nav-detail-1" role="tab"
						aria-controls="nav-detail-1" aria-selected="true">Details</a>
					<button type="button" class="btn btn-outline-dark"
						style="float: right; margin-right: 10px" id="backtoListTeam">Back</button>
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnSaveTeam">Save</button>
				</div>
			</nav>
		</div>

	</div>
	<div id="insideborder">
		<div id="formDisplay" style="margin-top: 4px;">
		    <label id="teamId" hidden="">${TEAM.teamId}</label>
			<div class="r" style="width: 100%; padding: 5px;">
				<form id="addTeam">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" /> 
					<div class="tab-content" id="nav-tabContent">

						<!-- Detail 1 -->
						<div class="tab-pane fade show active" id="nav-detail-1"
							role="tabpanel" aria-labelledby="nav-detail-1-tab">
							<div class="form-group row">
								<label for="teamName" class="col-sm-2 col-form-label">Team
									name <span>*</span>
								</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="teamName"
										name="teamName" value="${TEAM.teamName}">
								</div>
								<label for="typeOfBusiness" class="col-sm-2 col-form-label">Type
									of business<span>*</span>
								</label>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="text" class="form-control" id="typeOfBusiness"
											name="typeOfBusiness" readonly="readonly">
									</div>
								</div>
								<div class="col-sm-1">
									<a href="#">Lookup</a>
								</div>
							</div>

							<div class="form-group row">
								<label for="teamShortDescription"
									class="col-sm-2 col-form-label">Team Short <br>
									Description <span> *</span>
								</label>
								<div class="col-sm-4">
									<div class="form-group">
										<textarea class="form-control" rows="3"
											id="teamShortDescription" name="teamShortDescription">${TEAM.teamShortDescription}</textarea>
									</div>
								</div>
								<label for="sicCode" class="col-sm-2 col-form-label">SIC
									Code</label>
								<div class="col-sm-2">
									<div class="form-group">
										<input type="text" class="form-control" id="sicCode"
											name="sicCode" readonly="readonly">
									</div>
								</div>
							</div>


							<div class="form-group row">
								<label for="leadContact" class="col-sm-2 col-form-label">Lead
									Contact </label>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="text" class="form-control" id="leadContact"
											name="leadContact" readonly="readonly">
									</div>
								</div>
								<div class="col-sm-1">
									<a href="#">Lookup</a>
								</div>
								<label for="teamFullDescription" class="col-sm-2 col-form-label">Team
									Full<br /> Description
								</label>
								<div class="col-sm-4">
									<div class="form-group">
										<textarea class="form-control" rows="3"
											id="teamFullDescription" name="teamFullDescription">${TEAM.teamFullDescription}</textarea>
									</div>
								</div>

							</div>

							<div class="form-group row">
								<label for="leadContact" class="col-sm-2 col-form-label">Copy
									Address From</label>

								<div class="col-sm-2">
									<input type="radio" name="gender" value="male">
									Organizations<br>
								</div>
								<div class="col-sm-2">
									<input type="radio" name="gender" value="male"> Parent<br>
								</div>
							</div>

							<div class="form-group row">
								<label for="addressLine1" class="col-sm-2 col-form-label">Address
									Line 1 <span>*</span>
								</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="addressLine1"
										name="addressLine1" value="${TEAM.addressLine1}">
								</div>
								<label for="phoneNumber" class="col-sm-2 col-form-label">Phone
									number <span>*</span>
								</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="phoneNumber"
										name="phoneNumber" value="${TEAM.phoneNumber}">
								</div>
							</div>

							<div class="form-group row">
								<label for="addressLine2" class="col-sm-2 col-form-label">Address
									Line 2 </label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="addressLine2"
										name="addressLine2" value="${TEAM.addressLine2}">
								</div>
								<label for="fax" class="col-sm-2 col-form-label">Fax </label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="fax" name="fax"
										value="${TEAM.fax}">
								</div>
							</div>

							<div class="form-group row">
								<label for="addressLine3" class="col-sm-2 col-form-label">Address
									Line 3 </label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="addressLine3"
										name="addressLine3" value="${TEAM.addressLine3}">
								</div>
								<label for="email" class="col-sm-2 col-form-label">Email
								</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="email" name="email"
										value="${TEAM.email}">
								</div>
							</div>

							<div class="form-group row">
								<label for="postCode" class="col-sm-2 col-form-label">Postcode
									<span> *</span>
								</label>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="text" class="form-control" id="postCode"
											name="postCode" readonly="readonly" value="${TEAM.postcode}">
									</div>
								</div>
								<div class="col-sm-1">
									<a href="#">Lookup</a>
								</div>
								<label for="webAddress" class="col-sm-2 col-form-label">Web
									Address </label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="webAddress"
										name="webAddress" value="${TEAM.webAddress}">
								</div>
							</div>

							<div class="form-group row">
								<label for="city" class="col-sm-2 col-form-label">City/Town</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="town" name="town"
										value="${TEAM.town}">
								</div>

								<div class="col-sm-4"></div>
							</div>

							<div class="form-group row">
								<label for="county" class="col-sm-2 col-form-label">County</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="county"
										name="county" value="${TEAM.county}">
								</div>

								<div class="col-sm-4"></div>
							</div>

							<div class="form-group row">
								<label for="nation" class="col-sm-2 col-form-label">Nation/County</label>
								<div class="col-sm-3">
									<div class="form-group">
										<select class="form-control" id="nation">
											<option disabled="disabled" selected="selected">${TEAM.country}</option>
											<option>Vietnam</option>
											<option>Thailand</option>
											<option>Malaysia</option>
											<option>Korea</option>
											<option>Japan</option>
										</select>
									</div>
								</div>
								<div class="col-sm-1"></div>

								<div class="col-sm-4"></div>
							</div>
						</div>

						<!-- Detail 2 -->

						<!-- Detail 3 -->

					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="contextPath" hidden=""><%=request.getContextPath()%></div>
	<div id="error">
		<p>Please input the organisation name</p>

	</div>
</div>
 <script type="text/javascript">
$(document).ready(function(){
		var contextPath =$("#contextPath").text();
		$("#backtoListTeam").click(function() {
			alert("duong");
			//location.reload();
		});
		
		$("#btnSaveTeam").click(function() {
			alert("duong");
			//String teamId =$("#teamId").text();
			formdata = new FormData($("#addTeam")[0]); 
			var country=$( "#addTeam option:selected" ).text();
			console.log("country2"+country);
			formdata.append('country', country);
		 /* 	 if(teamId!=null)
				{
				formdata.append('teamId', teamId);
				}  */ 
			console.log(formdata);
			$.ajax({
		        url         : contextPath+'/user/save-team',
		        data        : formdata,
		        cache       : false,
		        contentType : false,
		        processData : false,
		        type        : 'POST',
		        success     : function(result){
		            // Callback code
		        	location.reload();
		        }
		    });
		}); 
		});
		
		</script>
