<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}"></c:set>

<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home</title>

    <!--Bootstrap-->
    <link rel="stylesheet" href="${context}/resources/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${context}/resources/css/home.css">
    <link rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
</head>
<body class="h-100">
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="#"> <img src="${context}/resources/image/fpt.jpg" alt="FPT Software" style="width: 45px;"></a>
     <a class="navbar-brand" id="system" href="#">SMS SYSTEM MANAGEMENT</a> 
    <ul class="navbar-nav  ml-auto">
        <li class="nav-item d-flex">
                <div class="mr-2 align-self-center" style="color:#fff">marocvi</div>
        </li>
        
        <li class="dropdown nav-item my-0">
                <a class="nav-link dropdown-toggle" style="color: #fff" href="#"
                id="navbarDropdown" data-toggle="dropdown"><i
                    class="fas fa-user"></i></a>
                <div class="dropdown-menu dropdown-menu-right shadow  ">
                    <a class="dropdown-item" href="${context}/user/logout"
                        style="color: #0b2e13"><i class="fas fa-sign-out-alt"></i>
                        Logout</a>
                </div>
            </li>

    </ul>

</nav>

    <div id="content-wrap">
            <div class="container-fluid">
                <div class="row" id="mainView">
                    <div class="d-flex" id="wrapper" style="width: 100%;">

                        <!-- Sidebar -->
                        <div class="bg-light border-right" id="sidebar-wrapper">
                            <div class="d-flex justify-content-center">
                                <div class="sidebar-heading">MENU</div>
                            </div>
                            
                            <div class="list-group list-group-flush" id="selected">
                                <a href="#" class="list-group-item list-group-item-action"
                                    id="list-organisations">Organisation</a> <a href="#"
                                    class="list-group-item list-group-item-action">Services</a> <a
                                    href="#" class="list-group-item list-group-item-action">Geography</a>
                                <a href="#" class="list-group-item list-group-item-action">Premises</a>
                            </div>
                        </div>
                        <!-- /#sidebar-wrapper -->

                        <!-- Page Content -->
                        <div id="page-content-wrapper">
                            <div class="container-fluid" id="dContent">
                                <h1 class="mt-4">WELCOME TO SMS SYSTEM MANAGEMENT</h1>
                            </div>
                        </div>
                        <!-- /#page-content-wrapper -->

                    </div>
                </div>
            </div>
        </div>



<footer id="footer" class="py-3 bg-dark text-white-50 fixed-bottom">
    <div class="container text-center">
        <small>Copyright &copy; Java1902 - Mock Project - Group 1</small>
    </div>
</footer>
<!--Jquery-->
<script src="${context}/resources/js/jquery/jquery.min.js"></script>
<!--Bootstrap-->
<script src="${context}/resources/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="${context}/resources/js/home.js"></script>
</body>
</html>