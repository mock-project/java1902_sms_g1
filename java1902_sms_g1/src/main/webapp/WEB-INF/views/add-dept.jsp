
<div id="containerOrganisation" style="padding: 10px">
	<a href="#" style="text-decoration: underline;">Organization</a> > <a
		href="#" style="text-decoration: underline;">Directorate</a> <br>
	<h5 style="color: #00cc00;">Department Details</h5>
	<div id="sortListOrganisations">
		<ul id="clothing-nav" class="nav nav-tabs" role="tablist">

			<li class="nav-item"><a class="nav-link active" href="#home"
				id="home-tab" role="tab" data-toggle="tab" aria-controls="home"
				aria-expanded="true">Details</a></li>

			<li class="nav-item mr-auto"><a class="nav-link " href="#hats"
				role="tab" id="hats-tab" data-toggle="tab" aria-controls="hats"
				>Teams</a></li>
			<div id="actions">
				<button type="button" class="btn btn-outline-primary"
					style="float: right;" id="backtoListDept">Back</button>
				<button type="button" class="btn btn-outline-primary"
					style="float: right;" id="btnSave">Save</button>
				<button type="button" class="btn btn-outline-primary"
					style="float: right;" id="btnInActive">In-Active</button>
			</div>


		</ul>
	</div>

	<!-- Content Panel -->
	<div id="clothing-nav-content" class="tab-content" style="padding: 10px">

		<div role="tabpanel" class="tab-pane fade show active" id="home"
			aria-labelledby="home-tab">
	
			
			<form id="addDept" >
			  <input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" id="token"/>
				<label id="departmentId" hidden="">${DEPT.departmentId}</label>
				<div class="form-group row">
					<label for="departmentName" class="col-sm-2 col-form-label">Department
						name <span>*</span>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="departmentName"
							name="departmentName" value="${DEPT.departmentName}">
					</div>
					<label for="" class="col-sm-2 col-form-label">Type of
						business<span>*</span>
					</label>
					<div class="col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control" id="typeOfBusiness"
								name="typeOfBusiness" readonly="readonly">
						</div>
					</div>
					<div class="col-sm-1">
						<a href="#">Lookup</a>
					</div>
				</div>

				<div class="form-group row">
					<label for="departmentShortDescription"
						class="col-sm-2 col-form-label">Department Short <br>
						Description <span> *</span>
					</label>
					<div class="col-sm-4">
						<div class="form-group">
							<textarea class="form-control" rows="3"
								id="departmentShortDescription"
								name="departmentShortDescription">${DEPT.departmentShortDescription}</textarea>
						</div>
					</div>
					<label for="sicCode" class="col-sm-2 col-form-label">SIC
						Code</label>
					<div class="col-sm-2">
						<div class="form-group">
							<input type="text" class="form-control" id="sicCode"
								name="sicCode" readonly="readonly">
						</div>
					</div>
				</div>


				<div class="form-group row">
					<label for="leadContact" class="col-sm-2 col-form-label">Lead
						Contact </label>
					<div class="col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control" id="leadContact"
								name="leadContact" readonly="readonly">
						</div>
					</div>
					<div class="col-sm-1">
						<a href="#">Lookup</a>
					</div>
					<label for="departmentFullDescription"
						class="col-sm-2 col-form-label">Department Full<br />
						Description
					</label>
					<div class="col-sm-4">
						<div class="form-group">
							<textarea class="form-control" rows="3"
								id="departmentFullDescription" name="departmentFullDescription">${DEPT.departmentFullDescription}</textarea>
						</div>
					</div>

				</div>

				<div class="form-group row">
					<label for="leadContact" class="col-sm-2 col-form-label">Copy
						Address From</label>

					<div class="col-sm-2">
						<input type="radio" name="gender" value="male">
						Organizations<br>
					</div>
					<div class="col-sm-2">
						<input type="radio" name="gender" value="male"> Parent<br>
					</div>
				</div>

				<div class="form-group row">
					<label for="addressLine1" class="col-sm-2 col-form-label">Address
						Line 1 <span>*</span>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="addressLine1"
							name="addressLine1" value="${DEPT.addressLine1}">
					</div>
					<label for="phoneNumber" class="col-sm-2 col-form-label">Phone
						number <span>*</span>
					</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="phoneNumber"
							name="phoneNumber" value="${DEPT.phoneNumber}">
					</div>
				</div>

				<div class="form-group row">
					<label for="addressLine2" class="col-sm-2 col-form-label">Address
						Line 2 </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="addressLine2"
							name="addressLine2" value="${DEPT.addressLine2}">
					</div>
					<label for="fax" class="col-sm-2 col-form-label">Fax </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="fax" name="fax"
							value="${DEPT.fax}">
					</div>
				</div>

				<div class="form-group row">
					<label for="addressLine3" class="col-sm-2 col-form-label">Address
						Line 3 </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="addressLine3"
							name="addressLine3" value="${DEPT.addressLine3}">
					</div>
					<label for="email" class="col-sm-2 col-form-label">Email </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="email" name="email"
							value="${DEPT.email}">
					</div>
				</div>

				<div class="form-group row">
					<label for="postCode" class="col-sm-2 col-form-label">Postcode
						<span> *</span>
					</label>
					<div class="col-sm-3">
						<div class="form-group">
							<input type="text" class="form-control" id="postCode"
								name="postCode" readonly="readonly" value="${DEPT.postcode}">
						</div>
					</div>
					<div class="col-sm-1">
						<a href="#">Lookup</a>
					</div>
					<label for="webAddress" class="col-sm-2 col-form-label">Web
						Address </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="webAddress"
							name="webAddress" value="${DEPT.webAddress}">
					</div>
				</div>

				<div class="form-group row">
					<label for="city" class="col-sm-2 col-form-label">City/Town</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="town" name="town"
							value="${DEPT.town}">
					</div>

					<div class="col-sm-4"></div>
				</div>

				<div class="form-group row">
					<label for="county" class="col-sm-2 col-form-label">County</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="county" name="county"
							value="${DEPT.county}">
					</div>

					<div class="col-sm-4"></div>
				</div>

				<div class="form-group row">
					<label for="nation" class="col-sm-2 col-form-label">Nation/County</label>
					<div class="col-sm-3">
						<div class="form-group">
							<select class="form-control" id="nation">
								<option disabled="disabled" selected="selected">${DEPT.country}</option>
								<option>Vietnam</option>
								<option>Thailand</option>
								<option>Malaysia</option>
								<option>Korea</option>
								<option>Japan</option>
							</select>
						</div>
					</div>
					<div class="col-sm-1"></div>

					<div class="col-sm-4"></div>
				</div>
			</form>
		</div>

		<div role="tabpanel" class="tab-pane fade" id="hats"
			aria-labelledby="hats-tab">
			<div class="row float-right d-flex align-items-center">
				<button type="button" class="btn btn-primary " id="create-team">Create</button>
				&nbsp; &nbsp; <input type="checkbox" name="vehicle1" value="Bike"
					class="hide-if-empty" id="include-inactive"> <span
					class="hide-if-empty" style="padding-right: 20px">&nbsp;Include
					in-Yes</span><br>
			</div>
			<br>
			<div class="d-flex p-2">
				<div id="sortListDepartment">
					<a href="#" class="badge badge-primary" id="all">All</a> | <a
						href="#" class="badge badge-primary" id="09">0 - 9</a> | <a
						href="#" class="badge badge-primary" id="ABCDE">A B C D E</a> | <a
						href="#" class="badge badge-primary" id="FGHIJ">F G H I J</a> | <a
						href="#" class="badge badge-primary">K L M N</a> | <a href="#"
						class="badge badge-primary">O P Q R</a> | <a href="#"
						class="badge badge-primary">S T U V</a> | <a href="#"
						class="badge badge-primary">W X Y Z</a>
				</div>
			</div>
			<div id="tableDisplay">
				<table class="table table-bordered table-striped"
					id="colorRowsTable">
					<thead>
						<tr>
							<th hidden="">Id</th>
							<th scope="col">Team Name</th>
							<th scope="col">Address Line 1</th>
							<th scope="col">Postcode</th>
							<th scope="col">Contact</th>
							<th scope="col">Is Active</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${LIST_TEAM}" var="team">
							<tr>
								<td hidden="">${team.teamId}</td>
								<td class="amendTeam"><a href="#">${team.departmentName}</a></td>
								<td>${team.addressLine1}</td>
								<td>${team.postcode}</td>
								<td>${team.contactByContactId.contactName}</td>
								<td>${team.active}</td>

							</tr>

						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>



	</div>
</div>

<div id="contextPath" hidden=""><%=request.getContextPath()%></div>

<!-- jQuery library -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>

Popper
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
 -->
<!-- Latest compiled and minified Bootstrap JavaScript -->
<!-- <script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

 -->
<script>
$(document).ready(function(){
	
    var contextPath=$("#contextPath").text();
    //team-start
    $("#create-team").click(function() {
    	
		$.get(contextPath+"/user/add-team",
				
				function(result) {
					$("#dContent").html(result);
				}
				)
		
	});
    $(".amendTeam").click(function() {
		var updateRow = $(this).parent();
        var teamId = updateRow.children().first().text();
		//var departmentId = parseInt(departmentId);
        console.log(teamId);
       $.get(contextPath+"/user/add-team",
				{teamId:teamId},
				function(result) {
					$("#dContent").html(result);
				}
				);

	});
    //team-end
    
	$("#backtoListDept").click(function() {
		$.get(contextPath + "/user/dept-list", function(result) {
			$("#dContent").html(result);

		});
	});
	$("#btnInActive").click(function() {
		var tokenVal = $("#token").val();
		$.post(contextPath + "/user/in-active-dept", {
			departmentId : $("#departmentId").text(),
			_csrf: tokenVal
			
		}, function(result) {
			$("#dContent").html(result);
		});
	});
	$("#btnSave").click(function() {
		var departmentId = $("#departmentId").text();

		var formdata = new FormData($("#addDept")[0]);
		if (departmentId) {
			console.log("test" + departmentId);
			formdata.append('departmentId', departmentId);
		}
		var country = $("#addDept option:selected").text();
		console.log("departmentId" + departmentId);
		
		console.log("country2" + country);
		
		formdata.append('country', country);
		console.log(formdata);
		$.ajax({
			url : contextPath + '/user/save-dept',
			data : formdata,
			cache : false,
			contentType : false,
			processData : false,
			type : 'POST',
			success : function(result) {
				// Callback code
				$("#dContent").html(result);
			}
		});
	});});
</script>
