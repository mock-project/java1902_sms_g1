<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% %>
<c:set var="context" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Login</title>
<!--Bootstrap-->
<link rel="stylesheet"
	href="${context}/resources/css/bootstrap/bootstrap.min.css">
</head>
<body class="h-100">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
		<a class="navbar-brand" href="#"> <img
			src="${context}/resources/image/fpt.jpg" alt="FPT Software"
			style="width: 45px;">
		</a> <a class="navbar-brand" id="system" href="#">SMS SYSTEM
			MANAGEMENT</a>
	</nav>
	<div class="container" style="margin-top: 150px">
		<div class="row">
			<div class="col-4 offset-4">
				<div class="card">
					<div class="card-header">
						<label>Please Login</label>
					</div>
					<div class="card-body">
						<p>${error}</p>
						<form id="frm-login" action="#">
							<div class="form-group">
								<input type="text" placeholder="Enter username."
									class="form-control " id="username" name="username">
							</div>
							<div class="form-group">
								<input type="password" placeholder="Enter password."
									class="form-control" id="password" name="password">
							</div>
							<div>
								<a class="card-link " href="${context}/forget-password">Forget Password</a>
							</div>

							<div class="mt-3">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button class="btn btn-primary " type="submit" id="login-btn">
									Login</button>
							</div>

						</form>
					</div>
				</div>

			</div>
		</div>
		<div class="row mt-3">
			<div class="col-4 offset-4">
				<div class="toast" data-delay="4000">
					<div class="toast-header">
						<strong style="color: red;">ERROR</strong>
					</div>
					<div class="toast-body">
						<p>Invalid username and password</p>
					</div>
				</div>
			</div>
			<div class="row mt-3"></div>

		</div>
	</div>

	<footer id="footer" class="py-3 bg-dark text-white-50 fixed-bottom">
		<div class="container text-center">
			<small>Copyright &copy; Java1902 - Mock Project - Group 1</small>
		</div>
	</footer>
	<!--Jquery-->
	<script src="${context}/resources/js/jquery/jquery.min.js"></script>
	<!--Bootstrap-->
	<script src="${context}/resources/js/bootstrap/bootstrap.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			jQuery.validator.addMethod("validUsername", function(value, element) {
				  return this.optional(element) || new RegExp("^[^<>%*$()+-.;,'?`~]*$").test(value);
				}, "Invalid username (Must be a-zA-Z0-9)");
		
			  jQuery.validator.addMethod("validPassword", function(value, element) {
				  return this.optional(element) ||true;
				}, "Invalid password (Must be a-zA-Z0-9 at least a special character (!,@,#,$)");
			
			$("#frm-login").validate({
		        rules : {
		            username : {
		                required : true,
		                minlength : 4,
		                validUsername: true
		            },
		            password : {
		                required : true,
		                minlength : 4,
		                validPassword: true
		            }
		        },
		        messages:{
		        	username :{
		        		required: "Username is required",
		        		minlength: "Invalid username (Must be length greater than or equal 8)",
		        		validUsername: "Invalid username (Must be a-zA-Z0-9)"
		        	},
		        	password: {
		        		required: "Password is required",
		        		minlength: "Invalid password (Must be length greater than or equal 8)",
		        		validPassword: "Invalid password (Must be a-zA-Z0-9 at least a special character (!,@,#,$)"
		        		
		        	}
		        },
		        highlight : function(element, errorClass, validClass) {
		            $(element).addClass("border-danger").removeClass("border-success");
		        },
		        unhighlight : function(element, errorClass, validClass) {
		            $(element).addClass("border-success").removeClass("border-danger");
		        },
		        submitHandler : function() {
		        	var data = {
		        			username : $('#username').val(),
		        			password : $('#password').val(),
		        			_csrf : "${_csrf.token}"
		        	}
		        	$.ajax({
		        		type: "POST",
		        		url: "${context}/login",
		        		data: data,
		        		success: function(){
		        			window.location.replace("${context}/user/homepage"); 
		        		},
		    			error: function (xhr,textStatus,errorThrown){
		    				$('.toast').toast('show');
		    				$('#username').addClass("border-danger").removeClass("border-success");
		    				$('#password').addClass("border-danger").removeClass("border-success");
		    			}
		        	})
		        	
		            
		        }
		    });
		})
	</script>
</body>
</html>