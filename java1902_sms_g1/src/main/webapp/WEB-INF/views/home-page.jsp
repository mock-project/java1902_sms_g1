<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SMS System Management</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">

<link href="<%=request.getContextPath()%>/resources/css/organisation/home-page.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/organisation/list-organisations.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/organisation/add-organisation.css"
	rel="stylesheet">
	<script src="<%=request.getContextPath()%>/resources/js/jquery/sweetalert.min.js"></script>
</head>
<body>
	<div id="page-container">
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
			<a class="navbar-brand" href="#"> <img
				src="<%=request.getContextPath()%>/resources/image/fpt.jpg"
				alt="FPT Software" style="width: 45px;">
			</a> <a class="navbar-brand" id="system" href="#">SMS SYSTEM
				MANAGEMENT</a>
		</nav>

		<div id="content-wrap">
			<div class="container-fluid">
				<div class="row" id="mainView">
					<div class="d-flex" id="wrapper" style="width: 100%;">

						<!-- Sidebar -->
						<div class="bg-light border-right" id="sidebar-wrapper">
							<div class="sidebar-heading">MENU</div>
							<div class="list-group list-group-flush" id="selected">
								<a href="#" class="list-group-item list-group-item-action"
									id="list-organisations">Organisation</a> <a href="#"
									class="list-group-item list-group-item-action">Services</a> <a
									href="#" class="list-group-item list-group-item-action">Geography</a>
								<a href="#" class="list-group-item list-group-item-action">Premises</a>
							</div>
						</div>
						<!-- /#sidebar-wrapper -->

						<!-- Page Content -->
						<div id="page-content-wrapper">
							<div class="container-fluid" id="dContent">
								<h1 class="mt-4">WELCOME TO SMS SYSTEM MANAGEMENT</h1>
							</div>
						</div>
						<!-- /#page-content-wrapper -->

					</div>
				</div>
			</div>
		</div>
		<footer id="footer" class="py-4 bg-dark text-white-50">
			<div class="container text-center">
				<small>Copyright &copy; Java1902 - Mock Project - Group 1</small>
			</div>
		</footer>
	</div>
	<script src="<%=request.getContextPath()%>/resources/js/jquery/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/resources/js/jquery/popper.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/jquery/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script src="<%=request.getContextPath()%>/resources/js/actions/home-page.js"></script>

</body>
</html>
