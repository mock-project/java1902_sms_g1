<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Forget Password</title>

<!--Bootstrap-->
<link rel="stylesheet"
	href="${context}/resources/css/bootstrap/bootstrap.min.css">
</head>
<body class="h-100">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
		<a class="navbar-brand" href="#"> <img
			src="${context}/resources/image/fpt.jpg" alt="FPT Software"
			style="width: 45px;">
		</a> <a class="navbar-brand" id="system" href="#">SMS SYSTEM
			MANAGEMENT</a>
	</nav>

	<div class="container" style="margin-top: 150px;">

		<div class="row">
			<div class="col-4 offset-4">
				<div class="card">
					<div class="card-header">Forget Password</div>
					<div class="card-body">
						<form method="post" action="#" id="frmLogin">
							<div class="form-group">
								<input type="text" placeholder="Enter username."
									class="form-control" id="username"
									name="username">
							</div>
							<div class="form-group">
								<input type="text" placeholder="Enter email."
									class="form-control" id="email" name="email">
							</div>
							<div>
								<a class="card-link " href="${context}/login">Click here to
									login.</a>
							</div>
							<div class="mt-3">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button class="btn btn-primary " type="submit" role="button"
									id="get-password">Get Password</button>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
		<div class="row mt-3">
			<div class="col-4 offset-4">
				<div class="toast" data-delay="5000" id="error">
					<div class="toast-header">
						<strong style="color: red;">ERROR</strong>
					</div>
					<div class="toast-body">
						<p>Username and password do not match</p>
					</div>
				</div>
				<div class="toast" data-delay="5000" id="success"">
					<div class="toast-header">
						<strong style="color: green;">SUCCESS</strong>
					</div>
					<div class="toast-body">
						<p>Your password has sent to your email. Please check!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer id="footer" class="py-3 bg-dark text-white-50 fixed-bottom">
		<div class="container text-center">
			<small>Copyright &copy; Java1902 - Mock Project - Group 1</small>
		</div>
	</footer>
	<!--Jquery-->
	<script src="${context}/resources/js/jquery/jquery.min.js"></script>
	<!--Bootstrap-->
	<script src="${context}/resources/js/bootstrap/bootstrap.min.js"></script>
	<script>
		$(document).ready(
				function() {
					$('#get-password').click(
							function(event) {
								event.preventDefault();
								var data = {
									username : $('#username').val(),
									email : $('#email').val(),
									_csrf : "${_csrf.token}"
								}
								$.ajax({
									type : "POST",
									url : "${context}/get-password",
									data : data,
									success : function() {
										$('#success').css("display", "block");
										$('#error').css("display", "none");
										$('#success').toast('show');
										$('#username').addClass(
												"border-success").removeClass(
												"border-danger");
										$('#email').addClass("border-success")
												.removeClass("border-danger");
									},
									error : function() {
										$('#success').css("display", "none");
										$('#error').css("display", "block");
										$('#error').toast('show');

										$('#username')
												.addClass("border-danger")
												.removeClass("border-success");
										$('#email').addClass("border-danger")
												.removeClass("border-success");
									}
								})

							})
				});
	</script>
</body>
</html>





















