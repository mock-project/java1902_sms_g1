<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
</head>
<body>
	<div id="dContent">
		<div class="borders">
			<h5 style="color: #00cc00;">Organisation Details</h5>
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item"><a class="nav-item nav-link active"
					id="nav-detail-1-tab" data-toggle="tab" href="#nav-detail-1"
					role="tab" aria-controls="nav-detail-1" aria-selected="true">Detail
						1</a></li>
				<li class="nav-item"><a class="nav-item nav-link"
					id="nav-detail-2-tab" data-toggle="tab" href="#nav-detail-2"
					role="tab" aria-controls="nav-detail-2" aria-selected="false">Detail
						2</a></li>
				<li class="nav-item"><a class="nav-item nav-link"
					id="nav-detail-3-tab" data-toggle="tab" href="#nav-detail-3"
					role="tab" aria-controls="nav-detail-3" aria-selected="false"
					hidden="hidden">Detail 3</a></li>
				<li class="nav-item"><a class="nav-item nav-link"
					id="nav-detail-4-tab" data-toggle="tab" href="#nav-detail-4"
					role="tab" aria-controls="nav-detail-4" aria-selected="false"
					hidden="hidden">Detail 4</a></li>
				<li class="nav-item"><a class="nav-item nav-link"
					id="nav-detail-5-tab" data-toggle="tab" href="#nav-detail-5"
					role="tab" aria-controls="nav-detail-5" aria-selected="false"
					hidden="hidden">Detail 5</a></li>
				<li class="nav-item"><a class="nav-item nav-link"
					id="nav-detail-6-tab" data-toggle="tab" href="#nav-detail-6"
					role="tab" aria-controls="nav-detail-6" aria-selected="false"
					hidden="hidden">BU/Directorates</a></li>
				<li class="nav-item ml-auto">
					<button class="btn btn-outline-danger" id="btnInActice"
						hidden="hidden">In-active</button>
					<button class="btn btn-outline-primary" id="btnSave">Save</button>
					<button class="btn btn-outline-dark" id="btnBack">Back</button>
					<button class="btn btn-outline-dark" id="btnTest">Test</button>
				</li>
			</ul>
			<div id="insideborder">
				<div id="formDisplay" style="margin-top: 4px;">
					<div class="container-fluid">
						<form id="saveOrganisation">
							<div class="tab-content" id="nav-tabContent">

								<!-- Detail 1 -->
								<div class="tab-pane fade show active" id="nav-detail-1"
									role="tabpanel" aria-labelledby="nav-detail-1-tab">
									<input type="hidden" id="organisationId" name="organisationId"
										value="${organisation.organisationId }">
									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="organisationName" class="control-label">Organisation
														name <span>*</span>
													</label>
												</div>

												<div class="col-md-6">
													<input type="text" name="organisationName"
														class="form-control" id="organisationName"
														value="${organisation.organisationName }" autofocus>
												</div>

											</div>
										</div>


										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="preferredOrganistion" class="control-label">Preferred
														Organisation</label>
												</div>

												<div class="col-md-6">
													<div class="form-check">
														<c:choose>
															<c:when
																test="${organisation.preferredOrganisation == 'Y'}">
																<input class="form-check-input" type="checkbox"
																	name="preferredOrganistion" id="preferredOrganistion"
																	value="${organisation.preferredOrganisation}" checked>
															</c:when>
															<c:otherwise>
																<input class="form-check-input" type="checkbox"
																	name="preferredOrganistion" id="preferredOrganistion"
																	value="">
															</c:otherwise>
														</c:choose>
													</div>
												</div>

											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="organisationShortDescription"
														class="control-label">Organisation Short
														Description <span> *</span>
													</label>
												</div>

												<div class="col-md-6">
													<textarea class="form-control" rows="3"
														id="organisationShortDescription"
														name="organisationShortDescription"
														value="${organisation.organisationShortDescription }">${organisation.organisationShortDescription }</textarea>
												</div>

											</div>
										</div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="expressionOfInterest" class="control-label">Expression
														of Interest</label>
												</div>

												<div class="col-md-6">
													<div class="form-check">
														<input class="form-check-input" type="checkbox" value=""
															id="expressionOfInterest" name="expressionOfInterest">
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-2">
													<label for="contactName" class="control-label">Lead
														Contact </label>
												</div>
												<div class="col-md-2"></div>
												<div class="col-md-5">
													<input type="text" name="contactName" class="form-control"
														autofocus readonly="readonly" id="contactName"
														value="${organisation.contactByContactId.contactName}">
												</div>


												<div class="col-md-3">
													<a href="#" id="lookUpContact">Lookup</a>
												</div>

											</div>
										</div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-3">
													<label for="bussinessName" class="control-label">Type
														of Bussiness <span>*</span>
													</label>
												</div>
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<input type="text" name="bussinessName" id="bussinessName"
														class="form-control" autofocus readonly="readonly"
														value="${organisation.bussinessTypeByBussinessTypeId.bussinessName}">
												</div>


												<div class="col-md-3">
													<a href="#" id="lookUpBusiness">Lookup</a>
												</div>

											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="addressLine1" class="control-label">Address
														Line 1 <span>*</span>
													</label>
												</div>

												<div class="col-md-6">
													<input type="text" name="addressLine1" id="addressLine1"
														class="form-control" value="${organisation.addressLine1}"
														autofocus>
												</div>

											</div>
										</div>


										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="sicCode" class="control-label">SIC Code</label>
												</div>
												<div class="col-md-3">
													<input type="text" name="sicCode" id="sicCode"
														class="form-control" autofocus readonly="readonly"
														value="${organisation.bussinessTypeByBussinessTypeId.sicCode}">
												</div>
											</div>
										</div>
									</div>


									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="addressLine2" class="control-label">Address
														Line 2 </label>
												</div>

												<div class="col-md-6">
													<input type="text" name="addressLine2" id="addressLine2"
														class="form-control" value="${organisation.addressLine2}"
														autofocus>
												</div>
											</div>
										</div>


										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="organisationFullDescription"
														class="control-label">Organisation Full
														Description</label>
												</div>

												<div class="col-md-6">
													<textarea class="form-control" rows="3"
														id="organisationFullDescription"
														name="organisationFullDescription"
														value="${organisation.organisationFullDescription}">${organisation.organisationFullDescription }</textarea>
												</div>

											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="addressLine3" class="control-label">Address
														Line 3 </label>
												</div>

												<div class="col-md-6">
													<input type="text" name="addressLine3" id="addressLine3"
														class="form-control" value="${organisation.addressLine3}"
														autofocus>
												</div>

											</div>
										</div>


										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="phoneNumber" class="control-label">Phone
														Number <span>*</span>
													</label>
												</div>

												<div class="col-md-6">
													<input type="text" name="phoneNumber" id="phoneNumber"
														class="form-control" value="${organisation.phoneNumber}"
														autofocus>
												</div>

											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-2">
													<label for="postcode" class="control-label">Postcode
														<span> *</span>
													</label>
												</div>
												<div class="col-md-2"></div>
												<div class="col-md-5">
													<input type="text" name="postcode" id="postcode"
														class="form-control" autofocus readonly="readonly"
														value="${organisation.postcode}">
												</div>


												<div class="col-md-3">
													<a href="#" id="lookUpPostcode">Lookup</a>
												</div>

											</div>
										</div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="fax" class="control-label">Fax </label>
												</div>
												<div class="col-md-6">
													<input type="text" name="fax" id="fax" class="form-control"
														value="${organisation.fax}" autofocus>
												</div>


											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="town" class="control-label">City/Town </label>
												</div>
												<div class="col-md-6">
													<input type="text" name="town" id="town"
														class="form-control" value="${organisation.town}"
														autofocus>
												</div>


											</div>
										</div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="email" class="control-label">Email </label>
												</div>
												<div class="col-md-6">
													<input type="text" name="email" id="email"
														class="form-control" value="${organisation.email}"
														autofocus>
												</div>
											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="county" class="control-label">County </label>
												</div>
												<div class="col-md-6">
													<input type="text" name="county" id="county"
														class="form-control" value="${organisation.county}"
														autofocus>
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="webAddress" class="control-label">Web/Address
													</label>
												</div>
												<div class="col-md-6">
													<input type="text" name="webAddress" id="webAddress"
														class="form-control" value="${organisation.webAddress}"
														autofocus>
												</div>
											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">
										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="country" class="control-label">Nation/Country
													</label>
												</div>
												<div class="col-md-5">
													<div class="form-group">
														<select class="form-control" id="country">
															<c:forEach items="${countries}" var="country">
																<option value="${country.countryId}"
																	${country.countryName == organisation.country ? 'selected="selected"' : ''}>${country.countryName }</option>
															</c:forEach>
														</select>
													</div>
												</div>


											</div>
										</div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="charityNumber" class="control-label">Charity
														Number </label>
												</div>
												<div class="col-md-6">
													<input type="text" name="charityNumber" id="charityNumber"
														class="form-control" value="${organisation.charityNumber}"
														autofocus>
												</div>
											</div>
										</div>
									</div>

									<div class="row" style="padding: 10px;">
										<div class="col-lg-6"></div>

										<div class="col-lg-6">
											<div class="row">
												<div class="col-md-4">
													<label for="companyNumber" class="control-label">Company
														Number </label>
												</div>
												<div class="col-md-6">
													<input type="text" name="companyNumber" id="companyNumber"
														class="form-control" value="${organisation.companyNumber}"
														autofocus>
												</div>
											</div>
										</div>
									</div>


								</div>
								<!-- Detail 2 -->
								<div class="tab-pane fade" id="nav-detail-2" role="tabpanel"
									aria-labelledby="nav-detail-2-tab">
									<div class="form-group row">
										<label for="organisationSpecialism"
											class="col-sm-2 col-form-label">Organisation
											Specicalism</label>
										<div class="col-sm-4">
											<div class="multiSelecta">

												<c:forEach items="${organisationSpecialismes}"
													var="organisation">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oOrganisationSpecialismes}"
														var="selectedOrganisation">
															<c:if test="${selectedOrganisation eq organisation }">
																<input type="checkbox" name="organisationSpecialism"
																	value="${organisation}" checked /> ${organisation}<br />
																<c:set var = "check" value="t"></c:set>
															</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="organisationSpecialism"
																	value="${organisation}" /> ${organisation}<br />
													</c:if>
												</c:forEach>


											</div>
										</div>
										<label for="servicePersonalCircumstacesCapabilities"
											class="col-sm-2 col-form-label">Service Personal
											Circumstances Capabilities</label>
										<div class="col-sm-3">
											<div class="multiSelecta">

												<c:forEach items="${servicePersonalCircumstacesCapabilities}"
													var="servicePersonalCircumstacesCapability">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oServicePersonalCircumstacesCapabilities}"
														var="oServicePersonalCircumstacesCapability">
														<c:if test="${servicePersonalCircumstacesCapability eq oServicePersonalCircumstacesCapabilities }">
															<input type="checkbox" name="servicePersonalCircumstacesCapabilities"
																value="${servicePersonalCircumstacesCapability}" checked /> ${servicePersonalCircumstacesCapability}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="servicePersonalCircumstacesCapabilities"
															value="${servicePersonalCircumstacesCapability}" /> ${servicePersonalCircumstacesCapability}<br />
													</c:if>
												</c:forEach>

											</div>
										</div>

										<label for="serviceDisablitiesCapabilities"
											class="col-sm-2 col-form-label">Service Disabilities
											Capabilities</label>
										<div class="col-sm-4">
											<div class="multiSelecta">

												<c:forEach
													items="${serviceDisablitiesCapabilities}"
													var="serviceDisablitiesCapability">
													<c:set var="check" value="f"></c:set>
													<c:forEach
														items="${oServiceDisablitiesCapabilities}"
														var="oServiceDisablitiesCapability">
														<c:if
															test="${serviceDisablitiesCapability eq oServiceDisablitiesCapability }">
															<input type="checkbox"
																name="serviceDisablitiesCapabilities"
																value="${serviceDisablitiesCapability}" checked /> ${serviceDisablitiesCapability}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox"
															name="serviceDisablitiesCapabilities"
															value="${serviceDisablitiesCapability}" /> ${serviceDisablitiesCapability}<br />
													</c:if>
												</c:forEach>
											</div>
										</div>

										<label for="serviceEthnicity" class="col-sm-2 col-form-label">Service
											Ethnicity Capabilities</label>
										<div class="col-sm-3">
											<div class="multiSelecta">

												<c:forEach items="${serviceEthnicity}" var="serviceE">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oServiceEthnicity}"
														var="oServiceEthnicityE">
														<c:if test="${serviceE eq oServiceEthnicityE }">
															<input type="checkbox" name="serviceEthnicity"
																value="${serviceE}" checked /> ${serviceE}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="serviceEthnicity"
															value="${serviceE}" /> ${serviceE}<br />
													</c:if>
												</c:forEach>
											</div>
										</div>

										<label for="serviceBarriersCapabilities"
											class="col-sm-2 col-form-label">Service Barriers
											Capabilities</label>
										<div class="col-sm-4">
											<div class="multiSelecta">

												<c:forEach items="${serviceBarriersCapabilities}"
													var="serviceBarriersCapability">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oServiceBarriersCapabilities}"
														var="oServiceBarriersCapability">
														<c:if
															test="${serviceBarriersCapability eq oServiceBarriersCapability }">
															<input type="checkbox" name="serviceBarriersCapabilities"
																value="${serviceBarriersCapability}" checked /> ${serviceBarriersCapability}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="serviceBarriersCapabilities"
															value="${serviceBarriersCapability}" /> ${serviceBarriersCapability}<br />
													</c:if>
												</c:forEach>
											</div>
										</div>

										<label for="accreditation" class="col-sm-2 col-form-label">Accreditation</label>
										<div class="col-sm-4">
											<div class="multiSelecta">

												<c:forEach items="${accreditation}" var="accreditate">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oAccreditation}" var="oAccre">
														<c:if test="${accreditate eq oAccre }">
															<input type="checkbox" name="accreditation"
																value="${accreditate}" checked /> ${accreditate}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="accreditation"
															value="${accreditate}" /> ${accreditate}<br />
													</c:if>
												</c:forEach>
											</div>
										</div>

										<label for="serviceBenefitsCapabilities"
											class="col-sm-2 col-form-label">Service Benifits
											Capabilities</label>
										<div class="col-sm-4">
											<div class="multiSelecta">

												<c:forEach items="${serviceBenefitsCapabilities}"
													var="serviceBenefitsCapability">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oServiceBenefitsCapabilities}"
														var="oServiceBenefitsCapability">
														<c:if
															test="${serviceBenefitsCapability eq oServiceBenefitsCapability }">
															<input type="checkbox" name="serviceBenefitsCapabilities"
																value="${serviceBenefitsCapability}" checked /> ${serviceBenefitsCapability}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="serviceBenefitsCapabilities"
															value="${serviceBenefitsCapability}" /> ${serviceBenefitsCapability}<br />
													</c:if>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>
								<!-- Detail 3 -->
								<div class="tab-pane fade" id="nav-detail-3" role="tabpanel"
									aria-labelledby="nav-detail-3-tab">
									<div class="form-group row">
										<label for="eoiProgrammes" class="col-sm-2 col-form-label">EOI
											Programmes</label>
										<div class="col-sm-4">
											<div class="multiSelecta">

												<c:forEach items="${eoiProgrammes}" var="eoiProgramm">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oEoiProgrammes}" var="oEoiProgramm">
														<c:if test="${eoiProgramm eq oEoiProgramm }">
															<input type="checkbox" name="eoiProgrammes"
																value="${eoiProgramm}" checked /> ${eoiProgramm}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="eoiProgrammes"
															value="${eoiProgramm}" /> ${eoiProgramm}<br />
													</c:if>
												</c:forEach>

											</div>
										</div>
										<div class="col-sm-6"></div>
										<label for="eoiServices" class="col-sm-2 col-form-label">EOI
											Services</label>
										<div class="col-sm-4">
											<div class="multiSelecta">
												<c:forEach items="${eoiServices}" var="eoiService">
													<c:set var="check" value="f"></c:set>
													<c:forEach items="${oEoiServices}" var="oEoiService">
														<c:if test="${eoiService eq oEoiService }">
															<input type="checkbox" name="eoiServices"
																value="${eoiService}" checked /> ${eoiService}<br />
															<c:set var="check" value="t"></c:set>
														</c:if>
													</c:forEach>
													<c:if test="${check == 'f'}">
														<input type="checkbox" name="eoiServices"
															value="${eoiService}" /> ${eoiService}<br />
													</c:if>
												</c:forEach>
											</div>
										</div>
									</div>
								</div>

								<!-- Detail 4 -->
								<div class="tab-pane fade" id="nav-detail-4" role="tabpanel"
									aria-labelledby="nav-detail-4-tab">
									<div class="form-group row">
										<div class="col-sm-12" style="margin-bottom: 3px;">
											<fieldset class="border p-2 fieldSet">
												<legend class="w-auto"> Premise</legend>
												<div id="asd">
													<table class="table table-bordered table-striped"
														id="colorRowsTable" style="background-color: white;">
														<thead>
															<tr style="background-color: #999999;">
																<th scope="col">Premise Name</th>
																<th scope="col">Address</th>
																<th scope="col">Primary Location</th>
																<th scope="col">Phone Number</th>
															</tr>
														</thead>
															<c:choose>
																<c:when test="${fn:length(premises) gt 0}">
																<tbody>
																	<c:forEach items="${premises}" var="premise">
																		<tr>
																			<td>${premise.premiseName}</td>
																			<td>${premise.address1}</td>
																			<td>${premise.locationName}</td>
																			<td>${premise.phoneNumber}</td>
																		</tr>
																	</c:forEach>
																	</tbody>
																</c:when>
																<c:otherwise>
																	<tbody style="visibility: hidden;">
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>

																		</tr>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>

																		</tr>
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																	</tbody>
																</c:otherwise>
															</c:choose>
														</tbody>
													</table>
												</div>
											</fieldset>
										</div>

										<div class="col-sm-12">
											<fieldset class="border p-2 fieldSet">
												<legend class="w-auto"> Located In</legend>

												<div class="form-group row">
													<label for="organisationName"
														class="col-sm-2 col-form-label">Ward: </label>
													<div class="col-sm-4">
														<input type="text" class="form-control" id="ward"
															name="ward" value="Charles Kelley" readonly="readonly">
													</div>
													<label for="preferredOrganistion"
														class="col-sm-2 col-form-label">NHS Authority</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" id="nhsAuthority"
															name="nhsAuthority" value="Stella House Goldcrest" readonly="readonly">
													</div>
												</div>

												<div class="form-group row">
													<label for="organisationName"
														class="col-sm-2 col-form-label">Borough: </label>
													<div class="col-sm-4">
														<input type="text" class="form-control" id="borough"
															name="borough" value="Middlesbrough" readonly="readonly">
													</div>
													<label for="govOfficeRegionName"
														class="col-sm-2 col-form-label">Gov't Office
														Region</label>
													<div class="col-sm-3">
														<div class="form-group">
															<select class="form-control" id="govOfficeRegionName">
																<c:forEach items="${govOfficeRegionNames}"
																	var="govOfficeRegionName">
																	<option value="${govOfficeRegionName}"
																		${govOfficeRegionName.govOfficeRegionName == organisation.govOfficeRegionName ? 'selected="selected"' : ''}>${govOfficeRegionName.govOfficeRegionName}</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="localAuthority" class="col-sm-2 col-form-label">Local
														Authority: </label>
													<div class="col-sm-4">
														<input type="text" class="form-control"
															id="localAuthority" name="localAuthority" value="Washington DC"
															readonly="readonly">
													</div>
													<label for="trustRegion" class="col-sm-2 col-form-label">Trust
														Region</label>
													<div class="col-sm-3">
														<div class="form-group">
															<select class="form-control" id="trustRegionName">
																<c:forEach items="${trustRegionNames}"
																	var="trustRegionName">
																	<option value="${trustRegionName}"
																		${trustRegionName == organisation.trustRegionName ? 'selected="selected"' : ''}>${trustRegionName}</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label for="unitaryAuthority"
														class="col-sm-2 col-form-label">Unitary Authority:
													</label>
													<div class="col-sm-4">
														<input type="text" class="form-control"
															id="unitaryAuthority" name="unitaryAuthority" value="Marlborough District Council"
															readonly="readonly">
													</div>
													<label for="trustDistrict" class="col-sm-2 col-form-label">Trust
														District</label>
													<div class="col-sm-3">
														<div class="form-group">
															<select class="form-control" id="trustDistrictName">
																<c:forEach items="${trustDistrictNames}"
																	var="trustDistrictName">
																	<option value="${trustDistrictName}"
																		${trustDistrictName == organisation.trustDistrictName ? 'selected="selected"' : ''}>${trustDistrictName}</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>

											</fieldset>
										</div>

									</div>
								</div>

								<!-- Detail 5 -->
								<div class="tab-pane fade" id="nav-detail-5" role="tabpanel"
									aria-labelledby="nav-detail-5-tab">
									<div class="form-group row">
										<div class="col-sm-12" style="margin-bottom: 3px;">
												<div id="dContentMaterial">
		<div class="borders">
			<b>Current List of Supporting Material</b> <br>
			<div style="padding: 5px; margin-right: 0px;"
				class="row float-right d-flex align-items-center">
				<button type="button" class="btn btn-outline-primary" id="btnCreateMaterial">Create</button>
				&nbsp; &nbsp; <input type="checkbox" name="vehicle1" value="Bike"
					class=""> &nbsp;Include in-active<br>
			</div>
			<br> <br>
			<div id="tableDisplay">
				<table class="table table-bordered table-striped"
					id="colorRowsMaterialTable" style="font-size: 14px;">
					<thead>
						<tr>
							<th scope="col" hidden><input type="checkbox" id="meterialIds"></th>
							<th scope="col">URL</th>
							<th scope="col">Description</th>
							<th scope="col">Type</th>
							<th scope="col">Added By</th>
							<th scope="col">Added Date</th>

						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach items="${listsupportingmaterial}"
								var="supportingmaterial">
								<tr>
								<th scope="row" hidden><input type="checkbox" name="idMaterial"
						value="${supportingmaterial.supportingMaterialId}"></th>
									<td><a class="supportingmaterialName"
										value="${supportingmaterial.active},${supportingmaterial.supportingMaterialId}"
										href="#">${supportingmaterial.url}</a></td>
									<td>${supportingmaterial.description}</td>
									<td>${supportingmaterial.type}</td>
									<td>${supportingmaterial.userByUserId.account}</td>
									<td>
										<%-- <c:set var="date" value="">${supportingmaterial.addDate}</c:set>
									<fmt:formatDate pattern = "yyyy-MM-dd" value = "${date}"/> --%>
									</td>
								</tr>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<!-- 	 <script type="text/javascript">
		$(document).ready(function() {
			var active = "Yes";
			var table = $('#colorRowsMaterialTable').DataTable({
				retrieve : true,
				info : false,
				"searchCols" : [ null, null, null, null, {
					"search" : active
				} ],
				"dom" : 'rtip',
				"pageLength" : 15,
				stateSave : true,

				"drawCallback" : function(settings) {
					var api = new $.fn.dataTable.Api(settings);
					modifyIfEmpty(api);
				}
			});
			table.column(4).search(active).draw();
			$(document).ready(function() {
				$('#all').trigger('click');
			});

			//In clude in-active
			$('#include-inactive').click(function() {

				table.state.save();
				var state = table.state();
				table.search(state.search.search).draw();
				if ($('#include-inactive').prop("checked") == true) {
					active = "";
				} else {
					active = "Yes";
				}
				table.column(4).search(active).draw();
			})

			//Filter
			$('#all').click(function() {
				table.search("").draw();
			})
			$('#ABCDE').click(function() {
				table.search("^[ABCDE]", true, false).draw();
			})
			$('#09').click(function() {
				table.search("^[0-9]", true, false).draw();
			})
			$('#FGHIJ').click(function() {
				table.search("^[FGHIJ]", true, false).draw();
			})
			$('#KLMN').click(function() {
				table.search("^[KLMN]", true, false).draw();
			})
			$('#OPQR').click(function() {
				table.search("^[OPQR]", true, false).draw();
			})
			$('#STUV').click(function() {
				table.search("^[STUV]", true, false).draw();
			})
			$('#WXYZ').click(function() {
				table.search("^[WXYZ]", true, false).draw();
			})

			function modifyIfEmpty(api) {

				//If no record.
				if (api.page.info().recordsDisplay == 0) {
					$('.dataTables_empty').html("");
					$('#colorRowsTable_paginate,.hide-if-empty').hide();

				} else {
					$('#colorRowsTable_paginate,.hide-if-empty').show();
				}
			}
			;

		});
	</script>  -->
										</div>

									</div>
								</div>

								<!-- BU/Directorates -->
								<div class="tab-pane fade" id="nav-detail-6" role="tabpanel"
									aria-labelledby="nav-detail-6-tab"></div>

							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="errorMessages">
				<p id="errorOrganisationName"></p>
				<p id="errorPhoneNumber"></p>
				<p id="errorAddressLine1"></p>
				<p id="errorShortDescription"></p>
				<p id="errorTypeOfBusiness"></p>
				<p id="errorPostcode"></p>
				<p id="errorCreate"></p>
			</div>
			<div id="successMessage">
				<p id="success"></p>
			</div>
		</div>
	</div>
	<span id ="csrf" hidden="">${_csrf.token}</span>
		<script
		src="<%=request.getContextPath()%>/resources/js/actions/create-organisation.js"></script>
					<script
		src="<%=request.getContextPath()%>/resources/js/supportingmaterial/supportingmaterial.js"></script>
</body>
</html>