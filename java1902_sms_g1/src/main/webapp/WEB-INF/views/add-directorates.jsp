<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/list-organisations.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/add-organisation.css"
	rel="stylesheet">
</head>
<body>
	<div id="dContent">
		<div class="borders">
			<a href="#">Organisation</a> >
			<h5 style="color: #00cc00;">Organisation Details</h5>
			<div id="containerOrganisation">
				<div id="sortListOrganisations">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-detail-1-tab"
								data-toggle="tab" href="#nav-detail-1" role="tab"
								aria-controls="nav-detail-1" aria-selected="true">Details</a>
						</div>
					</nav>
				</div>
				<div id="actions">
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnBack">Back</button>
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnSave">Save</button>
						<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="inActive">In-Active</button>
						<script>
								
						
						
						</script>
				</div>
			</div>
			<div id="insideborder">
				<div id="formDisplay" style="margin-top: 4px;">
				<div id="directorateId" hidden="" value="${directorate.directorateId}">${directorate.directorateId}</div>
					<div class="r" style="width: 100%; padding: 5px;">
						<form style="font-size: 14px;" action="<%=request.getContextPath()%>/adddirectorates" method="post">
							<div class="tab-content" id="nav-tabContent">

								<!-- Detail 1 -->
								
								<div class="tab-pane fade show active" id="nav-detail-1"
									role="tabpanel" aria-labelledby="nav-detail-1-tab">
									<div class="form-group row">
										<label for="organisationName" class="col-sm-2 col-form-label">BU/Directorate
											Name <span>*</span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="directorateName"
												name="directorateName" value="${directorate.directorateName}">
										</div>
										<label for="typeOfBusiness" class="col-sm-2 col-form-label">Type
											of Business <span>*</span>
										</label>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" id="typeOfBusiness"
													name="typeOfBusiness" readonly="readonly" value="${directorate.bussinessTypeByBussinessTypeId.bussinessTypeId}">
											</div>
										</div>
										<div class="col-sm-1">
											<a href="#" class="typeOfBusiness">Lookup</a>
										</div>

									</div>

									<div class="form-group row">
										<label for="organisationShortDescription"
											class="col-sm-2 col-form-label">BU/Directorate Name
											Short <br> Description
										</label>
										<div class="col-sm-4">
											<div class="form-group">
												<textarea class="form-control" rows="3"
													id="directorateNameShortDescription" name="directorateNameShortDescription">${directorate.directorateShortDescription}</textarea>
											</div>
										</div>
										<label for="expressionOfInterest"
											class="col-sm-2 col-form-label">SIC Code</label>
										<div class="col-sm-1">
											<div class="form-group">
												<input type="text" class="form-control" id="SICCode"
													name="SICCode" readonly value="${directorate.bussinessTypeByBussinessTypeId.sicCode}">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label for="leadContact" class="col-sm-2 col-form-label">Lead
											Contact </label>
										<div class="col-sm-3">
											<div class="form-group" id="leadcontact">
												<input type="text" class="form-control" id="leadContact"
													name="leadContact" readonly="readonly" value=${directorate.contactByContactId.contactId }>
											</div>
										</div>
										<div class="col-sm-1">
											<a href="#" id="leadContacts">Lookup</a>
										</div>
										<label for="organisationShortDescription"
											class="col-sm-2 col-form-label">BU/Directorate Name
											Full <br> Description
										</label>
										<div class="col-sm-3">
											<div class="form-group">
												<textarea class="form-control" rows="3"
													id="directorateNameFullDescription"
													name="directorateNameFullDescription" >${directorate.directorateFullDescription}</textarea>
											</div>
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-6">
											<input type="checkbox" name="vehicle1" value="Bike" class="">
											&nbsp; Copy Address from Organisation<br>
										</div>
										<label for="loginname"
											class="col-md-2 col-form-label text-md-left">Phone
											number</label>
										<div class="col-md-3">
											<input type="text" id="phonenumber" class="form-control"
												name="phonenumber" value="${directorate.phoneNumber}">
										</div>
									</div>

									<div class="form-group row">
										<label for="addressLine1" class="col-sm-2 col-form-label">Address
											Line 1 <span>*</span>
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="addressLine1"
												name="addressLine1" value="${directorate.addressLine1}">
										</div>
										<label for="fax" class="col-sm-2 col-form-label">Fax </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="fax" name="fax" value="${directorate.fax}">
										</div>
									</div>

									<div class="form-group row">
										<label for="addressLine2" class="col-sm-2 col-form-label">Address
											Line 2 </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="addressLine2"
												name="addressLine2" value="${directorate.addressLine2}">
										</div>
										<label for="fax" class="col-sm-2 col-form-label">Email
										</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="email"
												name="email" value="${directorate.email}">
										</div>
									</div>

									<div class="form-group row">
										<label for="addressLine3" class="col-sm-2 col-form-label">Address
											Line 3 </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="addressLine3"
												name="addressLine3" value="${directorate.addressLine3}">
										</div>
										<label for="phoneNumber" class="col-sm-2 col-form-label">Web
											Address </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="webAddress"
												name="webAddress" value="${directorate.webAddress}">
										</div>
									</div>

									<div class="form-group row">
										<label for="postCode" class="col-sm-2 col-form-label">Postcode
											<span> *</span>
										</label>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" id="postCode"
													name="postCode" readonly="readonly" value="${directorate.postcode}">
											</div>
										</div>
										<div class="col-sm-1">
											<a href="#" class="postcode">Lookup</a>
										</div>
										<label for="fax" class="col-sm-2 col-form-label">Charity
											Number </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="charityNumber"
												name="charityNumber" value="${directorate.charityNumber}">
										</div>
									</div>

									<div class="form-group row">
										<label for="city" class="col-sm-2 col-form-label">Town/Village/City</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="city" name="city" value="${directorate.town}">
										</div>
										<label for="email" class="col-sm-2 col-form-label">Company
											Number </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="companyNumber"
												name="companyNumber" value="${directorate.companyNumber}">
										</div>
									</div>

									<div class="form-group row">
										<label for="county" class="col-sm-2 col-form-label">County</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="county"
												name="county" value="${directorate.county}">
										</div>
									</div>

									<div class="form-group row">
										<label for="nation" class="col-sm-2 col-form-label">Nation/County</label>
										<div class="col-sm-3">
											<div class="form-group">
												<select class="form-control" id="nation" value="${directorate.country}">
													<option>Vietnam</option>
													<option>Thailand</option>
													<option>Malaysia</option>
													<option>Korea</option>
													<option>Japan</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- Detail 2 -->

							</div>
						</form>
					</div>
				</div>
			</div>
			<div style="color: green" id="message">
				<p>${message}</p>
			</div>
		</div>
	</div>
	<span id=context hidden=""><%=request.getContextPath()%></span>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/Jquery/jquery.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/directorate/add-directorate.js"></script>
</body>
</html>