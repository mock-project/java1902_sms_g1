<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/add-organisation.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/list-organisations.css"
	rel="stylesheet">
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />
</head>
<body>
	<div id="dContent">
		<div class="borders">
			<a href="#">Organisation</a> >
			<h5 style="color: #00cc00;">Supporting Materials Details</h5>
			<div id="containerOrganisation">
				<div id="sortListOrganisations">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-detail-1-tab"
								data-toggle="tab" href="#nav-detail-1" role="tab"
								aria-controls="nav-detail-1" aria-selected="true">Details</a>
						</div>
					</nav>
				</div>
				<div id="actions">
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnBackToMaterialScreen">Back</button>
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnSaveMaterial">Save</button>
				</div>
			</div>
			<div id="formDisplay" class="borders" style="margin-top: 4px;">
				<div class="r" style="width: 100%; padding: 5px;">
					<form>
						<!-- Detail 1 -->
						<div class="tab-pane fade show active" id="nav-detail-1"
							role="tabpanel" aria-labelledby="nav-detail-1-tab">
							<div class="form-group row">
								<input type="hidden" id="supportingMaterialId"
									name="supportingMaterialId"
									value="${supportingMaterial.supportingMaterialId }"> <label
									for="organisationName" class="col-sm-2 col-form-label">URL<span>*</span>
								</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="url" name="url">
								</div>
								<label for="organisationName" class="col-sm-2 col-form-label">Added
									By</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="userByUserId"
										name="userByUserId" readonly value="hienld7">
								</div>
							</div>

							<div class="form-group row">
								<label for="organisationShortDescription"
									class="col-sm-2 col-form-label">Description</label>
								<div class="col-sm-4">
									<div class="form-group">
										<textarea class="form-control" rows="3" id="description"
											name="description">${supportingMaterial.description}</textarea>
									</div>
								</div>
								<label for="organisationName" class="col-sm-2 col-form-label">Added
									Date</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" id="addDate"
										name="addDate" value="${supportingMaterial.addDate}" readonly>
								</div>
							</div>
							<div class="form-group row">
								<label for="organisationShortDescription"
									class="col-sm-2 col-form-label">Type</label>
								<div class="col-sm-4">
									<div class="form-group">
										<select id="type" class="form-control">
											<option>Doc</option>
											<option>Pdf</option>
											<option>Excel</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div id="successAddMaterial"></div>
			<div id="errorCreateMaterial"></div>
		</div>
	</div>
	<span id=context hidden=""><%=request.getContextPath()%></span>
	<span id=_csrf hidden="">${_csrf.token}</span>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/supportingmaterial/supportingmaterial.js"></script>
</body>
</html>