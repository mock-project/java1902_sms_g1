<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/resources/css/bootstrap/dataTables.bootstrap4.min.css">
</head>
<body>
	<div id="dContent">
		<div class="borders">
			<h5 style="color: #00cc00;">Organisation List</h5>

			<div class="d-flex p-2">
				<div id="sortListOrganisations">
					<a href="#" class="badge badge-primary" id="all">All</a> | <a
						href="#" class="badge badge-primary" id="09">0 - 9</a> | <a
						href="#" class="badge badge-primary" id="ABCDE">A B C D E</a> | <a
						href="#" class="badge badge-primary" id="FGHIJ">F G H I J</a> | <a
						href="#" class="badge badge-primary" id="KLMN">K L M N</a> | <a
						href="#" class="badge badge-primary" id="OPQR">O P Q R</a> | <a
						href="#" class="badge badge-primary" id="STUV">S T U V</a> | <a
						href="#" class="badge badge-primary" id="WXYZ">W X Y Z</a>
				</div>
				<div id="actionsMenu">
					<div class="d-inline-flex p-2" style="float: right;">
						<button type="button" class="btn btn-outline-primary"
							style="margin-top: -15px; margin-right: 7px; float: right;"
							id="btnCreate">Create</button>
						<div class="form-check">
							<input type="checkbox" class="form-check-input hide-if-empty"
								id="include-inactive"> <label class="form-check-label"
								for="exampleCheck1">Include In-active</label>
						</div>
					</div>
				</div>
			</div>

			<div id="tableDisplay">
				<table class="table table-bordered table-striped"
					id="colorRowsTable">
					<thead>
						<tr>
							<th scope="col">Organisation Name</th>
							<th scope="col">Head Office Address Line 1</th>
							<th scope="col">Postcode</th>
							<th scope="col">Contact</th>
							<th scope="col">Is Active?</th>
						</tr>
					</thead>
					<tbody>



						<c:forEach items="${organisations}" var="organisation">
							<tr>
								<td><a href="#" class="changeActive"
									value="${organisation.active},${organisation.organisationId}">${organisation.organisationName}</a></td>
								<td>${organisation.addressLine1}</td>
								<td>${organisation.postcode}</td>
								<td>${organisation.contactByContactId.contactName}</td>
								<c:choose>
									<c:when test="${organisation.active == 'T'}">
										<td>Yes</td>
									</c:when>
									<c:otherwise>
										<td>No</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>
		</div>
	</div>
	<script
		src="<%=request.getContextPath()%>/resources/js/actions/list-organisations.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var active = "Yes";
			var table = $('#colorRowsTable').DataTable({
				retrieve : true,
				info : false,
				"searchCols" : [ null, null, null, null, {
					"search" : active
				} ],
				"dom" : 'rtip',
				"pageLength" : 15,
				stateSave : true,

				"drawCallback" : function(settings) {
					var api = new $.fn.dataTable.Api(settings);
					modifyIfEmpty(api);
				}
			});

			$(document).ready(function() {
				$('#all').trigger('click');
			});

			//In clude in-active
			$('#include-inactive').click(function() {
				table.state.save();
				var state = table.state();
				table.search(state.search.search).draw();
				if ($('#include-inactive').prop("checked") == true) {
					active = "";
				} else {
					active = "Yes";

				}
				table.column(4).search(active).draw();
			})

			//Filter
			$('#all').click(function() {
				table.search("").draw();
			})
			$('#ABCDE').click(function() {
				table.search("^[ABCDE]", true, false).draw();
			})
			$('#09').click(function() {
				table.search("^[0-9]", true, false).draw();
			})
			$('#FGHIJ').click(function() {
				table.search("^[FGHIJ]", true, false).draw();
			})
			$('#KLMN').click(function() {
				table.search("^[KLMN]", true, false).draw();
			})
			$('#OPQR').click(function() {
				table.search("^[OPQR]", true, false).draw();
			})
			$('#STUV').click(function() {
				table.search("^[STUV]", true, false).draw();
			})
			$('#WXYZ').click(function() {
				table.search("^[WXYZ]", true, false).draw();
			})

			function modifyIfEmpty(api) {

				//If no record.
				if (api.page.info().recordsDisplay == 0) {
					$('.dataTables_empty').html("");
					$('#colorRowsTable_paginate,.hide-if-empty').hide();

				} else {
					$('#colorRowsTable_paginate,.hide-if-empty').show();
				}
			}
			;
		});
	</script>

</body>
</html>