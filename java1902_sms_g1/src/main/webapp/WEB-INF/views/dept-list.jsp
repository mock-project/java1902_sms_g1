<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}"></c:set>
<html>
<head>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Popper -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>


<link
	href="${context}/resources/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath() %>/resources/css/list-departments.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath() %>/resources/css/add-department.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

</head>
<body>
	<div hidden="" id="contextPath"><%=request.getContextPath()%></div>
    <input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" id="token"/>
	<div id="dContent" style="background-color: #f2f2f2;">
		<div class="borders">
			<!-- start -->
			<a href="#" style="text-decoration: underline;">Organizations</a>>
			<h5 style="color: #00cc00;">Business Unit/Directorate Details</h5>
			<div id="containerDepartment">
				<div id="sortListDepartment">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link " id="nav-detail-1-tab"
								data-toggle="tab" href="#nav-detail-1" role="tab"
								aria-controls="nav-detail-1" aria-selected="true">Detail</a> <a
								class="nav-item nav-link active" id="nav-detail-2-tab"
								data-toggle="tab" href="#nav-detail-2" role="tab"
								aria-controls="nav-detail-2" aria-selected="false">Department</a>

						</div>
					</nav>
				</div>

				<div id="actions">
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnSave">Back</button>
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnSave">Save</button>
					<button type="button" class="btn btn-outline-primary"
						style="float: right;" id="btnSave">In-Active</button>
				</div>

			</div>
			<!-- end -->
			<div id="insideborder">

				<div style="padding: 10px; margin-right: 0px;"
					class="row float-right d-flex align-items-center">
					<button type="button" class="btn btn-primary " id="create-dept">Create</button>
					&nbsp; &nbsp; <input type="checkbox" name="vehicle1" value="Bike"
						class="hide-if-empty" id="include-inactive"> <span
						class="hide-if-empty">&nbsp;Include in-Yes</span><br>
				</div>
				<br>
				<div class="d-flex p-2">
					<div id="sortListDepartment">
						<a href="#" class="badge badge-primary" id="all">All</a> | <a
							href="#" class="badge badge-primary" id="09">0 - 9</a> | <a
							href="#" class="badge badge-primary" id="ABCDE">A B C D E</a> | <a
							href="#" class="badge badge-primary" id="FGHIJ">F G H I J</a> | <a
							href="#" class="badge badge-primary">K L M N</a> | <a href="#"
							class="badge badge-primary">O P Q R</a> | <a href="#"
							class="badge badge-primary">S T U V</a> | <a href="#"
							class="badge badge-primary">W X Y Z</a>
					</div>
				</div>
				<div id="tableDisplay">
					<table class="table table-bordered table-striped"
						id="colorRowsTable">
						<thead>
							<tr>
								<th hidden="">Id</th>
								<th scope="col">Department Name</th>
								<th scope="col">Address Line 1</th>
								<th scope="col">Postcode</th>
								<th scope="col">Contact</th>
								<th scope="col">Is Active</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${LIST_DEPT}" var="dept">
								<tr>
									<td hidden="">${dept.departmentId}</td>
									<td class="amendDept"><a href="#">${dept.departmentName}</a></td>
									<td>${dept.addressLine1}</td>
									<td>${dept.postcode}</td>
									<td>${dept.contactByContactId.contactName}</td>
									<td>${dept.active}</td>

								</tr>

							</c:forEach>

						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function(){
        var active = "Yes";
        var table = $('#colorRowsTable').DataTable({
            info: false,
            "searchCols": [
               null,null,null,null,null,{"search":active}
            ],
            "dom": 'rtip',
            "pageLength": 3,
             stateSave: true,
            
            "drawCallback": function( settings ) {
                 var api = new $.fn.dataTable.Api( settings );
                 modifyIfEmpty(api);
            }
        });

        //In clude in-active
        $('#include-inactive').click(function(){
            table.state.save();
            var state = table.state();
            table.search(state.search.search).draw();
            if($('#include-inactive').prop("checked")==true){
                active = "";
            }
            else{
                active = "Yes";
            }
            table.column(5).search(active).draw();
            
        })


        //Filter
        $('#all').click(function(){
            table.search("").draw();
        })
        $('#ABCDE').click(function(){
            table.search("^[ABCDE]",true,false).draw();
        })
        $('#09').click(function(){
            table.search("^[0]",true,false).draw();
        })
        $('#FGHIJ').click(function(){
            table.search("^[FGHJ]",true,false).draw();
        })


        //remove

        //Change if empty

        function modifyIfEmpty(api){

      /*       //If no record.
            if(api.page.info().recordsDisplay==0){


                $('#colorRowsTable_paginate,.hide-if-empty').hide();

            }
            //if there some records.
            else{
                $('#colorRowsTable_paginate,.hide-if-empty').show();
            }    */
        }


    })

    
</script>
	<script src="<%=request.getContextPath()%>/resources/js/dept.js"></script>

</body>
</html>