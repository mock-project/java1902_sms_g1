<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}"></c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/list-organisations.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
</head>
<body>
	<div id="dContent">
		<div class="borders">
			<h5 style="color: #00cc00;">Organisation Details</h5>
			<br>
			<div style="padding: 5px;"
				class="row float-right d-flex align-items-center">
				<button type="button" class="btn btn-outline-primary" id="">In-Active</button>
				&nbsp; &nbsp;
				<button type="button" class="btn btn-outline-primary">Save</button>
				&nbsp; &nbsp;
				<button type="button" class="btn btn-outline-primary">Back</button>
				&nbsp; &nbsp;
			</div>

			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#home"><b>BU/Directorates</b></a></li>
			</ul>
			<br>
			<div id="insideborder">

				<div style="padding: 10px; margin-right: 0px;"
					class="row float-right d-flex align-items-center">
					<button type="button" class="btn btn-outline-primary " id="create">Create</button>
					&nbsp; &nbsp; <input type="checkbox" name="vehicle1" value="Bike"
						class="hide-if-empty" id="include-inactive"> <span class="hide-if-empty">&nbsp;Include
						in-active</span><br>
				</div>
				<br>
				<div class="d-flex p-2">
					<div id="sortListOrganisations">
						<a href="#" class="badge badge-primary" id="all">All</a> | <a
							href="#" class="badge badge-primary" id="09">0 - 9</a> | <a
							href="#" class="badge badge-primary" id="ABCDE">A B C D E</a> | <a
							href="#" class="badge badge-primary" id="FGHIJ">F G H I J</a> | <a
							href="#" class="badge badge-primary" id="KLMN">K L M N</a> | <a href="#"
							class="badge badge-primary" id="OPQR">O P Q R</a> | <a href="#"
							class="badge badge-primary" id="STUV">S T U V</a> | <a href="#"
							class="badge badge-primary" id="WXYZ">W X Y Z</a>
					</div>
				</div>
				<br>
				<div id="tableDisplay">
					<table class="table table-bordered table-striped"
						id="colorRowsTable" style="font-size: 14px;">
						<thead>
							<tr>
								<th scope="col">BU/Directorate Name</th>
								<th scope="col">Office Address Line 1</th>
								<th scope="col">Postcode</th>
								<th scope="col">Contact</th>
								<th scope="col">Is Active</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listDirectorate}" var="directorate">
								<tr>
									<td><a class = "directorateName" value="${directorate.active},${directorate.directorateId}" href="#">${directorate.directorateName}</a></td>
									<td>${directorate.addressLine1}</td>
									<td>${directorate.postcode}</td>
									<td>${directorate.contactByContactId.contactName}</td>
									<td>${directorate.active}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
	
	<span id=context hidden=""><%=request.getContextPath()%></span>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/Jquery/jquery.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/list-organisations.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/directorate/add-directorate.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/directorate/list-directorate.js"></script>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			var active = "Yes";
			var table = $('#colorRowsTable').DataTable({
				retrieve : true,
				info : false,
				"searchCols" : [ null, null, null,null, {
					"search" : active
				} ],
				"dom" : 'rtip',
				"pageLength" : 15,
				stateSave : true,

				"drawCallback" : function(settings) {
					var api = new $.fn.dataTable.Api(settings);
					modifyIfEmpty(api);
				}
			});
			table.column(4).search(active).draw();
			$(document).ready(function() {
				$('#all').trigger('click');
			});

			//In clude in-active
			$('#include-inactive').click(function() {
				
				table.state.save();
				var state = table.state();
				table.search(state.search.search).draw();
				if ($('#include-inactive').prop("checked") == true) {
					active = "";
				} else {
					active = "Yes";
				}
				table.column(4).search(active).draw();
			})

			//Filter
			$('#all').click(function() {
				table.search("").draw();
			})
			$('#ABCDE').click(function() {
				table.search("^[ABCDE]", true, false).draw();
			})
			$('#09').click(function() {
				table.search("^[0-9]", true, false).draw();
			})
			$('#FGHIJ').click(function() {
				table.search("^[FGHIJ]", true, false).draw();
			})
			$('#KLMN').click(function() {
				table.search("^[KLMN]", true, false).draw();
			})
			$('#OPQR').click(function() {
				table.search("^[OPQR]", true, false).draw();
			})
			$('#STUV').click(function() {
				table.search("^[STUV]", true, false).draw();
			})
			$('#WXYZ').click(function() {
				table.search("^[WXYZ]", true, false).draw();
			})

			function modifyIfEmpty(api) {

				//If no record.
				if (api.page.info().recordsDisplay == 0) {
					$('.dataTables_empty').html("");
					$('#colorRowsTable_paginate,.hide-if-empty').hide();

				} else {
					$('#colorRowsTable_paginate,.hide-if-empty').show();
				}
			}
			;

		});
	</script>


</body>
</html>