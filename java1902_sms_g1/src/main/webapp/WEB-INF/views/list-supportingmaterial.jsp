<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link href="<%=request.getContextPath()%>/resources/css/bootstrap/bootstrap.min.css"
	rel="stylesheet">
	<link href="<%=request.getContextPath()%>/resources/css/list-organisations.css"
	rel="stylesheet">
</head>
<body>
	<div id="dContent">
		<div class="borders">
			<h5 style="color: #00cc00;">Organisation Details</h5>
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#home"><b>Details 5</b></a>
				</li>
			</ul>
			<b>Current List of Supporting Material</b>
			<br>
			<div style="padding: 10px; margin-right: 0px;" class="row float-right d-flex align-items-center">
				<button type="button" class="btn btn-outline-primary">Create</button> &nbsp; &nbsp;
				<input type="checkbox" name="vehicle1" value="Bike" class=""> &nbsp;Include in-active<br>
			</div>
			<br>
			<div class="d-flex p-2">
				<div id="sortListOrganisations">
					<a href="#" class="badge badge-primary">All</a> | <a href="#"
					class="badge badge-primary">0 - 9</a> | <a href="#"
					class="badge badge-primary">A B C D E</a> | <a href="#"
					class="badge badge-primary">F G H I J</a> | <a href="#"
					class="badge badge-primary">K L M N</a> | <a href="#"
					class="badge badge-primary">O P Q R</a> | <a href="#"
					class="badge badge-primary">S T U V</a> | <a href="#"
					class="badge badge-primary">W X Y Z</a>
				</div>
				<br>
			</div>
			<div id="tableDisplay">
				<table class="table table-bordered table-striped"
				id="colorRowsTable" style="font-size: 14px;">
				<thead>
					<tr>
						<th scope="col">URL</th>
						<th scope="col">Description</th>
						<th scope="col">Type</th>
						<th scope="col">Added By</th>
						<th scope="col">Added Date</th>
						<th scope="col" ></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
					<tr>
						<td><a href="" title="">13123221.com</a></td>
						<td>Google Corp</td>
						<td>Doc</td>
						<td>Van Vu</td>
						<td>10/26/2019</td>
						<td><input type="button" name="" value="" hidden=""></td>
					</tr>
				</tbody>
			</table>

			<ul class="pagination justify-content-end">
				<li class="page-item"><a class="page-link" href="#">Previous</a></li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item active"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#">Next</a></li>
			</ul>
		</div>
	</div>
</div>
<script
src="<%=request.getContextPath()%>/resources/js/list-organisations.js"></script>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/Jquery/jquery.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/supportingmaterial/supportingmaterial.js"></script>
</body>
</html>