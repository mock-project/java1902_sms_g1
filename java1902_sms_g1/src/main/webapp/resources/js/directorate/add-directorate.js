$(document)
		.ready(
				function() {
					var context = $("#context").text();
					$("#btnSave")
							.click(
									function() {
										var directorateName = $(
												"#directorateName").val();
										var typeOfBusinessId = 1;
										var directorateNameShortDescription = $(
												"#directorateNameShortDescription")
												.val();
										var SICCode = 123456;
										var leadContact = 1;
										var Contact = {
											"contactId" : leadContact
										}
										var directorateNameFullDescription = $(
												"#directorateNameFullDescription")
												.val();
										var phonenumber = $("#phonenumber")
												.val();
										var addressLine1 = $("#addressLine1")
												.val();
										var addressLine2 = $("#addressLine2")
												.val();
										var addressLine3 = $("#addressLine3")
												.val();
										var webAddress = $("#webAddress").val();
										var postCode = $("#postCode").val();
										var charityNumber = $("#charityNumber")
												.val();
										var city = $("#city").val();
										var companyNumber = $("#companyNumber")
												.val();
										var county = $("#county").val();
										var nation = $("#nation").val();
										var active = "T";
										var email = $("#email").val();
										var fax = $("#fax").val();
										var BussinessType = {
											"bussinessTypeId" : typeOfBusinessId,
											"sicCode" : SICCode
										}
										var directorateId = $("#directorateId")
												.text();
										if (directorateId) {
											var data = {
												"directorateId" : directorateId,
												"directorateName" : directorateName,
												"directorateShortDescription" : directorateNameShortDescription,
												"bussinessTypeByBussinessTypeId" : BussinessType,
												"contactByContactId" : Contact,
												"directorateFullDescription" : directorateNameFullDescription,
												"phoneNumber" : phonenumber,
												"addressLine1" : addressLine1,
												"addressLine2" : addressLine2,
												"addressLine3" : addressLine3,
												"webAddress" : webAddress,
												"postcode" : postCode,
												"charityNumber" : charityNumber,
												"town" : city,
												"companyNumber" : companyNumber,
												"county" : county,
												"country" : nation,
												"active" : active,
												"email" : email,
												"fax" : fax,
											}

										} else {
											var data = {
												"directorateName" : directorateName,
												"directorateShortDescription" : directorateNameShortDescription,
												"bussinessTypeByBussinessTypeId" : BussinessType,
												"contactByContactId" : Contact,
												"directorateFullDescription" : directorateNameFullDescription,
												"phoneNumber" : phonenumber,
												"addressLine1" : addressLine1,
												"addressLine2" : addressLine2,
												"addressLine3" : addressLine3,
												"webAddress" : webAddress,
												"postcode" : postCode,
												"charityNumber" : charityNumber,
												"town" : city,
												"companyNumber" : companyNumber,
												"county" : county,
												"country" : nation,
												"active" : active,
												"email" : email,
												"fax" : fax,
											}
										}
										$
												.post({
													contentType : 'application/json; charset=utf-8',
													url : context
															+ "/adddirectorates",
													data : JSON.stringify(data),
													success : function() {

													},
													success : function(response) {
														$("#dContent").html(
																response);
													}
												})

									});
//					 $(".directorateName").click(function() {
//					 var updateRow = $(this).parent();
//					 var directorateName =
//					 updateRow.children().first().text();
//					 alert(directorateName);
//					 $.get(context+"/amend-directorates",
//					 {directorateName:directorateName},
//					 function(result) {
//					 $("#dContent").html(result);
//					 }
//					 );
//					 });

					$(".directorateName").click(function() {
						var url = $(this).attr("href")
						$.get(url, function(result) {
							$("#dContent").html(result);
						});
					});

					$("#leadContacts").click(function() {
						$("#leadContact").val(1);

					});
					$(".typeOfBusiness").click(function() {
						$("#typeOfBusiness").val(1);
						$("#SICCode").val(123456);
					});
					$(".postcode").click(function() {
						$("#postCode").val(123456);
					});

					$(".typeOfBusiness").click(function() {
						$("#SICCode").val(123456);
					});

					$("#btnBack").click(function() {
						$.get({
							url : context + "/list-directorates",
							success : function(response) {
								$("#dContent").html(response);
							}
						});
					});
					$("#inActive").click(function(){
						var directorateId=$("#directorateId").attr('value');
						
						$.post(context+"/change-inactive"),
							{directorateId: directorateId},
							function(response) {
								$("#dContent").html(response);
							}
					});
					
				});