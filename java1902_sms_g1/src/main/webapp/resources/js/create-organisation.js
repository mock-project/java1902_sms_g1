$(document).ready(function() {
	$("#btnBack").click(function() {
		$.get({
			url : "list-organisations",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'List Organisations';
			}
		});
	});
	
	$("#expressionOfInterest").change(function(event){
	    if (this.checked){
	    	$('#nav-detail-3-tab').removeAttr('hidden');
	    } else {
	    	$("#nav-detail-3-tab").attr("hidden",true);
	    }
	});
	
	$("#nav-detail-4-tab").click(function() {
		$("#error").attr("hidden",true);
	});
	
	$("#nav-detail-5-tab").click(function() {
		$("#error").attr("hidden",true);
	});
	
});