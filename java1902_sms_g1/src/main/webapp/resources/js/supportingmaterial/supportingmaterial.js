$(document)
		.ready(
				function() {
					var context = $("#context").text();

					$("#btnSave")
							.click(
									function() {
										var d = $('#addedDate').val();
										alert(d);
										var url = $("#url").val();
										var addedBy = $("#addedBy").val();
										var user = {
											"userId" : 1,
											"account" : addedBy
										};
										var description = $("#description")
												.val();
										var orgDetail = 2;
										var organisationDetailByOrganisationDetailId = {
											"organisationDetailId" : orgDetail
										}
										var type = $("#type").val();
										var active = "T";

										var data = {
											"url" : url,
											"userByUserId" : user,
											"description" : description,
											"type" : type,
											"addDate" : d,
											"organisationDetailByOrganisationDetailId" : organisationDetailByOrganisationDetailId,
											"active" : active
										}
										var token = $("meta[name='_csrf']")
												.attr("content");
										var header = $(
												"meta[name='_csrf_header']")
												.attr("content");
											$.ajax({
													type : "POST",
													contentType : 'application/json; charset=utf-8',
													beforeSend : function(xhr) {
														xhr.setRequestHeader(
																header, token);
													},
													url : context
															+ "/user/add-suportingmaterial",
													data : JSON.stringify(data),
													success : function() {

													},
													success : function(response) {
														$("#dContent").html(
																response);
													}
												})

									});
					$("#btnCreateMaterial").click(function() {
						var d = new Date();
						 var month = d.getMonth()+1;
						 var day = d.getDate();
						
						 var output = d.getFullYear() + '/' +
						 (month<10 ? '0' : '') + month + '/' +
						 (day<10 ? '0' : '') + day;
						$.get(
							context + "add-suportingmaterial",
							function(response) {
								$("#dContentMaterial").html(response);
								$('#addDate').val(output);
							}
						);
					});
					
					$("#btnSaveMaterial").click(function() {
						var url = $('#url').val();
						var description = $('#description').val();
						var type = $('#type option:selected').val();
						var addDate = $('#addDate').val();
						var userByUserId = $('#userByUserId').val();
						var csrf=$("#csrf").text();
						var formData = new FormData();
			    		formData.append('url', url);
			    		formData.append('description', description);
			    		formData.append('type', type);
			    		formData.append('addDate', addDate);
			    		formData.append('userByUserId', userByUserId);
			    		formData.append('_csrf',csrf);
						$.post({
							url : "create-support-material",
							data : formData,
							enctype : 'multipart/form-data',
							processData : false,
							contentType : false,
							success : function(response) {
								var message = response.description;
								if (response.name == "success") {
									$('#supportingMaterialId').val(response.id);
									$('#successAddMaterial').html(message);
								} else {
									$('#errorCreateMaterial').html(message);
								}
							}
						});
					});
					
					$("#btnBackToMaterialScreen").click(function() {
						var organisationId = $('#organisationId').val();
						$.get({
							url : "back-from-material",
							data : {
								organisationId : organisationId
							},
							contentType : 'application/json; charset=utf-8',
							success : function(response) {
								$("#dContent").html(response);
					        	$('#nav-detail-4-tab').removeAttr('hidden');
								$('#nav-detail-5-tab').removeAttr('hidden');
								$('#nav-detail-6-tab').removeAttr('hidden');
								$('#btnInActice').removeAttr('hidden');
								$('#nav-detail-5-tab').trigger('click');
								document.title = 'Update Organisation';
							}
						});
					});
				});
