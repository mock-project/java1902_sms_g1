$(document).ready(function() {
	$("#list-organisations").click(function() {
		$.get({
			url : "list-organisations",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'List Organisations';
			}
		});
	});
	
	$("#btnCreate").click(function() {
		$.get({
			url : "create-organisation",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'Create New Organisation';
			}
		});
	});
	
	$(".changeActive").click(function() {
		var statusAndId = $(this).attr("value");
		var convert = statusAndId.split(',');
		var active = convert[0];
		var organisationId = convert[1];
		if(active == "T") {
			$.get({
				url : "update-active-organisation",
				data : {
					organisationId : organisationId
				},
				contentType : 'application/json; charset=utf-8',
				success : function(response) {
					$("#dContent").html(response);
		        	$('#nav-detail-4-tab').removeAttr('hidden');
					$('#nav-detail-5-tab').removeAttr('hidden');
					$('#nav-detail-6-tab').removeAttr('hidden');
					$('#btnInActice').removeAttr('hidden');
					document.title = 'Update Organisation';
				}
			});
		} else {
			swal({
			    title: "Are you sure?",
			    text: "Do you want to make this Organization active?",
			    icon: "warning",
			    buttons: true
			})
			.then((willDelete) => {
			    if (willDelete) {
					$.get({
						url : "active-organisation",
						data : {
							organisationId : organisationId
						},
						contentType : 'application/json; charset=utf-8',
						success : function(response) {
							swal("Poof!" + " This Organization " 
									+ " had been changed to active status!", {
					            icon: "success",
					        }).then(function() {
					        	$("#dContent").html(response);
					        	document.title = 'Update Organisation';
					        	$('#nav-detail-4-tab').removeAttr('hidden');
								$('#nav-detail-5-tab').removeAttr('hidden');
								$('#nav-detail-6-tab').removeAttr('hidden');
								$('#btnInActice').removeAttr('hidden');
							});
						}
					});
			    } else {
			        swal("This Organization " + " still in in-active status!");
			    }
			});
		}  
	});
	
});