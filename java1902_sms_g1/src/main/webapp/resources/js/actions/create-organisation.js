$(document).ready(function() {
	$("#btnBack").click(function() {
		$.get({
			url : "list-organisations",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'List Organisations';
			}
		});
	});
	
	$("#btnTest").click(function() {
		$(document).ready(function() {
			$('#nav-detail-2-tab').trigger('click');
		});
	});
	$("#lookUpContact").click(function() {
		if($('#contactName').val().length == 0){
		$('#contactName').val('Mr. Hoang Nam Tien');
		} else {
			$('#contactName').val('');
		}
	});
	
	$("#lookUpBusiness").click(function() {
		if($('#bussinessName').val().length == 0){
		$('#bussinessName').val('abattoir (manufacturer)');
		$('#sicCode').val('10110');
		} else {
			$('#bussinessName').val('');
			$('#sicCode').val('');
		}
	});
	
	$("#btnInActice").click(function() {
		var organisationName = $('#organisationName').val();
		var organisationId = $('#organisationId').val();
		swal({
		    title: "Are you sure?",
		    text: "Organization " +organisationName + " is already in use, do you want to make this in-active?",
		    icon: "warning",
		    buttons: true,
		    dangerMode: true,
		})
		.then((willDelete) => {
		    if (willDelete) {
				$.get({
					url : "inActive-organisation",
					data : {
						organisationId : organisationId
					},
					contentType : 'application/json; charset=utf-8',
					success : function(response) {
						swal("Poof!" + " Organization " +organisationName
								+ " had been changed to in active status!", {
				            icon: "success",
				        }).then(function() {
				        	$("#dContent").html(response);
				        	document.title = 'List Organisations';
						});
					}
				});
		    } else {
		        swal("Organization " +organisationName + " still in active status!");
		    }
		});
	});
	
	$("#lookUpPostcode").click(function() {
		if($('#postcode').val().length == 0){
		$('#postcode').val('SE10 0SS');
		} else {
			$('#postcode').val('');
		}
	});

	$("#expressionOfInterest").change(function(event){
	    if (this.checked){
	    	$('#nav-detail-3-tab').removeAttr('hidden');
	    } else {
	    	$("#nav-detail-3-tab").attr("hidden",true);
	    }
	});
	
	$("#preferredOrganistion").change(function(event){
	    if (this.checked){
	    	$('#preferredOrganistion').val('Y');
	    } else {
	    	$('#preferredOrganistion').val('');
	    }
	});
	
	$("#btnSave").click(function() {
		var csrf=$("#csrf").text();
		var organisationName = $('#organisationName').val();
		var phoneNumber = $('#phoneNumber').val();
		var addressLine1 = $('#addressLine1').val();
		var organisationShortDescription = $('#organisationShortDescription').val();
		var typeOfBussiness = $('#bussinessName').val();
		var postCode = $('#postcode').val();
	    var checkFields = true;
	    
	    if($('#organisationName').val().length == 0 ) {
	    	$("#organisationName").css("border","1px solid red");
	    	if($("#errorOrganisationName").val().length == 0) {
	    		$('#errorOrganisationName').html("Please input the organisation name");
	    	}
	      checkFields = false;
	    } else {
	    	$("#organisationName").css("border","");
	    	$('#errorOrganisationName').html("");
	    }
	    
	    if($('#phoneNumber').val().length == 0 ) {
	    	 $("#phoneNumber").css("border","1px solid red");
	    	if($("#errorPhoneNumber").val().length == 0) {
		      $('#errorPhoneNumber').html("Please input the phone number");
	    	}
	    	checkFields = false;
	    	} else {
		    	$("#phoneNumber").css("border","");
		    	$('#errorPhoneNumber').html("");
		    }
	    
	    
	    if($('#addressLine1').val().length == 0 ) {
		      $("#addressLine1").css("border","1px solid red");
	    	if($("#errorAddressLine1").val().length == 0) {
		      $('#errorAddressLine1').html("Please input the address line 1");
	    	}
	    	checkFields = false;
	    	} else {
		    	$("#addressLine1").css("border","");
		    	$('#errorAddressLine1').html("");
		    }
	    
	    if($('#organisationShortDescription').val().length == 0 ) {
	    	$("#organisationShortDescription").css("border","1px solid red");
	    	if($("#errorShortDescription").val().length == 0) {
		      $('#errorShortDescription').html("Please input the short description");
	    	}
	    	checkFields = false;
	    	} else {
		    	$("#organisationShortDescription").css("border","");
		    	$('#errorShortDescription').html("");
		    }
	    
	    
	    if($('#bussinessName').val().length == 0 ) {
		      $("#bussinessName").css("border","1px solid red");
	    	if($("#errorTypeOfBusiness").val().length == 0) {
		      $('#errorTypeOfBusiness').html("Please input the type business");
	    	}
	    	checkFields = false;
	    	} else {
		    	$("#bussinessName").css("border","");
		    	$('#errorTypeOfBusiness').html("");
		    }
	    
	    
	    if($('#postcode').val().length == 0 ) {
	    	$("#postcode").css("border","1px solid red");
	    	if($("#errorPostcode").val().length == 0) {
		      $('#errorPostcode').html("Please input the post code");
	    	}
	    	checkFields = false;
	    	} else {
		    	$("#postcode").css("border","");
		    	$('#errorPostcode').html("");
	    }
	    
	    if(checkFields == true) {
	    	var organisationId = $('#organisationId').val();
	    	if(organisationId == "") {
	    		var organisationName = organisationName;
	    		var organisationShortDescription = organisationShortDescription;
	    		var addressLine1 = addressLine1;
	    		var addressLine2 = $('#addressLine2').val();
	    		var addressLine3 = $('#addressLine3').val();
	    		var postcode = postCode;
	    		var town = $('#town').val();
	    		var county = $('#county').val();
	    		var country = $('#country').val();
	    		var preferredOrganisation = $('#preferredOrganistion').val();
	    		var organisationFullDescription = $('#organisationFullDescription').val();
	    		var phoneNumber = phoneNumber;
	    		var fax = $('#fax').val();
	    		var email = $('#email').val();
	    		var webAddress = $('#webAddress').val();
	    		var charityNumber = $('#charityNumber').val();
	    		var companyNumber = $('#companyNumber').val();
	    		var bussinessTypeByBussinessTypeId = typeOfBussiness;
	    		var contactByContactId = $('#contactName').val();
	    		var firstCheckBoxes = [];
	            $.each($("input[name='organisationSpecialism']:checked"), function(){            
	            	firstCheckBoxes.push($(this).val());
	            });
	    		var organisationSpecialism = firstCheckBoxes.join(", ");
	    		var secondCheckBoxes = [];
	            $.each($("input[name='serviceDisablitiesCapabilities']:checked"), function(){            
	            	secondCheckBoxes.push($(this).val());
	            });
	    		var serviceDisablitiesCapabilities = secondCheckBoxes.join(", ");
	    		var thirdCheckBoxes = [];
	            $.each($("input[name='serviceBarriersCapabilities']:checked"), function(){            
	            	thirdCheckBoxes.push($(this).val());
	            });
	    		var serviceBarriersCapabilities = thirdCheckBoxes.join(", ");
	    		var fourthCheckBoxes = [];
	            $.each($("input[name='serviceBenefitsCapabilities']:checked"), function(){            
	            	fourthCheckBoxes.push($(this).val());
	            });
	    		var serviceBenefitsCapabilities = fourthCheckBoxes.join(", ");
	    		var fiveCheckBoxes = [];
	            $.each($("input[name='servicePersonalCircumstacesCapabilities']:checked"), function(){            
	            	fiveCheckBoxes.push($(this).val());
	            });
	    		var servicePersonalCircumstacesCapabilities = fiveCheckBoxes.join(", ");
	    		var sixthCheckBoxes = [];
	            $.each($("input[name='serviceEthnicity']:checked"), function(){            
	            	sixthCheckBoxes.push($(this).val());
	            });
	    		var serviceEthnicity = sixthCheckBoxes.join(", ");
	    		var seventhCheckBoxes = [];
	            $.each($("input[name='accreditation']:checked"), function(){            
	            	seventhCheckBoxes.push($(this).val());
	            });
	    		var accreditation = seventhCheckBoxes.join(", ");
	    		var eighthCheckBoxes = [];
	            $.each($("input[name='eoiProgrammes']:checked"), function(){            
	            	eighthCheckBoxes.push($(this).val());
	            });
	    		var eoiProgrammes = eighthCheckBoxes.join(", ");
	    		var ninethCheckBoxes = [];
	            $.each($("input[name='eoiServices']:checked"), function(){            
	            	ninethCheckBoxes.push($(this).val());
	            });
	    		var eoiServices = ninethCheckBoxes.join(", ");
	    		var csrf=csrf;
	    		var formData = new FormData();
	    		formData.append('organisationName', organisationName);
	    		formData.append('organisationShortDescription', organisationShortDescription);
	    		formData.append('addressLine1', addressLine1);
	    		formData.append('addressLine2', addressLine2);
	    		formData.append('addressLine3', addressLine2);
	    		formData.append('postcode', postcode);
	    		formData.append('town', town);
	    		formData.append('county', county);
	    		formData.append('country', country);
	    		formData.append('preferredOrganisation', preferredOrganisation);
	    		formData.append('organisationFullDescription', organisationFullDescription);
	    		formData.append('phoneNumber', phoneNumber);
	    		formData.append('fax', fax);
	    		formData.append('email', email);
	    		formData.append('webAddress', webAddress);
	    		formData.append('charityNumber', charityNumber);
	    		formData.append('companyNumber', companyNumber);
	    		formData.append('bussinessTypeByBussinessTypeId', bussinessTypeByBussinessTypeId);
	    		formData.append('contactByContactId', contactByContactId);
	    		formData.append('organisationSpecialism', organisationSpecialism);
	    		formData.append('serviceDisablitiesCapabilities', serviceDisablitiesCapabilities);
	    		formData.append('serviceBarriersCapabilities', serviceBarriersCapabilities);
	    		formData.append('serviceBenefitsCapabilities', serviceBenefitsCapabilities);
	    		formData.append('servicePersonalCircumstacesCapabilities', servicePersonalCircumstacesCapabilities);
	    		formData.append('serviceEthnicity', serviceEthnicity);
	    		formData.append('accreditation', accreditation);
	    		formData.append('eoiProgrammes', eoiProgrammes);
	    		formData.append('eoiServices', eoiServices);
	    		formData.append('_csrf',csrf);
				$.post({
					url : "create-organisation",
					data : formData,
					enctype : 'multipart/form-data',
					processData : false,
					contentType : false,
					success : function(response) {
						var message = response.description;
						if (response.name == "success") {
							$('#organisationId').val(response.id);
				        	$('#nav-detail-4-tab').removeAttr('hidden');
							$('#nav-detail-5-tab').removeAttr('hidden');
							$('#nav-detail-6-tab').removeAttr('hidden');
							$('#btnInActice').removeAttr('hidden');
							$('#success').html(message);
						} else {
							$('#errorCreate').html(message);
						}
					}
				});
	    	} else {
	    		var csrf=csrf;
	    		var organisationName = organisationName;
	    		var organisationShortDescription = organisationShortDescription;
	    		var addressLine1 = addressLine1;
	    		var addressLine2 = $('#addressLine2').val();
	    		var addressLine3 = $('#addressLine3').val();
	    		var postcode = postCode;
	    		var town = $('#town').val();
	    		var county = $('#county').val();
	    		var country = $('#country').val();
	    		var preferredOrganisation = $('#preferredOrganistion').val();
	    		var organisationFullDescription = $('#organisationFullDescription').val();
	    		var phoneNumber = phoneNumber;
	    		var fax = $('#fax').val();
	    		var email = $('#email').val();
	    		var webAddress = $('#webAddress').val();
	    		var charityNumber = $('#charityNumber').val();
	    		var companyNumber = $('#companyNumber').val();
	    		var bussinessTypeByBussinessTypeId = typeOfBussiness;
	    		var contactByContactId = $('#contactName').val();
	    		var firstCheckBoxes = [];
	            $.each($("input[name='organisationSpecialism']:checked"), function(){            
	            	firstCheckBoxes.push($(this).val());
	            });
	    		var organisationSpecialism = firstCheckBoxes.join(", ");
	    		var secondCheckBoxes = [];
	            $.each($("input[name='serviceDisablitiesCapabilities']:checked"), function(){            
	            	secondCheckBoxes.push($(this).val());
	            });
	    		var serviceDisablitiesCapabilities = secondCheckBoxes.join(", ");
	    		var thirdCheckBoxes = [];
	            $.each($("input[name='serviceBarriersCapabilities']:checked"), function(){            
	            	thirdCheckBoxes.push($(this).val());
	            });
	    		var serviceBarriersCapabilities = thirdCheckBoxes.join(", ");
	    		var fourthCheckBoxes = [];
	            $.each($("input[name='serviceBenefitsCapabilities']:checked"), function(){            
	            	fourthCheckBoxes.push($(this).val());
	            });
	    		var serviceBenefitsCapabilities = fourthCheckBoxes.join(", ");
	    		var fiveCheckBoxes = [];
	            $.each($("input[name='servicePersonalCircumstacesCapabilities']:checked"), function(){            
	            	fiveCheckBoxes.push($(this).val());
	            });
	    		var servicePersonalCircumstacesCapabilities = fiveCheckBoxes.join(", ");
	    		var sixthCheckBoxes = [];
	            $.each($("input[name='serviceEthnicity']:checked"), function(){            
	            	sixthCheckBoxes.push($(this).val());
	            });
	    		var serviceEthnicity = sixthCheckBoxes.join(", ");
	    		var seventhCheckBoxes = [];
	            $.each($("input[name='accreditation']:checked"), function(){            
	            	seventhCheckBoxes.push($(this).val());
	            });
	    		var accreditation = seventhCheckBoxes.join(", ");
	    		var eighthCheckBoxes = [];
	            $.each($("input[name='eoiProgrammes']:checked"), function(){            
	            	eighthCheckBoxes.push($(this).val());
	            });
	    		var eoiProgrammes = eighthCheckBoxes.join(", ");
	    		var ninethCheckBoxes = [];
	            $.each($("input[name='eoiServices']:checked"), function(){            
	            	ninethCheckBoxes.push($(this).val());
	            });
	    		var eoiServices = ninethCheckBoxes.join(", ");
	    		var govOfficeRegionName = $("#govOfficeRegionName option:selected").val();
	    		var trustRegionName = $("#trustRegionName option:selected").val();
	    		var trustDistrictName = $("#trustDistrictName option:selected").val();
	    		var formData = new FormData();
	    		formData.append('organisationId', organisationId);
	    		formData.append('organisationName', organisationName);
	    		formData.append('organisationShortDescription', organisationShortDescription);
	    		formData.append('addressLine1', addressLine1);
	    		formData.append('addressLine2', addressLine2);
	    		formData.append('addressLine3', addressLine2);
	    		formData.append('postcode', postcode);
	    		formData.append('town', town);
	    		formData.append('county', county);
	    		formData.append('country', country);
	    		formData.append('preferredOrganisation', preferredOrganisation);
	    		formData.append('organisationFullDescription', organisationFullDescription);
	    		formData.append('phoneNumber', phoneNumber);
	    		formData.append('fax', fax);
	    		formData.append('email', email);
	    		formData.append('webAddress', webAddress);
	    		formData.append('charityNumber', charityNumber);
	    		formData.append('companyNumber', companyNumber);
	    		formData.append('bussinessTypeByBussinessTypeId', bussinessTypeByBussinessTypeId);
	    		formData.append('contactByContactId', contactByContactId);
	    		formData.append('organisationSpecialism', organisationSpecialism);
	    		formData.append('serviceDisablitiesCapabilities', serviceDisablitiesCapabilities);
	    		formData.append('serviceBarriersCapabilities', serviceBarriersCapabilities);
	    		formData.append('serviceBenefitsCapabilities', serviceBenefitsCapabilities);
	    		formData.append('servicePersonalCircumstacesCapabilities', servicePersonalCircumstacesCapabilities);
	    		formData.append('serviceEthnicity', serviceEthnicity);
	    		formData.append('accreditation', accreditation);
	    		formData.append('eoiProgrammes', eoiProgrammes);
	    		formData.append('eoiServices', eoiServices);
	    		formData.append('govOfficeRegionName', govOfficeRegionName);
	    		formData.append('trustRegionName', trustRegionName);
	    		formData.append('trustDistrictName', trustDistrictName);
	    		formData.append('_csrf',csrf);
				$.post({
					url : "update-organisation",
					data : formData,
					enctype : 'multipart/form-data',
					processData : false,
					contentType : false,
					success : function(response) {
						var message = response.description;
						if (response.name == "success") {
							$('#organisationId').val(organisationId);
							$('#success').html(message);
						} else {
							$('#success').html("");
							$('#errorCreate').html("Fail to update. " +message);
						}
					}
				});
	    	}
	    } else {
    	/*
    	 * Do nothing
    	 */
	    }

	});
	
	
});