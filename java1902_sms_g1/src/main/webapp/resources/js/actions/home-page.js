$(document).ready(function() {
	$(document).ready(function() {
		/* Highlight 'a' selected */
		$("#selected a").click(function() {
			$("#selected a").removeClass("active");
			$("#selected a").css({
				'background-color' : '',
				'opacity' : ''
			});
			$("#selected a").css("border", "");
			$("#selected a").css("font-weight", "");
			$(this).addClass("active");
			$(this).css("background-color", "#ff6600");
			$(this).css("border", "1px solid #ff6600");
			$(this).css("font-weight", "bold");
		});
	});

	$("#list-organisations").click(function() {
		$.get({
			url : "list-organisations",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'List Organisations';
			}
		});
	});

	$("#btnCreate").click(function() {
		$.get({
			url : "create-organisation",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'Create Organisation';
			}
		});
	});
	
});