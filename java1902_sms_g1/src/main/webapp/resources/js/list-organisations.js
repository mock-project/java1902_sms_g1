$(document).ready(function() {
	$("#list-organisations").click(function() {
		$.get({
			url : "list-organisations",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'List Organisations';
			}
		});
	});
	
	$("#btnCreate").click(function() {
		$.get({
			url : "create-organisation",
			success : function(response) {
				$("#dContent").html(response);
				document.title = 'Create New Organisation';
			}
		});
	});
});